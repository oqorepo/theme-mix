<?php
get_header();
get_template_part('secciones/header-normal');
?>
<script>
$(document).ready(function() {

  var url = window.location.href;
  if(url.indexOf('?ver=bases') != -1 || url.IndexOf('?ver=bases') != -1) {
  $('#bases').modal('show');
  }

  $('.head').css('border', '0px');
  $('.linea-menu-bot').addClass('activo');
  // Google Tag Manager
  dataLayer.push({
  'Promo': '
  <?php the_title();?>',
  'tipoPagina': 'Promoción',
  });

});
</script>
<!-- Título -->
<section class="seccion-oscura" style="margin-bottom:30px;">
  <div class="container">
    <div class="row">
      <h1 class="display blanco"><?php the_title();?></h1>
    </div>
  </div>
</section>
<!-- Título -->
<section class="contenido-promo" style="margin-bottom:25px;">
  <div class="container" >
    <div class=" btn-group btn-breadcrumb breadubicaciones">
      <a href="<?php bloginfo('url');?>" class="btn btn-default btn-breadcrumb"><i class="glyphicon glyphicon-home"></i></a>
      <a href="<?php bloginfo('url');?>/promociones" class="btn btn-default btn-breadcrumb">Promociones</a>
      <a href="<?php the_permalink();?>" class="btn btn-default btn-breadcrumb"><?php the_title();?></a>
    </div>
    <div class="row">
      <!-- Contenido -->
      <div class="col-md-4">
        <?php
        $imagen = get_field('imagen_promocion_mobile');
        $imagen_promo_array = wp_get_attachment_image_src($imagen, 'full');
        ?>
        <!-- Estado de la promoción -->
        <?php
        $estado = get_field('estado');
        $personalizar = get_field('personalizar');
        $fecha = get_field('fecha_de_expiracion', false, false);
        $fecha = new DateTime($fecha);
        $fechaFormato = $fecha->format('Ymd');
        $hoy = date( 'Ymd', current_time( 'timestamp', 0 ));
        ?>
        <?php  if ($fechaFormato >= $hoy):?>
        <?php if ($estado):?>
        <?php if ($estado == "Vigente"):?>
        <?php else:?>
        <div class="ribbon ribbon-top-left" style="margin-top:0px; margin-left:0px; left:15px !important;"><span style="font-size:13px !important; top:42px;">
          <?php if ($estado == "Personalizar"):?>
          <?php echo $personalizar;?>
          <?php else:?>
          <?php echo $estado;?>
          <?php endif;?>
        </span></div>
        <?php endif;?>
        <?php endif;?>
        <img src="<?php echo $imagen_promo_array[0];?>" class="img-responsive tooltipster" title="Ampliar" data-action="zoom" alt="<?php the_title();?>" />
        <?php else:?>
        <div class="ribbon ribbon-top-left" style="margin-top:0px; margin-left:0px; left:15px !important;"><span style="font-size:13px !important; top:42px;">
          No es Vigente
        </span>
      </div>
      <img src="<?php echo $imagen_promo_array[0];?>" class="img-responsive tooltipster blancoynegro" title="Ampliar" data-action="zoom" alt="<?php the_title();?>" />
      <?php endif;?>
      <!-- Fin de estado de Promoción -->
    </div>
    <div class="col-md-8">
      <div class="informacion-promociones" style="margin-top:15px; margin-bottom:15px;">
        <?php
        $estado = get_field('estado');
        $personalizar = get_field('personalizar');
        $fecha = get_field('fecha_de_expiracion', false, false);
        $fecha = new DateTime($fecha);
        $fechaFormato = $fecha->format('jmy');
        $hoy = date( 'jmy', current_time( 'timestamp', 0 ));
        ?>
        <?php  if ($fechaFormato >= $hoy):?>
        <?php else:?>
        <!-- Esta promoción ya no es vigente. -->
        <?php endif;?>
        <?php
        $selecciona = get_field('selecciona_proyecto_o_planta');?>
        <?php if( $selecciona ): ?>
        <h3 class="links-promo" style="font-weight: bold;">Promoción válida para
        <?php foreach( $selecciona as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <a href="<?php the_permalink();?>"  onclick="dataLayer.push({'promocionVinculada': '<?php the_title();?>'});"><?php the_title();?></a>
        <?php endforeach; ?>
        </h3>
        <?php wp_reset_postdata();?>
        <?php endif; ?>
        <?php wp_reset_postdata();?>
        <?php
        $dateformatstring = "j F Y";
        $unixtimestamp = strtotime(get_field('fecha_de_expiracion', false, false));
        ?>
        <!-- Fecha -->
        <?php $fecha = get_field('fecha_de_expiracion', false, false);
        $fecha = new DateTime($fecha);
        ?>
        <p><i class="far fa-calendar-alt"></i> Vigencia hasta <?php echo date_i18n($dateformatstring, $unixtimestamp);?> </p>
      </div>
      <?php the_field('contenido_promocion');?>
      <?php $urlespecial = get_field('¿envia_a_una_url_en_especial');
      $urllink = get_field('url_personalizada');
      $formaurl = get_field('forma_de_abrir_la_nueva_url');
      if ($urlespecial == "Si"):?>
      <a href="<?php echo $urllink;?>" target="_<?php echo $formaurl;?>">
        <button class="btn btn-rojo" style="margin-top:30px; font-size:11px !important;" onclick="dataLayer.push({'Participar': 'Si'});">
        <?php 
        $av_text_btn = get_field('av_texto_del_boton');
        if ($av_text_btn) {
          echo $av_text_btn;
        }else{
          echo 'Participar';
        }
        ?>
        </button>
      </a>
      <?php endif;?>
      <!-- Bases Legales -->
      <?php $bases = get_field('contenido_bases');?>
      <?php if ($bases):?>
      <button class="btn btn-gris-claro abrir-bases" style="margin-top:30px; font-size:11px !important;" onclick="dataLayer.push({'verBases': 'Si'});" data-toggle="modal" data-target="#bases"><i class="far fa-file-alt"></i> Ver bases legales</button>
      <?php endif;?>
      <!-- Modal -->

      <div class="modal fade" id="bases" tabindex="999999" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close tooltipster" title="Cerrar" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">BASES LEGALES PROMOCIÓN <?php the_title();?></h4>
            </div>
            <div class="modal-body">
              <?php the_field('contenido_bases');?>
              <?php $pdf = get_field('agregar_pdf');?>
              <?php if ($pdf):?>
              <hr>
              <a href="<?php echo $pdf;?>" target="_new">
                <button class="btn btn-rojo text-center" style="display:block; margin:0 auto;">VER BASES LEGALES PDF</button>
              </a>
              <?php endif;?>
            </div>
            <div class="modal-footer">
              <?php if (!is_page('contacto')):?><a href="<?php bloginfo('url');?>/contacto"><div class="btn btn-gris"><i class="fas fa-envelope"></i> Contacto</div></a><?php endif;?>
              <button type="button" class="btn btn-default btn-rojo" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cerrar</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin Bases Legales -->
      <!-- Compartir -->
      <!-- Compartir -->
    </div>
  </div>
</div>
</section>


<?php $post_id = get_the_ID();?>
<!-- Loop Promociones -->
<?php
$post_id          = get_the_ID();
$hoy              = date( 'Ymd', current_time( 'timestamp', 0 ));
$query            = new WP_Query(array(
'post_type'       => array('promociones'),
'posts_per_page'  => -1,
'post_status'     => 'publish',
'post__not_in'    => array($post_id),
'order'           => 'DESC',
'orderby'         => 'meta_value',
'meta_key'        => 'fecha_de_expiracion' // Se ordena por fecha de expiración
));
if ( $query->have_posts() ) : ?>
<section class="mas-promos">
  <div class="container">
    <h2>Más Promociones</h2>
    <div class="row">
      <?php while ( $query->have_posts() ) : $query->the_post(); ?>
      <div class="col-md-3" style="margin-top:30px;">
        <?php // Variables
        $imagen = get_field('imagen_promocion_mobile');
        $imagen_promo_array = wp_get_attachment_image_src($imagen, 'full');
        $estado = get_field('estado');
        $personalizar = get_field('personalizar');
        $fecha = get_field('fecha_de_expiracion', false, false);
        $fecha = new DateTime($fecha);
        $fechaFormato = $fecha->format('Ymd');
        $hoy = date( 'Ymd', current_time( 'timestamp', 0 ));
        $dateformatstring = "j F Y";
        $unixtimestamp = strtotime(get_field('fecha_de_expiracion', false, false));
        $selecciona = get_field('selecciona_proyecto_o_planta');
        ?>
        <!-- Condicionales -->
        <?php  if ($fechaFormato >= $hoy):?>
        <a href="<?php the_permalink();?>" id="clickPromocion" onclick="dataLayer.push({'Promo': '<?php the_title();?>'});">
          <div class="ribbon ribbon-top-left" style="margin-top:0px; margin-left:0px; left:15px !important;">
            <span style="font-size:13px !important; top:42px;">
              <?php if ($estado == "Personalizar"):?>
              <?php echo $personalizar;?>
              <?php else:?>
              <?php echo $estado;?>
              <?php endif;?>
            </span>
          </div>
          <img src="<?php echo $imagen_promo_array[0];?>" class="img-responsive tooltipster" title="Ver Promoción"  alt="<?php the_title();?>" />
        </a>
        <strong><?php the_title();?></strong>
        <p><i class="far fa-calendar-alt"></i> Vigencia hasta <?php echo date_i18n($dateformatstring, $unixtimestamp);?> </p>
        <hr>
        <?php if( $selecciona ): ?>
        <div>Promoción válida para</div>
        <?php foreach( $selecciona as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <a href="<?php the_permalink();?>"  onclick="dataLayer.push({'promocionVinculada': '<?php the_title();?>'});"><?php the_title();?></a>
        <?php endforeach; ?>
        <?php endif;?>
        <?php else:?>
        <img src="<?php echo $imagen_promo_array[0];?>" class="img-responsive tooltipster blancoynegro" title="Promoción No Vigente"  alt="<?php the_title();?>" />
        <strong><?php the_title();?></strong>
        <div><i class="far fa-calendar-alt"></i> Promoción No Vigente</div>
        Finalizó el <?php echo date_i18n($dateformatstring, $unixtimestamp);?>
        <?php if( $selecciona ): ?>
        <hr>
        <div>Promoción válida para</div>
        <?php foreach( $selecciona as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <a href="<?php the_permalink();?>"  onclick="dataLayer.push({'promocionVinculada': '<?php the_title();?>'});"><?php the_title();?></a>
        <?php endforeach; ?>
        <?php endif;?>
        <?php endif;?>
        <!-- Condicionales -->
      </div>
      <?php endwhile;?>
    </div>
  </div>
</section>
<?php endif;?>


<?php get_footer();?>