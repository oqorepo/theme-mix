<?php
class BD {
		/* variables de conexion */
		var $BaseDatos;
		var $Servidor;
		var $Usuario;
		var $Clave;

		/* identificador de conexion y consulta */
		var $Conexion_ID = 0;
		var $Consulta_ID = 0;

		/* texto error */
		var $Error = false;
		var $ID_Error;

		function BD() {
			$this->Servidor = 'localhost';
			$this->Usuario = 'desarrol_oqo';
			// $this->Usuario = 'root';
			$this->Clave = 'desarrollo';
			// $this->Clave = '';
			// $this->BaseDatos_sitio = 'avellane_sitio';
			$this->BaseDatos_postventa = "desarrol_postventa";
			// $this->BaseDatos_newsletter = "avellane_newsletter";
			// $this->BaseDatos_contacto = "avellane_contactos";
		}

		/*Conexion a la base de datos*/
		function conectar(){
			$this->Error = false;
			// Conectamos al servidor
			$this->Conexion_ID = mysqli_connect($this->Servidor, $this->Usuario, $this->Clave);
			if (!$this->Conexion_ID) {
				$this->Error = "Ha fallado la conexion";
				return 0;
			}
			mysqli_select_db($this->BaseDatos_sitio);
			return $this->Conexion_ID;
		}

		/*Conexion a la base de datos*/
		function conectar_postventa(){
			$this->Error = false;
			// Conectamos al servidor
			$this->Conexion_ID = mysqli_connect($this->Servidor, $this->Usuario, $this->Clave);
			if (!$this->Conexion_ID) {
				$this->Error = "Ha fallado la conexion";
				return 0;
			}
			mysqli_select_db($this->BaseDatos_postventa);
			return $this->Conexion_ID;
		}

		/*Conexion a la base de datos*/
		function conectar_newsletter(){
			$this->Error = false;
			// Conectamos al servidor
			$this->Conexion_ID = mysqli_connect($this->Servidor, $this->Usuario, $this->Clave);
			if (!$this->Conexion_ID) {
				$this->Error = "Ha fallado la conexion";
				return 0;
			}
			mysqli_select_db($this->BaseDatos_newsletter);
			return $this->Conexion_ID;
		}

		/*Conexion a la base de datos*/
		function conectar_contactos(){
			$this->Error = false;
			// Conectamos al servidor
			$this->Conexion_ID = mysqli_connect($this->Servidor, $this->Usuario, $this->Clave);
			if (!$this->Conexion_ID) {
				$this->Error = "Ha fallado la conexion";
				return 0;
			}
			mysqli_select_db($this->BaseDatos_contacto);
			return $this->Conexion_ID;
		}

		/*Desonexi0n a la base de datos*/
		function desconectar(){
			mysqli_close($this->Conexion_ID);
		}

		/*Limpia una consulta realizada*/
		function limpiar(){
			$this->Error = false;
			mysqli_free_result ($this->Consulta_ID);
		}

		function consulta($sql = ""){
			if ($sql == "") {
				$this->Error = true;
				return 0;
			}
			$this->Consulta_ID = mysqli_query($sql, $this->Conexion_ID);
			if (!$this->Consulta_ID) {
				$this->Error = true;
				echo $sql;
			}
			return $this->Consulta_ID;
		}

		/* Devuelve el n�mero de campos de una consulta */
		function numcampos() {
			return mysqli_num_fields($this->Consulta_ID);
		}

		/* Devuelve el n�mero de registros de una consulta */
		function numregistros(){
			return mysqli_num_rows($this->Consulta_ID);
		}

		/* Devuelve el nombre de un campo de una consulta */
		function nombrecampo($numcampo) {
			return mysqli_field_name($this->Consulta_ID, $numcampo);
		}

		function fetch_row($num_row){
			return mysqli_fetch_assoc($this->Consulta_ID);
		}

		function affected_rows(){
			return mysqli_affected_rows($this->Conexion_ID);
		}

		function last_id(){
			return mysqli_insert_id($this->Conexion_ID);
		}

		function get_error(){
			return $this->Error;
		}
}

