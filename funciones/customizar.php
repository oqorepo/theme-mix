<?php
function personalizar_logo() {
echo '
<style type="text/css">
#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon { width: 100px; height: 50px; float: left !important;  }
#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before { background: url(' . get_bloginfo('stylesheet_directory') . '/recursos/img/iconos/logoadmin.svg) top no-repeat !important; background-position: 0 0; color: rgba(0, 0, 0, 0); width: 130px; height: 50px; float: left; }
#wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon { background-position: 0 0; }
#wpadminbar #wp-admin-bar-wp-logo>.ab-item .ab-icon { width: 150px; }
#adminmenu, #adminmenuback, #adminmenuwrap { background: #363b3f; margin-top: 20px; }
#wpadminbar { background: #363b3f; }
#adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head, #adminmenu .wp-menu-arrow, #adminmenu .wp-menu-arrow div, #adminmenu li.current a.menu-top, #adminmenu li.wp-has-current-submenu a.wp-has-current-submenu, .folded #adminmenu li.current.menu-top, .folded #adminmenu li.wp-has-current-submenu { background: #e5302d; }
#adminmenu li.menu-top:hover, #adminmenu li.opensub>a.menu-top, #adminmenu li>a.menu-top:focus { color: #e5302d; }
#adminmenu .wp-submenu a:focus, #adminmenu .wp-submenu a:hover, #adminmenu a:hover, #adminmenu li.menu-top>a:focus { color: #e5302d; }
#adminmenu li a:focus div.wp-menu-image:before, #adminmenu li.opensub div.wp-menu-image:before, #adminmenu li:hover div.wp-menu-image:before { color: #e5302d; }
#ubicacionesdiv { display: none; }
#adminmenu .wp-submenu a:focus, #adminmenu .wp-submenu a:hover, #adminmenu a:hover, #adminmenu li.menu-top>a:focus { color: white; }
#collapse-button {
	display:none !important;
	visibility: hidden;
}


</style>
';
}
 
//hook into the administrative header output
add_action('wp_before_admin_bar_render', 'personalizar_logo');



add_action( 'admin_menu', 'my_remove_menu_pages' );

function my_remove_menu_pages() {
    remove_menu_page('link-manager.php');
    //remove_menu_page('tools.php');
    // remove_menu_page('users.php');
    remove_menu_page('edit-comments.php');  
    remove_menu_page ('edit.php');
}