<nav id="menu">
	<ul>
		<li><a href="<?php bloginfo('url');?>"><i class="fas fa-home"></i> Inicio</a></li>
		<li><span><i class="fas fa-building"></i> Proyectos</span>
		<ul>
			<li><span>En Santiago</span>
			<ul>
			
			<?php
			 $_posts = new WP_Query( array(
			'post_type'         => 'proyectos',
			'posts_per_page'    => -1,
            'post_status'		=> 'publish',
				'tax_query' => array(
				array(
				'taxonomy' => 'ubicaciones',
				'terms'    => '23'),
				),
			));
			if( $_posts->have_posts() ) :
				while ( $_posts->have_posts() ) : $_posts->the_post();
                    $pronto = get_field('url_landing_proyecto');
                    $agotado = get_field('selecciona_tag_del_proyecto');
                    ?>
					<?php if ($agotado === "Agotado"):?>
					<?php else:?>
						<?php if ($pronto):?>
							<li><a href="<?php echo $pronto;?>" target="_blank"><?php the_title();?></a></li>
						<?php else:?>
							<li><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
						<?php endif;?>
					<?php endif;?>
				<?php endwhile;?>
            <?php endif;?>
				
			</ul>
		</li>
		<li><span>En Regiones</span>
		<ul>
			<?php
			 $_posts = new WP_Query( array(
			'post_type'         => 'proyectos',
			'posts_per_page'    => -1,
            'post_status'		=> 'publish',
				'tax_query' => array(
				array(
				'taxonomy' => 'ubicaciones',
				'terms'    => '25'),
				),
			));
			if( $_posts->have_posts() ) :
				while ( $_posts->have_posts() ) : $_posts->the_post();
					$pronto = get_field('url_landing_proyecto');
					$agotado = get_field('selecciona_tag_del_proyecto');?>
					<?php if ($agotado === "Agotado"):?>
					<?php else:?>
						<?php if ($pronto):?>
							<li><a href="<?php echo $pronto;?>" target="_blank"><?php the_title();?></a></li>
							<?php else:?>
							<li><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
						<?php endif;?>
					<?php endif;?>
				<?php endwhile;?>
			<?php endif;?>
		</ul>
	</li>
</ul>
</li>
<li><span><i class="fas fa-handshake"></i> Servicio al Cliente</span>
<ul>
		<li><a href="<?php bloginfo('url');?>/servicio-al-cliente/servicio-post-venta/"><i class="fas fa-comments"></i> Servicio Post-Venta</a></li>
	<li><a href="https://avellaneda.agendapro.co/cl" target="_new"><i class="fas fa-calendar-check"></i> Reserva de Hora Pre-entrega y entrega</a></li>
</ul>
</li>

<?php  wp_nav_menu_no_ul_celular(); ?>
</ul>
</nav>
<?php wp_reset_postdata(); ?>