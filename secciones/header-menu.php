<script>
window.onload = function() {
		$(document).ready(function() {
	 if ($('ul.menu-principal').children().find('.open').length) {
	    	    $('.linea-menu-bot').css('background', '#e5302d');
	    }
	});
};
	</script>

	
<header>
	<?php get_template_part('secciones/menu-top');?>
	<div class="head">
		<div class="container">
		<div class="logo animated fadeInLeft">
				<a href="<?php bloginfo('url');?>" class="tooltipster" title="Ir al Inicio"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logo.svg" class="img-responsive" alt="<?php bloginfo('name');?>"></a>
			</div>
			<ul class="menu-principal">
				<li class="menu_proyectos_rm"><a href="#">Proyectos Región Metropolitana</a></li>
				<li class="menu_proyectos_re"><a href="#">Proyectos Regiones</a></li>
				<?php get_template_part('secciones/menu-wp');?>

	

			</ul>
		</div>
		<?php get_template_part('modulos/menu_rm');?>
		<?php get_template_part('modulos/menu_re');?>

			<div class="linea-menu-bot linea-menu-bot-fija"></div>
			
	</div>



</header>




<header class="celular">
	<div class="container">
		<div class="logo">
			<a href="<?php bloginfo('url');?>" class="tooltipster" title="Ir al Inicio"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logo.svg" class="img-responsive" alt="<?php bloginfo('name');?>"></a>
		</div>
		<div class="mh-head Sticky">
			<span class="mh-btns-left">
				<a href="#menu"> <i class="fas fa-bars"></i></a>
			</span>
		</div>
		<!--<div class="buscador">
			<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/buscar.svg" class="img-responsive" alt="Buscar">
		</div>-->
	</div>
	<?php get_template_part('secciones/menu-top-celular');?>
			<div class="linea-menu-bot"></div>
</header>



<?php get_template_part('secciones/menu-celular');?>