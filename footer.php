<?php if (is_page('Contacto')):?>
<?php else:?>
<!-- Sección Landing -->
<?php
$taxonomy = 'category';
$terms = get_terms($taxonomy);?>
<?php	if ( $terms && !is_wp_error( $terms ) ) :	?>
<section class="landing">
	<div class="container">
		<div class="categoria">
			<?php foreach ( $terms as $term ) :?>
			<li class="row bloques-paginas-landing">
				<div class="col-md-2">
					<div class="nombre-landing"> <?php echo $term->name; ?></div>
				</div>
				<div class="col-md-10">
					<?php
						$query = new WP_Query(array(
											'post_type' 		=> array('landing'),
									'posts_per_page'	=> -1,
											'post_status'		=> 'publish',
							'orderby' => 'ID',
							'order' => 'ASC',
							'tax_query' => array(
							array(
								'taxonomy' => 'category',
								'terms'    => $term,
								),
						),
					));
					if ( $query->have_posts() ) :?>
					<ul class="paginas-landing">
						<?php while ( $query->have_posts() ) : $query->the_post();?>
						<?php $apareceEnElMenu = get_field('incluir_landing_en_el_footer_del_sitio');
						if ($apareceEnElMenu == "Si"):?>
						<li class="si-existe">
							<a href="<?php the_permalink();?>" onclick="dataLayer.push({'Landing': '<?php the_title();?>'});" style="<?php if (get_field('destacar_landing') === "Si" ):?>
								color:#e5302d !important; font-weight: bold;
								<?php endif;?>"><?php the_title();?>
							</a>
						</li>
						<?php endif;?>
						<?php endwhile;?>
					</ul>
					<?php endif;?>
				</div>
			</li>
			<?php endforeach;?>
		</div>
		<?php /* $data_args = array(
                'post_type'      => 'legal',
                'posts_per_page' => 1
            );
            $data_loop = new WP_Query( $data_args ); 
            ?>
            <?php if ( $data_loop->have_posts() ) :?>
            <?php while ($data_loop->have_posts()) : $data_loop->the_post();*/
            ?>
		<?php //echo the_field('_actualizacion_de_precios_footer'); ?>
		<?php //endwhile; endif; ?>
	</div>
</section>
<?php endif;?>
<?php wp_reset_postdata();
?>
<!-- Sección Landing -->
<?php endif;?>
<div class="subir">
	<a href="#up" class="click"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/subir.svg" alt="Subir" class="subir tooltipster2" data-tooltipster='{"side":"left"}' title="Subir al Inicio" /></a>
</div>
<?php if (is_page_template('pag-registros.php') || is_page_template('pag-registrospadre.php')):?>
<?php else:?>
<div class="solo-desktop">
	<div class="side-compartir">
		<li class="twitter">
			<a class="tooltipster2"  data-tooltipster='{"side":"right"}' href="https://twitter.com/share?url=<?php the_permalink();?>" title="Compartir en Twitter" target="_blank">
				<i class="fab fa-twitter"></i>
			</a>
		</li>
		<li class="facebook">
			<a class="tooltipster2"  data-tooltipster='{"side":"right"}' href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>" title="Compartir en Facebook" target="_blank"><i class="fab fa-facebook"></i></a>
		</li>
		<li class="email">
			<a class="tooltipster2"  data-tooltipster='{"side":"right"}' href="mailto:?Subject=<?php the_title();?>&amp;Body=<?php the_permalink();?>" title="Compartir por E-mail"><i class="fas fa-envelope"></i></a>
		</li>
	</div>
</div>
<div class="solo-celular">
	<div class="footer-compartir">
		<div class="icono-compartir-footer">
			<span class="fa-layers fa-fw" style="font-size:40px;">
				<i class="fas fa-circle" style="color:#e24d2d"></i>
				<i class="fa-inverse fas fa-share-alt" data-fa-transform="shrink-6"></i>
			</span>
		</div>
	</div>
	<div class="compartir-iconos">
		<p>Compartir</p>
		<li class="twitter">
			<a class="tooltipster2"  data-tooltipster='{"side":"right"}' href="https://twitter.com/share?url=<?php the_permalink();?>" title="Compartir en Twitter" target="_blank">
				<i class="fab fa-twitter"></i>
			</a>
		</li>
		<li class="facebook">
			<a class="tooltipster2"  data-tooltipster='{"side":"right"}' href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>" title="Compartir en Facebook" target="_blank"><i class="fab fa-facebook"></i></a>
		</li>
		<li class="email">
			<a class="tooltipster2"  data-tooltipster='{"side":"right"}' href="mailto:?Subject=<?php the_title();?>&amp;Body=<?php the_permalink();?>" title="Compartir por E-mail"><i class="fas fa-envelope"></i></a>
		</li>
		<li class="whatsapp">
			<a class="tooltipster2"  data-tooltipster='{"side":"bottom"}' href="whatsapp://send?text=<?php the_permalink();?>" data-action="share/whatsapp/share" title="Compartir en WhatsApp">
				<i class="fab fa-whatsapp"></i>
			</a>
		</li>
	</div>
</div>
<?php endif;?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="logo">
					<a href="<?php bloginfo('url');?>" class="tooltipster" title="Ir al Inicio"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logo.svg" alt="<?php bloginfo('name');?>" class="img-responsive"></a>
				</div>
			</div>
			<div class="col-md-6">
				<p>
					<a href="https://goo.gl/maps/giB3uX9S9fk" target="_new"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/googlemaps.svg" class="img-responsive icon-svg" alt="Google Maps" style="max-width:25px;"/></a>
					<a href="https://goo.gl/maps/giB3uX9S9fk" target="_blank">Av. Santa María 2450, piso 4 Providencia, Santiago, Chile</a> <a href="tel:+56224903200">(56) 2 2490 3200</a>
				</p>
			</div>
			<div class="col-md-4">
				<div class="social-networks">
					<a href="https://www.youtube.com/user/AvellanedaVideos" title="Síguenos en YouTube" target="_blank" class="social-networks__youtube tooltipster">
						<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/youtube.svg" class="img-responsive" alt="Youtube"/>
					</a>
					<a href="https://www.instagram.com/inmobiliariaavellaneda/" title="Síguenos en Instagram" target="_blank" class="social-networks__instagram tooltipster">
						<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/instagram.svg" class="img-responsive" alt="Instagram" />
					</a>
					<a href="https://www.facebook.com/InmobiliariaAvellaneda" title="Síguenos en Facebook" class="social-networks__facebook tooltipster" target="_blank">
						<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/facebook.svg" class="img-responsive" alt="Facebook" />
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="pie">
		<div class="container">
			<strong>Avellaneda</strong> <?php echo date('Y'); ?>.
			Todos los derechos reservados. <span class="politica" data-toggle="modal" data-target="#politica">Política de privacidad</span>
		</div>
	</div>
</footer>
<footer class="celular">
	<div class="container">
		<div class="text-center">
			Av. Santa María 2450, piso 4.</br>
			Providencia, Santiago, Chile.
			<hr></hr>
			<div class="col-md-12">
				<a href="waze://?ll=-33.4182577,-70.6123402&amp;navigate=yes" target="_new"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/waze2.svg" class="img-responsive icon-svg" alt="Waze" /></a>
				<a href="https://goo.gl/maps/giB3uX9S9fk" target="_new"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/googlemaps.svg" class="img-responsive icon-svg" alt="Google Maps"/></a>
				<a href="tel:+56224903200"><div class="btn btn-rojo"><i class="fas fa-phone"></i> +56 2 2490 3200</div></a>
			</div>
		</div>
	</div>
	<div class="pie">
		<div class="container">
			<strong>Avellaneda</strong> <?php echo date('Y'); ?>.</br>
			Todos los derechos reservados. </br>
			<span class="politica" data-toggle="modal" data-target="#politica">Política de privacidad</span>
		</div>
	</div>
</footer>
<div id="oqo" style="display:none; background:#333333; position: fixed; top:0; bottom:0; left:0; right:0; z-index:900">
	<div class="container">
		<div style="margin:200px auto; text-align: center; color:white;">
			<!-- <img src="https://www.oqo.cl/wp-content/uploads/2017/09/oqo-branding-y-marketing-neg-1.gif" alt="OQO Digital" style="width:197px; display:block; margin:30px auto;"> -->
		</div>
	</div>
</div>
<?php get_template_part('modulos/popup');?>
<?php wp_footer(); ?>
</div>
</body>
</html>