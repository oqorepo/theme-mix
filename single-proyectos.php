<?php get_header(); ?>
<?php 
$noAdd = get_field('ingresa_el_correo_donde_quieres_que_lleguen_las_solicitudes');
$idProyectoMailing = get_the_ID();
?>
<script>
$(document).ready(function() {
$('.head').css('border', '0px');
$('.linea-menu-bot-fija').addClass('activo');
// Google Tag Manager
dataLayer.push({
'Proyecto': '<?php the_title();?>',
'tipoPagina': 'Proyecto',
});
var noAdd = "<?php echo $noAdd;?>";
$('input[name="EmailDB"]').val(noAdd);


// Remarketing

$('input[name="MailingMetraje"]').val('<?php the_field('metraje_marketing');?>');
$('input[name="MailingPrecio"]').val('<?php the_field('precio_marketing');?>');
$('input[name="MailingImgPrincipal"]').val('<?php the_field('imagen_destacada');?>');
$('input[name="MailingImgPiloto"]').val('<?php the_field('imagen_piloto');?>');
$('input[name="MailingPDF"]').val('<?php the_field('sube_el_ebook_digital');?>');
$('input[name="MailingEmail"]').val('<?php the_field('correo_electronico');?>');
$('input[name="MailingMetrajeProyecto"]').val('<?php the_field('metraje_marketing');?>');
$('input[name="MailingPrecioProyecto"]').val('<?php the_field('precio_marketing');?>');
$('input[name="MailingIDProyecto"]').val('<?php echo $idProyectoMailing;?>');



});
    
</script>
<?php get_template_part('secciones/header-menu');?>
<?php
$terms = get_the_terms( $post->ID , 'ubicaciones' );
if ( $terms != null ):?>
<?php
foreach( $terms as $term ) :?>
<?php if ($term->parent == "23"):?>
<script>
window.onload = function() {
$('.menu_proyectos_rm').addClass('activoactual');
$('.head').css('border-bottom', '0px');
};
</script>
<?php elseif ($term->parent == "25"):?>
<script>
window.onload = function() {
$('.menu_proyectos_re').addClass('activoactual');
$('.head').css('border-bottom', '0px');
};
</script>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
<!-- ================================================================================================================== -->
<!-- ======================================== Menu para Proyectos ======================================== -->
<!-- ================================================================================================================== -->
<div class="menu-fixed">
	<div class="nombre-proyecto-menu">
		<div class="container">
			<h4>
				<?php the_title();?>
				<div class="estado-proyecto-small estado-normal">
					<?php
				// variables
				$proyecto_externo = get_field('proyecto_externo');
				$field_color = get_field_object('av_tag_color');
				$value_color = $field_color['value'];
				$label_color = $field_color['choices'][ $value_color ];
				$tag = get_field('selecciona_tag_del_proyecto');
				$tag_personalizado = get_field('personalizar_tag');
				if ($tag == "Personalizar"):?>
					<span class="label label-default"><?php echo $tag_personalizado;?></span>
					<?php elseif ($tag == "Normal"):?>
					<?php elseif ($tag == "Slide personalizado"):?>
					<?php else:?>
					<span class="label label-default"><?php echo $tag;?></span>
					<?php endif;?>
				</div>
			</h4>
		</div>
	</div>
	<a href="#formulario-solicitar-informacion" class="click2" onclick="dataLayer.push({'link': 'Ver detalle'});">
		<div class="menu-cotizar menu-cotizar-celular">
			<div class="botones-menu-fixed">
				<div class="iconos-menu-fixed">
					<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/contacto.svg" class="img-responsive icon-solicitar" alt="Solicitar Información de <?php the_title();?>" />
				</div>
				<p><strong>Solicitar</strong> Información</p>
			</div>
		</div>
	</a>
	<!-- Menu Interno -->
	<div class="container">
		<div class="logo">
			<a href="<?php bloginfo('url');?>"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logo.svg" class="img-responsive" alt="<?php bloginfo('name');?>" alt="<?php bloginfo('name');?>"></a>
		</div>
		<ul class="menu-principal menu-proyectos">
			<?php if( have_rows('slide_fotografias_galeria_finales') ):?><li><a href="#caracteristicas" class="click atributos">Características</a></li><?php endif;?>
			<?php if( have_rows('crear_tabs')):?><li><a href="#equipamiento" class="click equipamiento">EQUIPAMIENTO</a></li><?php endif;?>
			<?php
			$query = new WP_Query(array(
										'post_type'      	=> 'plantas',
										'posts_per_page'	=> -1,
												'post_status'		=> 'publish',
								'meta_query' => array(
													array(
													'key' => 'vincular_planta_a_proyecto',
													'value' => '' . get_the_ID() . '',
													'compare' => '='
													)
												),
														'meta_key'			=> 'dormitorios_para_filtrar',
														'orderby'			=> 'meta_value',
																'order'				=> 'ASC'
			));
			?>
			<?php if ( $query->have_posts() ) : ?>
			<li><a href="#plantas" class="click plantas">TIPOS DE PLANTAS</a></li>
			<?php endif;?>
			<?php $mapa_id = get_field('vincular_mapa'); if ($mapa_id):?>
			<li><a href="#ubicacion" class="click ubicacion">UBICACIÓN</a></li>
			<?php endif;?>
		</ul>
	</div>
</div>
<!-- ================================================================================================================== -->
<!-- ======================================== Título ======================================== -->
<!-- ================================================================================================================== -->
<section class="seccion-oscura">
	<div class="container">
		<h1 class="display blanco">
		<?php the_title();?></h1>
	</div>
</section>
<!-- ================================================================================================================== -->
<!-- ======================================== Sección Big Slides ======================================== -->
<!-- ================================================================================================================== -->
<?php if( have_rows('slides_principal') ):?>
<section class="inicio">
	<!-- Cargador de Slides -->
	<div class="loading_slides loading_slides_inicio">
		<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/loading.svg" alt="Cargando <?php the_title();?>" />
	</div>
	<!-- Cargador de Slides -->
	<div class="big-slide-contenedor">
		<div class="owl-carousel owl-theme owl-bigslide">
			<?php while ( have_rows('slides_principal') ) : the_row();
				$fotografia			= get_sub_field('fotografia_slide');
				$fotografia_mobile	= get_sub_field('fotografia_mobile');
				$size				= 'slides';
				$image_array		= wp_get_attachment_image_src($fotografia, $size);
				$image_array_mobile	= wp_get_attachment_image_src($fotografia_mobile, $size);
				$video				= get_sub_field('video_slide');
				$tag				= get_field('selecciona_tag_del_proyecto');
				$tag_personalizado	= get_field('personalizar_tag');
			?>
			<!-- Slide -->
			<div class="item_slide">
				<!-- Tag del Proyecto  -->
				<?php
				if ($tag == "Personalizar"):?>
				<div class="container">
					<div class="estado-wrap">
						<div class="estado-de-proyecto"><?php echo $tag_personalizado;?></div>
					</div>
				</div>
				<?php elseif ($tag == "Slide personalizado"):?>
				<?php if (get_sub_field('sv_bs_texto_del_tag')): ?>
				<div class="container">
					<div class="estado-wrap">
						<div class="estado-de-proyecto"
							style="background:<?php the_sub_field('sv_bs_color_del_tag'); ?>;">
							<?php the_sub_field('sv_bs_texto_del_tag');?></div>
					</div>
				</div>
				<?php endif;?>
				<?php elseif ($tag == "Normal"):?>
				<?php else:?>
				<div class="container">
					<div class="estado-wrap">
						<div class="estado-de-proyecto"><?php echo $tag;?></div>
					</div>
				</div>
				<?php endif;?>
				<!-- Fotografía -->
				<picture>
					<source media="(max-width: 920px)" srcset="<?php echo $image_array_mobile[0];?>">
					<img src="<?php echo $image_array[0];?>" class="img-responsive" alt="<?php the_title();?>">
				</picture>
			</div>
			<!-- Slide -->
			<?php endwhile;?>
			<?php wp_reset_postdata(); ?>
		</div>
		<!-- Controles Slide -->
		<div id="controles-slides">
			<div class="siguiente">
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Siguiente" class="tooltipster2 big-slide-siguiente" title="Ver Siguiente" />
			</div>
			<div class="anterior">
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Anterior" class="tooltipster2 big-slide-anterior" title="Ver Anterior" />
			</div>
		</div>
		<!-- Fin Controles Slide -->
	</div>
</section>
<?php else: endif;?>
<!-- ================================================================================================================== -->
<!-- ======================================== Sección Descripción y Logo ======================================== -->
<!-- ================================================================================================================== -->
<?php
$logo = get_field('logo_proyecto');
$logo_array = wp_get_attachment_image_src($logo, 'logo');
$logo_url = $logo_array[0];
$entrega = get_field('entrega');
$bloque_rojo = get_field('bloque_rojo');
?>
<div class="big-slide-contenedor">
	<section class="seccion-oscura descripcion-proyecto">
		<div class="container">
			<div class="row">
				<!-- Logo -->
				<?php if ($logo_url):?>
				<div class="col-md-2">
					<div class="avatar-proyecto borde-redondo pos-absolute animated fadeInDown">
						<img src="<?php echo $logo_url;?>" class="img-responsive borde-redondo" alt="<?php the_title();?>"  />
					</div>
				</div>
				<!-- Ubicación -->
				<div class="col-md-6">
					<div class="bloque-gris">
						<div class="solo-desktop">
							<h2><div class="ubicacion-actual">Ubicación</div> <?php get_template_part('modulos/ubicacion-actual');?>
							</h2>
						</div>
						<div class="solo-celular text-center">
							<span class="ubicacion-actual">Ubicación <?php get_template_part('modulos/ubicacion-actual');?>
							</span>
						</div>
					</div>
				</div>
				<?php else:?>
				<div class="col-md-8">
					<div class="bloque-gris">
						<p><strong>
						UBICACIÓN</strong></p>
						<h3><?php get_template_part('modulos/ubicacion-actual');?>
						</h3>
					</div>
				</div>
				<?php endif;?>
				<!-- UF y Llamada -->
				<div class="col-md-4">
					<div class="bloque-rojo">
						<?php echo $bloque_rojo;?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- ================================================================================================================== -->
<!-- ======================================== Sección Mensaje ======================================== -->
<!-- ================================================================================================================== -->
<?php $mensaje = get_field('mensaje');
if ($mensaje):?>
<section class="gris-claro">
	<div class="container">
		<div class="frase">
			<?php the_field('mensaje');?>
		</div>
	</div>
</section>
<?php endif;?>
<!-- ================================================================================================================== -->
<!-- ======================================== Sección Fotos Características ======================================== -->
<!-- ================================================================================================================== -->
<section id="caracteristicas">
	<!-- Título galería -->
	<?php if (get_field('titulo_seccion_galeria') ):?>
	<div class="container">
		<?php if (get_field('titulo_seccion_galeria')):?>
		<h2 class="titulos">
		<?php the_field('titulo_seccion_galeria');?>
		</h2>
		<?php endif;?>
		<?php if (get_field('sub-titulo_seccion_galeria')):?>
		<h4 class="bajadas"><?php the_field('sub-titulo_seccion_galeria');?></h4>
		<?php endif;?>
	</div>
	<?php endif;?>
	<!-- Sección características solo desktop -->
	<div class="solo-desktop">
		<?php if( have_rows('slide_fotografias_galeria_finales') ):?>
		<!-- Cargador de Slides -->
		<div class="loading_slides loading_slides_proyectos">
			<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/loading.svg" alt="Cargando <?php the_title();?>" />
		</div>
		<!-- Fin Cargador de Slides -->
		<div class="owl-carousel owl-theme owl-proyectos">
			<?php
			while ( have_rows('slide_fotografias_galeria_finales') ) : the_row();
			// Variables
			$descripcion_carac = get_sub_field('descripcion_galeria');
			$fotografia_carac = get_sub_field('fotografia_galeria');
			$size_carac = 'fotografias';
			$size_carac_mobile = 'fotografias-equipamiento';
			$image_array_carac = wp_get_attachment_image_src($fotografia_carac, $size_carac);
			$image_array_carac_mobile = wp_get_attachment_image_src($fotografia_carac, $size_carac_mobile);
			$tipo_de_multimedia = get_sub_field('tipo_de_multimedia');
			$video_carac = get_sub_field('video_galeria');
			?>
			<?php if ($tipo_de_multimedia === "Fotografía"):?>
			<div class="slide-imagen">
				<div class="fotografia-fondo" style="background:url('<?php echo $image_array_carac_mobile[0];?>'); background-size: cover; background-repeat:no-repeat;">
				</div>
				<div class="foto_galeria">
					<div class="container">
						<picture>
							<source
							media="(max-width: 920px)"
							srcset="<?php echo $image_array_carac_mobile[0];?>">
							<img src="<?php echo $image_array_carac[0];?>" class="img-responsive imagen-slider-slider" alt="<?php the_title();?>">
						</picture>
					</div>
				</div>
				<?php if ($descripcion_carac):?>
				<div class="galerias_wrap">
					<div class="container">
						<div class="descripcion_galeria_proyectos">
							<?php echo $descripcion_carac;?>
						</div>
					</div>
				</div>
				<?php endif;?>
			</div>
			<?php else:?>
			<div class="slide-imagen">
				<div class="fotografia-fondo" style="background:url('https://img.youtube.com/vi/<?php echo $video_carac;?>/hqdefault.jpg'); background-size: cover; background-repeat:no-repeat;">
				</div>
				<div class="foto_galeria">
					<div class="container">
						<div class="item-video" data-merge="3"><a class="owl-video" href="https://www.youtube.com/watch?v=<?php echo $video_carac;?>"></a></div>
					</div>
				</div>
			</div>
			<?php endif;?>
			<?php endwhile;?>
			<?php wp_reset_postdata(); ?>
		</div>
		<!-- Controles Slide -->
		<div id="controles-slides">
			<div class="siguiente">
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Siguiente" class="tooltipster2 proyectos-siguiente" title="Ver Siguiente" />
			</div>
			<div class="anterior">
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Anterior" class="tooltipster2 proyectos-anterior" title="Ver Anterior" />
			</div>
		</div>
		<!-- Fin Controles Slide -->
		<?php endif;?>
	</div>
	<!-- <div id="solo-desktop"> -->
	<!-- Sección características solo mobile -->
	<div class="solo-celular">
		<?php if( have_rows('slide_fotografias_galeria_finales') ):?>
		<!-- Cargador de Slides -->
		<div class="loading_slides loading_slides_caracteristicas">
			<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/loading.svg" alt="Cargando <?php the_title();?>" />
		</div>
		<!-- Cargador de Slides -->
		<div class="owl-carousel owl-theme owl-proyectos-caracteristicas">
			<?php
			while ( have_rows('slide_fotografias_galeria_finales') ) : the_row();
				// Variables
				$descripcion_carac = get_sub_field('descripcion_galeria');
				$fotografia_carac = get_sub_field('fotografia_galeria');
				$size_carac = 'fotografias-tab-cuadrado';
				$image_array_carac = wp_get_attachment_image_src($fotografia_carac, $size_carac);
				$image_array_carac_full = wp_get_attachment_image_src($fotografia_carac, 'full');
				$tipo_de_multimedia = get_sub_field('tipo_de_multimedia');
				$video_carac = get_sub_field('video_galeria');
			?>
			<?php if ($tipo_de_multimedia === "Fotografía"):?>
			<div>
				<a href="<?php echo $image_array_carac_full[0];?>" data-fancybox="caracteristicas" data-caption="<h4><?php the_title();?></h4><?php echo $descripcion_carac;?>">
					<img src="<?php echo $image_array_carac[0];?>" class="img-responsive" alt="<?php the_title();?>">
					<?php if ($descripcion_carac):?>
					<div class="descripcion-proyectos-tabs">
						<?php echo $descripcion_carac;?>
					</div>
					<?php endif;?>
				</a>
			</div>
			<?php else:?>
			<div>
				<div class="item-video" data-merge="1"><a class="owl-video" href="https://www.youtube.com/watch?v=<?php echo $video_carac;?>"></a></div>
			</div>
			<?php endif;?>
			<?php endwhile;?>
		</div>
		<div class="alert alert-info text-center" style="margin-top:20px;"><i class="fas fa-info-circle"></i> Haz click en la imagen para acercarla.</div>
		<?php endif;?>
		<!-- Controles Slide -->
		<div id="controles-slides">
			<div class="siguiente">
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Siguiente" class="tooltipster2 proyectos-caracteristicas-siguiente" title="Ver Siguiente" />
			</div>
			<div class="anterior">
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Anterior" class="tooltipster2 proyectos-caracteristicas-anterior" title="Ver Anterior" />
			</div>
		</div>
		<!-- Fin Controles Slide -->
	</div>
</section>
<!-- ================================================================================================================== -->
<!-- ======================================== Sección Tabs ======================================== -->
<!-- ================================================================================================================== -->
<section>
	<?php if (get_field('titulo_seccion_equipamiento') ):?>
	<div class="container">
		<?php if (get_field('titulo_seccion_equipamiento')):?>
		<h2 class="titulos"><?php the_field('titulo_seccion_equipamiento');?></h2>
		<?php endif;?>
		<?php if (get_field('sub-titulo_seccion_equipamiento')):?>
		<h4 class="bajadas"><?php the_field('sub-titulo_seccion_equipamiento');?></h4>
		<?php endif;?>
	</div>
	<?php endif;?>
	<div id="equipamiento" class="container">
		<?php if( have_rows('crear_tabs')): $count = 0; $count2 = 0;?>
		<div class="solo-desktop">
			<ul  class="nav nav-pills tabs-proyectos">
				<?php while ( have_rows('crear_tabs') ) : the_row();
												$tab 				= get_sub_field('titulo_tab');
						$tabpersonalizada 	= get_sub_field('personalizar');
				$count++;?>
				<?php if ($count == 1):?>
				<li class="active">
					<?php else:?>
					<li>
						<?php endif;?>
						<a id="tabProyecto" href="#<?php echo $count;?>b" data-toggle="tab">
							<?php if ($tabpersonalizada):?>
							<div class="solo-desktop">
								<?php echo $tabpersonalizada;?>
							</div>
							<?php else :?>
							<div class="solo-desktop">
								<?php  echo $tab;?>
							</div>
							<?php endif;?>
						</a>
					</li>
					<?php endwhile;?>
				</ul>
			</div>
			<div class="solo-celular">
				<ul  class="nav nav-pills tabs-proyectos tabs-celular">
					<span class="btn-siguiente-tab"><i class="fas fa-chevron-right"></i></span>
					<?php while ( have_rows('crear_tabs') ) : the_row();
					$tab = get_sub_field('titulo_tab');
					$tabpersonalizada = get_sub_field('personalizar');
					$count2++;?>
					<?php if ($count2 == 1):?>
					<li class="active">
						<?php else:?>
						<li>
							<?php endif;?>
							<a id="tabProyecto" href="#<?php echo $count2;?>c" data-toggle="tab">
								<?php if ($tabpersonalizada):?>
								<div class="tab-grande"><?php echo $tabpersonalizada;?></div>
								<?php else :?>
								<div class="tab-grande"><?php echo $tab;?></div>
								<?php endif;?>
							</a>
						</li>
						<?php endwhile;?>
						<span class="btn-anterior-tab"><i class="fas fa-chevron-left"></i></span>
					</ul>
				</div>
				<?php endif;?>
				<div class="solo-desktop">
					<?php if( have_rows('crear_tabs')):
					$count = 0;?>
					<div class="tab-content clearfix">
						<?php while ( have_rows('crear_tabs') ) : the_row();
							$tab = get_sub_field('titulo_tab');
							$tabpersonalizada = get_sub_field('personalizar');
							$descripcion = get_sub_field('descripcion');
							$descripcion_portada = get_sub_field('descripcion_de_portada');
							$fotografia_portada = get_sub_field('fotografia_de_portada');
							$size_tab = 'fotografias';
							$size_tab_mobile = 'fotografias-equipamiento';
							$image_array_portada = wp_get_attachment_image_src($fotografia_portada, $size_tab);
							$image_array_portada_mobile = wp_get_attachment_image_src($fotografia_portada, $size_tab_mobile);
							$pregunta_tabs = get_sub_field('¿tiene_mas_fotografias_la_tab');
							$videoyoutube = get_sub_field('video_youtube');
							$vista360 = get_sub_field('vista_360');
						$count++;?>
						<!-- Tab  -->
						<div class="tab-pane
							<?php if ($count == 1):?>
							active
							<?php else: endif;?>" id="<?php echo $count;?>b">
							<?php if ($fotografia_portada):?>
							<div class="slide-proyectos-tab">
								<picture>
									<source
									media="(max-width: 920px)"
									srcset="<?php echo $image_array_portada_mobile[0];?>">
									<img src="<?php echo $image_array_portada[0];?>" class="img-responsive carga-fotos" alt="<?php echo $tab;?> - <?php the_title();?>">
								</picture>
								<?php if ($descripcion_portada):?>
								<div class="descripcion_galeria_proyectos">
									<?php echo $descripcion_portada;?>
								</div>
								<?php endif;?>
							</div>
							<?php endif;?>
							<?php if ($vista360):?>
							<iframe width="100%" height="522" src="https://my.matterport.com/show/?m=<?php echo $vista360;?>" frameborder="0" allowfullscreen></iframe>
							<?php endif;?>
							<?php if ($videoyoutube):?>
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item video-youtube-tab" src="//www.youtube.com/embed/<?php echo $videoyoutube;?>" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen"
								msallowfullscreen="msallowfullscreen"
								oallowfullscreen="oallowfullscreen"
								webkitallowfullscreen="webkitallowfullscreen"></iframe>
							</div>
							<?php endif;?>
							<?php if ($pregunta_tabs == "Si"):?>
							<!-- Vistas previa galería -->
							<?php if( have_rows('fotografias_y_descripcion')):?>
							<div class="miniaturas_de_tabs_wrap">
								<?php while ( have_rows('fotografias_y_descripcion') ) : the_row();
								$descripcion_tab = get_sub_field('descripcion');
								$fotografia_tab = get_sub_field('fotografia_tabs');
								$size_tab = 'galeria';
								$image_array_tab = wp_get_attachment_image_src($fotografia_tab, $size_tab);
								?>
								<div class="miniaturas_de_tabs">
									<img src="<?php echo $image_array_tab[0];?>" class="img-responsive" alt="<?php echo $tab;?> - <?php the_title();?>" />
									<?php if ($descripcion_tab):?>
									<div class="descripcion_tab"><?php echo $descripcion_tab;?></div>
									<?php endif;?>
								</div>
								<?php endwhile;?>
								<div class="miniaturas_de_tabs">
									<img src="<?php echo $image_array_portada[0];?>" class="img-responsive portada_tab miniatura_activa_portada" alt="<?php echo $tab;?> - <?php the_title();?>" />
									<?php if ($descripcion_portada):?>
									<div class="descripcion_tab"><?php echo $descripcion_portada;?></div>
									<?php endif;?>
								</div>
							</div>
							<?php endif;?>
							<?php endif; // cierre pregunta tabs ?>
						</div>
						<?php endwhile;?>
						<?php endif;?>
					</div>
				</div>
				<!-- Tabs solo para celular -->
				<div class="solo-celular">
					<?php if( have_rows('crear_tabs')):
					$count2 = 0;?>
					<div class="tab-content clearfix">
						<?php while ( have_rows('crear_tabs') ) : the_row();
								$tab = get_sub_field('titulo_tab');
								$tabpersonalizada = get_sub_field('personalizar');
								$descripcion = get_sub_field('descripcion');
								$descripcion_portada = get_sub_field('descripcion_de_portada');
								$fotografia_portada = get_sub_field('fotografia_de_portada');
								$size_tab = 'fotografias-tab-cuadrado';
								$image_array_portada = wp_get_attachment_image_src($fotografia_portada, $size_tab);
								$image_array_portada_full = wp_get_attachment_image_src($fotografia_portada, 'full');
								$pregunta_tabs = get_sub_field('¿tiene_mas_fotografias_la_tab');
								$videoyoutube = get_sub_field('video_youtube');
								$vista360 = get_sub_field('vista_360');
						$count2++;?>
						<div class="tab-pane <?php if ($count2 == 1):?>active<?php else: endif;?>" id="<?php echo $count2;?>c">
							<?php if ($pregunta_tabs == "Si"):?>
							<?php if( have_rows('fotografias_y_descripcion')):?>
							<!-- Cargador de Slides -->
							<div class="loading_slides loading_slides_miniaturas">
								<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/loading.svg" alt="Cargando <?php the_title();?>" />
							</div>
							<!-- Cargador de Slides -->
							<div class="proyectos-slide-miniaturas_contenedor owl-carousel owl-theme owl-miniaturas">
								<!-- Fotografía de Portada -->
								<a href="<?php echo $image_array_portada_full[0];?>" data-fancybox="<?php echo $tab;?>" data-caption="<h4><?php the_title();?></h4><h5><?php echo $tab;?></h5><?php echo $descripcion_portada;?>">
									<img src="<?php echo $image_array_portada[0];?>" class="img-responsive" alt="<?php echo $tab;?> - <?php the_title();?>"/>
									<?php if ($descripcion_portada):?>
									<div class="descripcion-proyectos-tabs">
										<?php echo $descripcion_portada;?>
									</div>
									<?php endif;?>
								</a>
								<!-- Fotografía de Portada -->
								<!-- Fotografias Tabs -->
								<?php while ( have_rows('fotografias_y_descripcion') ) : the_row();
												$descripcion_tab = get_sub_field('descripcion');
												$fotografia_tab = get_sub_field('fotografia_tabs');
												$size_tab = 'fotografias-tab-cuadrado';
												$image_array_tab = wp_get_attachment_image_src($fotografia_tab, $size_tab);
												$image_array_tab_full = wp_get_attachment_image_src($fotografia_tab, 'full');
								?>
								<a href="<?php echo $image_array_tab_full[0];?>" data-fancybox="<?php echo $tab;?>" data-caption="<h4><?php the_title();?></h4><h5><?php echo $tab;?></h5><?php echo $descripcion_tab;?>">
									<img src="<?php echo $image_array_tab[0];?>" class="img-responsive" alt="<?php echo $tab;?> - <?php the_title();?>" />
									<?php if ($descripcion_tab):?>
									<div class="descripcion-proyectos-tabs">
										<?php echo $descripcion_tab;?>
									</div>
									<?php endif;?>
								</a>
								<!-- Fotografias Tabs -->
								<?php endwhile;?>
							</div>
							<div class="alert alert-info"><p class="text-center"><i class="fas fa-info-circle"></i> Haz click en la imagen para acercarla.</p></div>
							<?php endif;?>
							<?php else:?>
							<!-- Video -->
							<?php if ($videoyoutube):?>
							<div class="embed-responsive embed-responsive-16by9" style="margin-top:20px;">
								<iframe class="embed-responsive-item video-youtube-tab" src="//www.youtube.com/embed/<?php echo $videoyoutube;?>" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen"
								msallowfullscreen="msallowfullscreen"
								oallowfullscreen="oallowfullscreen"
								webkitallowfullscreen="webkitallowfullscreen"></iframe>
							</div>
							<?php endif;?>
							<!-- Video -->
							<?php if ($vista360):?>
							<iframe width="100%" height="522" src="https://my.matterport.com/show/?m=<?php echo $vista360;?>" frameborder="0" allowfullscreen></iframe>
							<?php endif;?>
							<!-- Cuando existe sólo una fotografía en el tab -->
							<?php if ($fotografia_portada):?>
							<!-- Cargador de Slides -->
							<div class="loading_slides loading_slides_miniaturas_single">
								<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/loading.svg" alt="Cargando <?php the_title();?>" />
							</div>
							<!-- Cargador de Slides -->
							<div class="owl-carousel owl-theme owl-miniaturas-single proyectos-slide-miniaturas_single_contenedor">
								<a href="<?php echo $image_array_portada_full[0];?>" data-fancybox="<?php echo $tab;?>" data-caption="<h4><?php the_title();?></h4><h5><?php echo $tab;?></h5><?php echo $descripcion_portada;?>">
									<img src="<?php echo $image_array_portada[0];?>" class="img-responsive" alt="<?php the_title();?>" />
									<?php if ($descripcion_portada):?>
									<div class="descripcion-proyectos-tabs">
										<?php echo $descripcion_portada;?>
									</div>
									<?php endif;?>
								</a>
							</div>
							<div class="alert alert-info" style="margin-top:15px;"><p class="text-center"><i class="fas fa-info-circle"></i> Haz click en la imagen para acercarla.</p></div>
							<?php endif;?>
							<!-- Cuando existe sólo una fotografía en el tab -->
							<?php endif;?>
						</div>
						<?php endwhile;?>
					</div>
					<?php endif;?>
				</div>
			</div>
		</div>
	</section>
	<!-- ================================================================================================================== -->
	<!-- ======================================== Sección Plantas ======================================== -->
	<!-- ================================================================================================================== -->
	<?php
	$query = new WP_Query(array(
		'post_type'      	=> 'plantas',
		'posts_per_page'	=> -1,
		'post_status'		=> 'publish',
		'meta_query' => array(
			array(
				'key' => 'vincular_planta_a_proyecto',
				'value' => '' . get_the_ID() . '',
				'compare' => '='
				)
			),
		'meta_key'		=> 'dormitorios_para_filtrar',
		'orderby'		=> 'meta_value title',
		'order'			=> 'ASC'
	));
	?>
	<?php if ( $query->have_posts() ) : ?>
	<?php	$proyecto = get_the_title();	?>
	<section id="plantas">
		<?php if (get_field('titulo_seccion_plantas') ):?>
		<div class="container">
			<?php if (get_field('titulo_seccion_plantas')):?>
			<h2 class="titulos"><?php the_field('titulo_seccion_plantas');?></h2>
			<?php endif;?>
			<?php if (get_field('sub-titulo_seccion_planta')):?>
			<h4 class="bajadas"><?php the_field('sub-titulo_seccion_planta');?></h4>
			<?php endif;?>
		</div>
		<?php endif;?>
		<!-- Filtros -->
		<div class="container">
			<div class="button-group filters-button-group filtros av-filter-i">
				<button class="btn btn-filtros is-checked" data-filter="*">Todos</button>
				<button class="btn btn-filtros 1dormitorio" data-filter=".1dorm">1 Dorm</button>
				<button class="btn btn-filtros 2dormitorio" data-filter=".2dorm">2 Dorm</button>
				<button class="btn btn-filtros 3dormitorio" data-filter=".3dorm">3 Dorm</button>
				<button class="btn btn-filtros 4dormitorio" data-filter=".4dorm">4 Dorm o más</button>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="plantas-grid">
					<div class="row">
						<?php while ( $query->have_posts() ) : $query->the_post();
						// Variables
						$filtros = get_field('dormitorios_para_filtrar');
						$valor = get_field('valor');
						$totalsuperficie = get_field('dormitorios_para_filtrar');
						$descripcion = get_field('descripcion_breve_pagina_del_proyecto');
						$estado = get_field('estado');
						$tipo_de_planta = get_field('tipo_de_planta');
						$fotografia_planta = get_field('fotografia_planta');
						$vincular_parent_planta = get_field('vincular_planta_a_proyecto');
						$tienestadocustom = get_field('tiene_estado_personalizado');
						$estadopersonalizado = get_field('estado_personalizado');
						?>
						<!-- Plantas -->
						<div class="bloque-plantas <?php echo $filtros;?>">
							<div class="bloque-plantas-wrap">
								<?php if ($estado == "Agotado"):?>
								<div class="box">
									<div class="ribbon ribbon-small ribbon-top-left ribbon-plantas ribbon-agotado">
										<span><?php the_field('estado');?></span>
									</div>
									<?php if ($fotografia_planta):?>
									<?php if ($tipo_de_planta == "Departamento"):
									$size_planta = 'fotografias-departamentos';
									$image_array_planta = wp_get_attachment_image_src($fotografia_planta, $size_planta);
									?>
									<img src="<?php echo $image_array_planta[0];?>" alt="Planta <?php the_title();?> - <?php echo $vincular_parent_planta->post_title;?>" class="img-responsive <?php if ($estado == "Agotado"):?>blancoynegro<?php endif;?>"/>
									<?php endif;?>
									<?php if ($tipo_de_planta == "Casa"):
									$size_planta = 'fotografias-casas';
									$image_array_planta = wp_get_attachment_image_src($fotografia_planta, $size_planta);
									?>
									<img src="<?php echo $image_array_planta[0];?>" alt="Planta <?php the_title();?> - <?php echo $vincular_parent_planta->post_title;?>" class="img-responsive <?php if ($estado == "Agotado"):?>blancoynegro<?php endif;?>"/>
									<?php endif;?>
									<?php else:?>
									<?php if ($tipo_de_planta == "Departamento"):?>
									<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto.jpg" alt="Sin Fotografía" class="img-responsive"/>
									<?php endif;?>
									<?php if ($tipo_de_planta == "Casa"):?>
									<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto3.jpg" alt="Sin Fotografía" class="img-responsive"/>
									<?php endif;?>
									<?php endif;?>
								</div>
								<?php else:?>
								<div class="box">
									<?php if ($tienestadocustom == "Si"):?>
									<div class="ribbon ribbon-small ribbon-top-left ribbon-plantas">
										<span style="font-size:10px;"><?php echo $estadopersonalizado;?></span>
									</div>
									<?php endif;?>
									<a href="<?php the_permalink(); ?>" id="verPlantas" class="tooltipster" title="Ver Detalle" onclick="dataLayer.push({'nombreProyecto': '<?php echo $proyecto;?>', 'nombrePlanta': '<?php the_title();?>'});">
										<!-- Si es una fotografía de Departamento -->
										<?php if ($fotografia_planta):?>
										<?php if ($tipo_de_planta == "Departamento"):
													$size_planta = 'fotografias-departamentos';
														$image_array_planta = wp_get_attachment_image_src($fotografia_planta, $size_planta);
										?>
										<img src="<?php echo $image_array_planta[0];?>" class="img-responsive" alt="Planta <?php the_title();?> - <?php echo $vincular_parent_planta->post_title;?>"/>
										<?php endif;?>
										<!-- Si es una fotografía de Casa -->
										<?php if (	$tipo_de_planta == "Casa"):
										$size_planta = 'fotografias-casas';
										$image_array_planta = wp_get_attachment_image_src($fotografia_planta, $size_planta);
										?>
										<img src="<?php echo $image_array_planta[0];?>" class="img-responsive" alt="Planta <?php the_title();?> - <?php echo $vincular_parent_planta->post_title;?>"/>
										<?php endif;?>
										<!-- Si la suben sin fotografía -->
										<?php else:?>
										<?php if ($tipo_de_planta == "Departamento"):?>
										<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto.jpg" class="img-responsive" alt="Sin Fotografía"/>
										<?php endif;?>
										<?php if ($tipo_de_planta == "Casa"):?>
										<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto3.jpg" alt="Sin Fotografía" class="img-responsive"/>
										<?php endif;?>
										<?php endif;?>
									</a>
								</div>
								<?php endif;?>
								<h3 class="altura-titulos"><?php the_title();?></h3>
								<?php if ($valor):?>
								<div class="detalle_planta misma-altura">
									<div class="bloque-gris">
										<small>desde</small> <strong><?php echo $valor;?> UF</strong>
									</div>
								</div>
								<?php endif;?>
								<div class="detalles-planta altura-plantas">
									<?php echo $descripcion;?>
								</div>
								<?php if ($estado == "Agotado"):?>
								<div class="cotizar-planta">
									<btn disabled class="btn btn-rojo btn-block btn-sin-borde btn-disabled">Ver Detalle</btn>
								</div>
								<?php else:?>
								<div class="cotizar-planta">
									<a href="<?php the_permalink(); ?>" id="verPlantas" onclick="dataLayer.push({'nombreProyecto': '<?php echo $proyecto;?>', 'nombrePlanta': '<?php the_title();?>'});"><btn class="btn btn-rojo btn-block btn-sin-borde">Ver Detalle</btn></a>
								</div>
								<?php endif;?>
							</div>
						</div>
						<?php endwhile;?>
						<?php wp_reset_postdata(); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="separador"></div>
	</section>
	<?php endif;?>
	<!-- ================================================================================================================== -->
	<!-- ======================================== Sección Mapa ======================================== -->
	<!-- ================================================================================================================== -->
	<?php
	$mapa_id = get_field('vincular_mapa');
	$mapa_capture = get_field('captura');
	if ($mapa_capture ):?>
	<section id="ubicacion">
		<?php if (get_field('titulo_seccion_mapa') ):?>
		<div class="container">
			<?php if (get_field('titulo_seccion_mapa')):?>
			<h2 class="titulos">
			<?php the_field('titulo_seccion_mapa');?>
			</h2>
			<?php endif;?>
			<?php if (get_field('sub_titulo_seccion_mapa')):?>
			<h4 class="bajadas"><?php the_field('sub_titulo_seccion_mapa');?></h4>
			<?php endif;?>
		</div>
		<?php endif;?>
		<?php $mensaje_mapa = get_field('mensaje_mapa');?>
		<?php if ( $mensaje_mapa):?>
		<section class="gris-claro">
			<div class="frase-mapa">
				<div class="container">
					<?php echo $mensaje_mapa;?>
				</div>
			</div>
		</section>
		<?php endif;?>
		
		<img src="<?php echo esc_url($mapa_capture['url']); ?>" alt="<?php echo esc_attr($mapa_capture['alt']); ?>" style="width:100%">

		<?php if( have_rows('bloque_de_ubicacion_personalizado') ):?>
		<div class="container" style="margin-top:30px;">
			<?php while ( have_rows('bloque_de_ubicacion_personalizado') ) : the_row();
			// variables
			$titulo_bloque = get_sub_field('titulo_del_bloque');
			$contenido_bloque = get_sub_field('contenido_del_bloque');
			$icono = get_sub_field('icono');
			?>
			<div class="col-md-4">
				<?php echo $icono;?> <strong class="altas"><?php echo $titulo_bloque;?></strong>
				<?php echo $contenido_bloque;?>
			</div>
			<?php endwhile;?>
		</div>
		<?php endif;?>
		
		<div class="separador"></div>
	</section>
	<?php endif;?>
	<!-- ================================================================================================================== -->
	<!-- ======================================== Sección Información y Formulario ======================================== -->
	<!-- ================================================================================================================== -->
	<section id="masinformacion">
		<?php
			$telefono1 = get_field('telefono');
			$telefono2 = get_field('telefono_2');
			$direccion = get_field('direccion');
			$direccion_google = get_field('ingresa_url_google_maps');
			$email = get_field('correo_electronico');
			$horario_visita = get_field('horario_visita');
			$horario_sala_de_ventas = get_field('horario_sala_de_ventas');
			$coordenadas = get_field('ingresa_coordenadas');

		?>
		<div class="part-b">
			<div class="background"></div>
			<div class="container">
				<div class="row seccion-formulario especial-mobile">
					<?php get_template_part('secciones/descargar-pdf');?>
					<div class="col-md-6 texto-blanco col-fluid-left col-rojo sala-de-ventas">
						<div class="texto-col">
							<div class="espacio-form"></div>
							<?php $bloque = get_field('bloque_html');
							if ($bloque):?>
							<?php echo $bloque;?>
							<?php endif;?>
							<?php if ($direccion):?><h3>Sala de ventas</h3><?php endif;?>
							<div class="espacio-form"></div>

<!-- Dirección -->
<?php if ($direccion):?>
	<?php $reemplazar_links = get_field('reemplazar_links_de_google_maps');?>
	<?php if ($proyecto_externo == "Si"):?>
		<?php if ($reemplazar_links):?>
			<a href="<?php echo $reemplazar_links;?>" class="tooltipster2" title="Ver Dirección" target="_blank"><i class="fas fa-map-marker-alt"></i> <?php echo $direccion;?></a><hr>
		<?php else:?>
		<a href="<?php echo $direccion_google;?>" class="tooltipster2" title="Ver Dirección en Google Maps" target="_blank"><i class="fas fa-map-marker-alt"></i> <?php echo $direccion;?></a><hr>
		<?php endif;?>

	<?php else:?>
		<a href="<?php echo $direccion_google;?>" class="tooltipster2" title="Ver Dirección en Google Maps" target="_blank"><i class="fas fa-map-marker-alt"></i> <?php echo $direccion;?></a><hr>
	<?php endif;?>
<?php endif;?>
<!-- Fin Dirección -->


<!-- Teléfono -->
<?php if ($telefono1):?>
	<a href="tel:<?php echo $telefono1;?>" class="tooltipster2" title="Llamar"><i class="fas fa-phone"></i> <?php echo $telefono1;?></a>
<?php endif;?>
<?php if ($telefono2):?> 
	- <a href="tel:<?php echo $telefono2;?>" class="tooltipster2" title="Llamar"><i class="fas fa-phone"></i> <?php echo $telefono2;?></a>
<?php endif;?>
<!-- Fin Teléfono -->

<!-- Email -->
<?php if ($email):?>
	<hr>
	<a href="mailto:<?php echo $email;?>" class="tooltipster2" title="Enviar Email"><i class="fas fa-envelope"></i> <?php echo $email;?></a>
<?php endif;?>
<!-- Fin Email -->

<!-- Horario de atención -->
<?php if ($horario_visita):?>
	<hr>
	<i class="far fa-clock"></i> <?php echo $horario_visita;?>
<?php endif;?>
<!-- Fin Horario de atención -->


<!-- Iconos Dirección -->
<?php if ($coordenadas['longitud']):?>
<hr>
		<!-- Icono Waze -->
		<div class="solo-celular">
			<?php if ($proyecto_externo == "Si"):?>
					<?php if ($reemplazar_links):?>
						<a href="<?php echo $reemplazar_links;?>" target="_new">
					<?php else:?>
						<a href="waze://?ll=<?php echo $coordenadas['longitud'];?>,<?php echo $coordenadas['latitud'];?>&amp;navigate=yes" target="_new">
					<?php endif;?>
			<?php else:?>
				<a href="waze://?ll=<?php echo $coordenadas['longitud'];?>,<?php echo $coordenadas['latitud'];?>&amp;navigate=yes" target="_new">
			<?php endif;?>
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/waze-blanco.svg" class="img-responsive icon-svg" style="margin-right:15px;" alt="Ver Dirección en Waze" />
				</a>
		</div>
		<!-- Fin Icono Waze -->

		<!-- Icono Google Maps -->
		<?php if ($proyecto_externo == "Si"):?>
			<?php if ($reemplazar_links):?>
				<a href="<?php echo $reemplazar_links;?>" target="_new">
			<?php else:?>
				<a href="http://maps.google.com/maps?daddr=<?php echo $coordenadas['longitud'];?>,<?php echo $coordenadas['latitud'];?>" target="_new">
			<?php endif;?>
		<?php else:?>
			<a href="http://maps.google.com/maps?daddr=<?php echo $coordenadas['longitud'];?>,<?php echo $coordenadas['latitud'];?>" target="_new">
		<?php endif;?>
			<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/googlemaps-blanco.svg" class="img-responsive icon-svg" alt="Google Maps" />
			</a>

		<!-- Fin Icono Google Maps -->

<?php endif;?>
<!-- Fin Iconos Dirección -->


</div>




						<!-- Archivo -->
						<?php $archivo = get_field('sube_el_ebook_digital');
						if ($archivo):?>
						<script>
						window.onload = function() {
						$(document).ready(function() {
						$('input[name="URLPDF"]').val('<?php echo $archivo;?>');
						});
						};
						</script>
						<div class="descargar-proyecto">
							<p>
								Quiero más antecedentes del proyecto.</br>
								<button class="btn btn-gris-claro" data-toggle="modal" data-target="#descargarpdf" onclick="dataLayer.push({'evento': 'Descargar Folleto Digital', 'nombreProyecto': '<?php the_title();?>'});"><i class="fas fa-file-pdf"></i> Descargar Folleto Digital</button>
							</p>
						</div>
						<div class="espacio-form"></div>
						<div class="espacio-form"></div>
						<?php endif;?>
						<!-- Archivo -->
					</div>
					<div class="col-md-6 texto-blanco col-formulario">
						<div class="texto-col col-fluid-right">
							<div id="gracias-form-solicitar-informacion">
								<div class="col-md-12">
									<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/enviado.svg" alt="Mensaje Enviado" class="img-responsive icon-enviado">
									<h3>Gracias <span class="nombre-persona"></span></h3>
									<p><strong>Tu solicitud ha sido enviada con éxito.</strong></p>
									<p>Pronto te contactaremos.</p>
								</div>
							</div>
							<div id="formulario-solicitar-informacion">
								<div class="row">
											<?php if ($tag == "Agotado"):?>
											<?php else:?>

													<?php if ($proyecto_externo == "Si"):?>

													<?php else:?>
													<div class="col-md-12">
														<h3>Solicitar Información</h3>
													</div>
													<?php endif;?>
											
											<?php endif;?>

									<?php if ($tag == "Agotado"):?>

									<div class="col-md-12 text-center" style="margin-top:100px; margin-bottom:100px;">
									<h3>Este proyecto está agotado</h3>
									<p><strong>Las solicitudes están desactivadas.</strong></p>
									<a href="<?php bloginfo('url');?>/proyectos"><button class="btn btn-rojo">Ver más proyectos</button></a>
									</div>



									<?php else:?>

										<?php if ($proyecto_externo == "Si"):?>
											<div class="col-md-12 text-center" style="margin-top:100px; margin-bottom:100px;">
											<h3>Solicita Información</h3>
											<p><strong>Para conocer más del proyecto <?php the_title();?></br>
											Visita el sitio web</strong></p>
											<a href="<?php the_field('ingresa_url_del_proyecto');?>" target="_blank"><button class="btn btn-rojo btn-block">Visitar Proyecto</button></a>
											</div>

										<?php else:?>
											<?php echo do_shortcode('[contact-form-7 id="101" title="Solicitar Información" html_id="formulario-solicitar"]');?>
										<?php endif;?>

								<?php endif;?>
								</div>
							</div>
							<!-- Formulario -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="separador"></div>
	</section>
	<?php get_footer();?>