<!-- Modal -->
<div class="modal fade" id="avisopop" tabindex="10" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-body" style="padding:0px;">
       
<?php 
$imagen = get_field('imagen_promocion_mobile');
$imagen_promo_array = wp_get_attachment_image_src($imagen, 'full');

?>

<a href="<?php the_permalink();?>" class="linkEnPopUp" onclick="dataLayer.push({'nombrePromocion': '<?php the_title();?>', 'event' : 'avisoPopUp'});">
<img src="<?php echo $imagen_promo_array[0];?>" class="img-responsive" alt="<?php the_title();?>"  />
</a>



          </div>
                     
          </div>
        </div>
      </div>
    </div>
