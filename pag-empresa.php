<?php
/**
 * Template Name: Página Empresa
*/
get_header(); 
get_template_part('secciones/header-normal');
?>






<section class="seccion-oscura">
	<div class="container">
		<h1 class="display blanco"><?php the_title();?></h1>
	</div>
</section>



<!-- ============================================================================= -->
<!-- Portada -->
<!-- ============================================================================= -->

<?php 
$imagenportada = get_field('imagen');
$size = 'corporativo';
$imagenportada_array = wp_get_attachment_image_src($imagenportada, $size);
?>
<?php if ($imagenportada):?>
<section class="fotomastexto">
	<div class="bloque-fondo-completo">
		<div class="bloque-con-fondo" style="background: url('<?php echo $imagenportada_array[0];?>') top;"></div>
		<div class="bloque-con-fondo-wrap">
			<div class="container">
				<div class="row row-eq-height fondo-rojo centrarmedio">
					<div class="col-md-8 sin-padding">
						
						<img src="<?php echo $imagenportada_array[0];?>" class="img-responsive  img-full" alt="<?php the_title();?>" />
					</div>
					<div class="col-md-4 bloque-color  font-media">
						<div class="col-md-12 text-center">
							<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logoblanco.svg" alt="<?php bloginfo('name');?>" class="logo-empresa" />
						</div>
					<?php the_field('contenido');?>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;?>



<!-- ============================================================================= -->
<!-- Hitos -->
<!-- ============================================================================= -->
<?php if( have_rows('hito') ):?>
<section class="destacados">
	<div class="container">
		<div class="row">
			<?php while ( have_rows('hito') ) : the_row();
			$icono = get_sub_field('icono');
			$titulo = get_sub_field('titulo');
			$texto = get_sub_field('texto');
			?>
			<div class="col-md-4 destacados" style="margin-bottom:15px;">
				<img src="<?php echo $icono;?>" class="img-responsive icono-destacado" alt="Hitos Destacados Avellaneda" />
				<h2><?php echo $titulo;?></h2>
				<div class="bajo-destacado"><?php echo $texto;?></div>
				
			</div>
			<?php endwhile;?>
			
		</div>
	</div>
</section>
<?php endif;?>


<!-- ============================================================================= -->
<!-- Visión -->
<!-- ============================================================================= -->

<?php 
		$mision = get_field('mision');
		$vision = get_field('vision');
		?>

<?php if ($mision && $vision):?>
<section class="misionyvision">
<div class="container">
	<div class="row">
		
		<?php if ($mision):?>
			<div class="col-md-6" style="margin-bottom:30px;">
			<h3 class="text-center font-media"><strong>Misión</strong></h3>
			<?php the_field('mision');?>
		</div>
	<?php endif;?>
	<?php if ($vision):?>

		<div class="col-md-6" style="margin-bottom:30px;">
				<h3 class="text-center font-media"><strong>Visión</strong></h3>
			<?php the_field('vision');?>
		</div>
	<?php endif;?>
	</div>
</div>
</section>
<?php endif;?>



<!-- ============================================================================= -->
<!-- Artículos -->
<!-- ============================================================================= -->
<?php
$ids = get_field('selecciona_articulos_corporativos', false, false);
$query = new WP_Query(array(
	'post_type' => array('corporativo'),
	'posts_per_page'	=> -1,
	'post__in'			=> $ids,
	'post_status'		=> 'publish',
	'orderby'        	=> 'post__in',
));

?>

<?php if ( $query->have_posts() ) : 
$count = 0;?>

<?php while ( $query->have_posts() ) : $query->the_post(); ?>
<?php
$count++;
$imagenportada = get_field('imagen');
$size = 'corporativo';
$imagenportada_array = wp_get_attachment_image_src($imagenportada, $size);
$contenido = get_field('contenido');
$tipocontenido = get_field('tipo_de_contenido');
$videoid = get_field('ingresa_id_de_youtube');

?>

<?php if ($tipocontenido == "Texto"):?>

<section class="fotomastexto">
	<div class="bloque-fondo-completo">
		<div class="bloque-con-fondo"></div>
		<div class="bloque-con-fondo-wrap">
			<div class="container">
				<div class="row row-eq-height fondo-rojo centrarmedio">
					
<?php if ($count % 2 == 0) :?>

<div class="col-md-8 sin-padding col-lg-push-4">
	<img src="<?php echo $imagenportada_array[0];?>" class="img-responsive img-full" alt="<?php the_title();?>" />
</div>

<div class="col-md-4 bloque-color  font-media col-lg-pull-8">
	<h2 class="titulos"><?php the_title();?></h2>
	<div style="font-size:12px !important;"><?php echo $contenido;?></div>
</div>





<?php else:?>

<div class="col-md-8 sin-padding">
	<img src="<?php echo $imagenportada_array[0];?>" class="img-responsive  img-full" alt="<?php the_title();?>" />
</div>

<div class="col-md-4 bloque-color  font-media">
	<h2 class="titulos"><?php the_title();?></h2>
	<?php echo $contenido;?>
</div>

<?php endif;?>


					
					
					
					
				</div>
			</div>
		</div>
	</div>
</section>

<?php endif;?>



<?php if ($tipocontenido == "Video"):?>


<section class="fotomastexto">
	<div class="bloque-fondo-completo">
		<div class="bloque-con-fondo"></div>
		<div class="bloque-con-fondo-wrap">
			<div class="container">
				<div class="row row-eq-height fondo-rojo centrarmedio">
					
<?php if ($count % 2 == 0) :?>

<div class="col-md-8 sin-padding col-lg-push-4">
<div class="embed-responsive embed-responsive-16by9">
					<iframe allowfullscreen="allowfullscreen" allowfullscreen class="embed-responsive-item" src="//www.youtube.com/embed/<?php echo $videoid;?>"></iframe>
				</div>
</div>

<div class="col-md-4 bloque-color  font-media col-lg-pull-8">
	<h2 class="titulos"><?php the_title();?></h2>
	<div style="font-size:12px !important;"><?php echo $contenido;?></div>
</div>





<?php else:?>

<div class="col-md-8 sin-padding">
<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item"  allowfullscreen="allowfullscreen" allowfullscreen src="//www.youtube.com/embed/<?php echo $videoid;?>"></iframe>
				</div>
</div>

<div class="col-md-4 bloque-color  font-media">
	<h2 class="titulos"><?php the_title();?></h2>
	<?php echo $contenido;?>
</div>

<?php endif;?>


					
					
					
					
				</div>
			</div>
		</div>
	</div>
</section>










<!-- <section class="fotomastexto" style="margin-top:15px;">
	<div class="bloque-con-fondo" style="background: url('http://i.ytimg.com/vi/<?php echo $videoid;?>/maxresdefault.jpg') top;"></div>
	<div class="container">
		
		<div class="videocorporativo-wrap">
			<div class="videocorporativo">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="//www.youtube.com/embed/<?php echo $videoid;?>"></iframe>
				</div>
			</div>
		</div>
		
		
		
		
	</div>
</section> -->
<?php endif;?>


<?php endwhile;?>
<?php endif;?>








<!--=====================================
=            Logos 			            =
======================================-->
<?php $logos = new WP_Query(array(
	'post_type' 		=> array('logos'),
	'posts_per_page'	=> -1,
	'post_status'		=> 'publish',
	'orderby'        	=> 'post__in',
));
if ( $logos->have_posts() ) : ?>
<section class="bloque-logos-corporativos solo-desktop">
	<div class="container">
		<h2 class="text-center">Socios y Alianzas Corporativas</h2>
		<div class="owl-carousel owl-theme owl-logos">
			<?php while ( $logos->have_posts() ) : $logos->the_post(); ?>
			<?php 		// variables
			$logo 		= get_field('agregar_imagen_logo');
			$logo_array = wp_get_attachment_image_src($logo, 'full');
			$url 		= get_field('url');
			?>
	
				<a href="<?php echo $url;?>" target="_blank">
					<img src="<?php echo $logo_array[0];?>" alt="<?php the_title();?>" class="img-responsive" />
				</a>

	
			<?php endwhile;?>
		</div>
	</div>
</section>

<section class="bloque-logos-corporativos solo-celular">
	<div class="container">
		<h2 class="text-center">Socios y Alianzas Corporativas</h2>
		<div class="owl-carousel owl-theme owl-logos">
			<?php while ( $logos->have_posts() ) : $logos->the_post(); ?>
			<?php 		// variables
			$logo 		= get_field('agregar_imagen_logo');
			$logo_array = wp_get_attachment_image_src($logo, 'full');
			$url 		= get_field('url');
			?>
	
			
					<img src="<?php echo $logo_array[0];?>" alt="<?php the_title();?>" class="img-responsive" />
			

	
			<?php endwhile;?>
		</div>
	</div>
</section>
<?php endif;?>
<?php wp_reset_postdata(); ?>

<!-- ============================================================================= -->
<!-- Proyectos -->
<!-- ============================================================================= -->

<section class="proyectos">
	<div class="separador-de-seccion">
		<div class="container">
			<div class="row">
				<h2 class="titulos">Proyectos en Santiago</h2>
				<?php
				$_posts = new WP_Query( array(
				'post_type'         => 'proyectos',
				'posts_per_page'    => -1,
				'post_status'		=> 'publish',
					'tax_query' => array(
					array(
					'taxonomy' => 'ubicaciones',
					'terms'    => '23'),
					),
				));
				if( $_posts->have_posts() ) :
				while ( $_posts->have_posts() ) : $_posts->the_post();
				$pronto = get_field('url_landing_proyecto');
				$fotografia_menu = get_field('foto_portada_para_menu');
				$size = 'foto-menu-col4';
				$image_array = wp_get_attachment_image_src($fotografia_menu, $size);
				?>
				
				<div class="col-md-2 bloque-proyecto-menu">
					
					<?php
					$fotografia = get_field('foto_portada_para_menu');
						$size = 'foto-menu-col4';
						$image_array = wp_get_attachment_image_src($fotografia, $size);
						
					?>
					<div class="estado-proyecto-small-menu">
						<?php
						// variables
						$tag					= get_field('selecciona_tag_del_proyecto');
						$tag_personalizado 		= get_field('personalizar_tag');
						$slide_personalizado 	= get_field('av_tag_menu_txt');
						if ($tag == "Personalizar"):?>
						<span class="label label-default"><?php echo $tag_personalizado;?></span>
						<?php elseif ($tag == "Normal"):?>
						<?php elseif ($tag == "Slide personalizado"):?>
						<span class="label label-default"><?php echo $slide_personalizado ;?></span>
						<?php else:?>
						<span class="label label-default"><?php echo $tag;?></span>
						<?php endif;?>
					</div>
					
					<?php if ($pronto):?>
					<a href="<?php echo $pronto;?>" target="_blank" class="tooltipster" title="Ver Proyecto" id="menuProyectos" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
						<?php else:?>
						<a href="<?php the_permalink();?>" class="tooltipster" title="Ver Proyecto" id="menuProyectos" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
							<?php endif;?>
							<?php if ($fotografia):?>
							<img src="<?php echo $image_array[0];?>" class="img-responsive img-full" alt="<?php the_title();?>" />
							<?php else:?>
							<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto2.jpg" class="img-responsive img-full" alt="Sin Fotografía"/>
							<?php endif;?>
							
							
							<div class="listado_de_proyectos misma-altura-proyectos">
								<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
								<h4><?php the_title(); ?></h4>
								
							</div>
						</a>
					</div>
					<?php
					endwhile;
					endif;
					wp_reset_postdata();
					?>
				</div>
			</div>
		</div>



		<div class="separador-de-seccion">
			<div class="container">
				<div class="row">
					<h2 class="titulos">Proyectos en Regiones</h2>
					<?php
					$_posts = new WP_Query( array(
					'post_type'         => 'proyectos',
					'post_status'		=> 'publish',
					'posts_per_page'    => -1,
						'tax_query' => array(
						array(
						'taxonomy' => 'ubicaciones',
						'terms'    => '25'),
						),
					));
					if( $_posts->have_posts() ) :
					while ( $_posts->have_posts() ) : $_posts->the_post();
					$pronto = get_field('url_landing_proyecto');
					$fotografia_menu = get_field('foto_portada_para_menu');
					$size = 'foto-menu-col4';
					$image_array = wp_get_attachment_image_src($fotografia_menu, $size);
					?>
					
					<div class="col-md-2 bloque-proyecto-menu">
						
						<?php
						$fotografia = get_field('foto_portada_para_menu');
							$size = 'foto-menu-col4';
							$image_array = wp_get_attachment_image_src($fotografia, $size);
							
						?>
						<div class="estado-proyecto-small-menu">
						<?php
							$tag					= get_field('selecciona_tag_del_proyecto');
							$tag_personalizado 		= get_field('personalizar_tag');
							$slide_personalizado 	= get_field('av_tag_menu_txt');
						if ($tag == "Personalizar"):?>
						<span class="label label-default"><?php echo $tag_personalizado;?></span>
						<?php elseif ($tag == "Normal"):?>
						<?php elseif ($tag == "Slide personalizado"):?>
						<span class="label label-default"><?php echo $slide_personalizado ;?></span>
						<?php else:?>
						<span class="label label-default"><?php echo $tag;?></span>
						<?php endif;?>
						</div>
						
						<?php if ($pronto):?>
						<a href="<?php echo $pronto;?>" target="_blank" class="tooltipster" title="Ver Proyecto" id="menuProyectos" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
							<?php else:?>
							<a href="<?php the_permalink();?>" class="tooltipster" title="Ver Proyecto" id="menuProyectos" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">
								<?php endif;?>
								<?php if ($fotografia):?>
								<img src="<?php echo $image_array[0];?>" class="img-responsive img-full" alt="<?php the_title();?>" />
								<?php else:?>
								<img src="<?php bloginfo('template_url');?>/recursos/img/sinfoto2.jpg" class="img-responsive img-full" alt="Sin Fotografías"/>
								<?php endif;?>
								
								
								<div class="listado_de_proyectos misma-altura-proyectos">
									<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
									<h4><?php the_title(); ?></h4>
									
								</div>
							</a>
						</div>
						<?php
						endwhile;
						endif;
						wp_reset_postdata();
						?>
					</div>
				</div>
			</div>
		</section>




<?php get_footer();?>
