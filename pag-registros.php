<?php
/**
 * Template Name: Registros
*/
setlocale (LC_TIME, 'es_ES.utf8','ES'); 
get_header(); 
get_template_part('secciones/header-registros');
?>

<?php
if (! is_user_logged_in() ) :?>

<?php auth_redirect();?>

<?php else:?>




<section class="pagina-registros">


<section class="seccion-oscura">
	<div class="container">
		<h1 class="display blanco">


			<?php the_title();?></h1>
	</div>
</section>


	  <ol class="breadcrumb" style="margin-top:0px; border-radius:0px;">

    <li><a href="<?php bloginfo('url');?>/registros/">Registros</a></li>
    <li class="active"><?php the_title();?></li>

 
  </ol>



<!-- <div class="col-md-12" style="margin-bottom:30px;">


<h1>Exportar</h1>


<p>Selecciona Meses</p>


</div>


<hr></hr> -->


<div class="col-md-12">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<h1>Registros</h1>
<hr></hr>

<?php the_content();?>

<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

</div>

</section>







<?php get_footer();?>


<?php endif;?>