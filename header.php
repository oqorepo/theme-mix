<!doctype html>
<html lang="es">
	<head>
		<script>
		function resizeIframe(obj) {
		obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
		}
		</script>
		<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@id": "https://www.avellaneda.cl",
				"@type": "Organization",
				"name": "Inmobiliaria Avellaneda",
				"url": "https://www.avellaneda.cl/",
				"logo": "https://www.avellaneda.cl/web/wp-content/themes/avellaneda/recursos/img/logo.png",
				"contactPoint": [
					{
						"@type": "ContactPoint",
						"telephone": "+56-2-2490-3200",
						"contactType": "sales",
						"areaServed": [ "CL" ]
					}
				],
				"sameAs": [
					"https://www.youtube.com/user/AvellanedaVideos",
					"https://www.facebook.com/InmobiliariaAvellaneda",
					"https://www.instagram.com/inmobiliariaavellaneda/"
				]
			}
		</script>
		<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
		<?php endif; ?>
		<!-- Page-hiding snippet (recommended)  -->
        <style>.async-hide { opacity: 0 !important} </style>
        <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
        h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
        (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',4000,
        {'GTM-PVL62N3':true});</script>
        <!-- Modified Analytics code with Optimize plugin -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-3721793-1', 'auto');  // Update tracker settings 
            ga('require', 'GTM-PVL62N3');           // Add this line
        </script>
        <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkVd6hg93FK8s6RAS0DjEgKdR_gaR2UXs"></script>
<script type="text/javascript" src="https://maps.stamen.com/js/tile.stamen.js?v1.3.0"></script>
  <script>$ = jQuery.noConflict(false);</script>
  <script type='text/javascript' src='https://www.avellaneda.cl/wp-content/plugins/wp-google-map-gold/assets/js/maps.js?ver=2.3.4' defer onload=''></script>
		<script type='text/javascript' src='https://www.avellaneda.cl/wp-includes/js/wp-embed.min.js?ver=4.9.10' defer onload=''></script>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-5L5DZZF');</script>
		<!-- End Google Tag Manager -->
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php if( is_front_page() ) { } else { ?><?php wp_title('-','true','right'); ?><?php if ( is_single() ) ?><?php bloginfo('name'); ?><?php } ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="author" content="Agencia OQO">
		<meta name="generator" content="WordPress 4.5.3" />
		<meta name="apple-mobile-web-app-title" content="Avellaneda">
		<meta name="theme-color" content="#e5302d">
		<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url');?>/recursos/img/iconos/57px.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url');?>/recursos/img/iconos/60px.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url');?>/recursos/img/iconos/72px.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url');?>/recursos/img/iconos/114px.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url');?>/recursos/img/iconos/120px.png">
		<link rel="icon" sizes="128×128" href="<?php bloginfo('template_url');?>/recursos/img/iconos/128px.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url');?>/recursos/img/iconos/144px.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url');?>/recursos/img/iconos/152px.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url');?>/recursos/img/iconos/180px.png">
		<link rel="icon" sizes="192x192" href="<?php bloginfo('template_url');?>/recursos/img/iconos/192px.png">
		<link rel="shortcut icon" href="<?php bloginfo('template_url');?>/recursos/img/iconos/favicon.png">
		<?php wp_head(); ?>

		<!-- ----------------------------- -->
		<!-- ----------------------------- -->
		<!-- Etiquetas Open Graph -->
		<!-- ----------------------------- -->
		<!-- ----------------------------- -->
		<!-- Si son Proyectos -->
		<?php if (is_singular('proyectos')):
				$fotografia 	= get_field('foto_portada_para_menu');
				$image_array 	= wp_get_attachment_image_src($fotografia, 'full');
		?>
		<meta property="og:image" content="<?php echo $image_array[0];?>">
		<meta name="twitter:image" content="<?php echo $image_array[0];?>">
		<?php endif;?>
		<!-- Si son Plantas -->
		<?php if (is_singular('plantas')):
			$planta 				= get_field('fotografia_planta');
			$planta_array 			= wp_get_attachment_image_src($planta, 'full');
			$descripcion_planta		= get_field('descripcion_breve_pagina_del_proyecto');
			$descripcion_formato 	= sanitize_text_field($descripcion_planta);
			$supertotal 			= get_field('superficie_total');
		?>
		<meta property="og:image" content="<?php echo $planta_array[0];?>">
		<meta property="og:description" content="<?php echo $descripcion_formato;?><?php if ($supertotal):?> - Superficie Total: <?php echo $supertotal;?> m²<?php endif;?>">
		<meta property="og:title" content="Planta <?php the_title();?> &mdash; Avellaneda - Inmobiliaria y Constructora">
		<meta name="twitter:title" content="Planta <?php the_title();?> &mdash; Avellaneda - Inmobiliaria y Constructora">
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:image" content="<?php echo $planta_array[0];?>">
		<meta name="twitter:description" content="<?php echo $descripcion_formato;?><?php if ($supertotal):?> - Superficie Total: <?php echo $supertotal;?> m²<?php endif;?>">
		<meta name="google-site-verification" content="0-QfDosakqkCqHA-DINV_fRdwGQl3jXqLUUJdCl25rY" />
		<?php endif;?>
		<!-- Si son páginas -->
		<?php if (is_page('Contacto') || is_page('Servicio-al-cliente') ||  is_page('Servicio-Post-Venta')):?>
		<meta property="og:image" content="<?php bloginfo('template_url');?>/recursos/img/fb_avellaneda.png" />
		<?php endif;?>

		<!-- Si es Corporativo -->
		<?php if (is_singular('corporativo')):
			$imagenportada = get_field('imagen');
			$imagenportada_array = wp_get_attachment_image_src($imagenportada, 'full');
			$tipocontenido = get_field('tipo_de_contenido');
			$videoid = get_field('ingresa_id_de_youtube');
		?>
		<?php if ($tipocontenido == "Texto"):?>
		<meta property="og:image" content="<?php echo $imagenportada_array[0];?>" />
		<?php else:?>
		<meta property="og:image" content="https://i.ytimg.com/vi/<?php echo $videoid;?>/maxresdefault.jpg" />
		<?php endif;?>
		<?php endif;?>

		<!-- Si es landing -->
		<?php if (is_singular('landing')):?>
			<?php
			$idsfb = get_field('selecciona_proyectos', false, false);
			$looplanding = new WP_Query(array(
				'post_type' 		=> array('proyectos'),
				'posts_per_page'	=> -1,
				'post__in'			=> $idsfb,
				'post_status'		=> 'publish',
				'orderby'        	=> 'post__in',
			));
			if ( $looplanding->have_posts() ) :?>
				<?php while ( $looplanding->have_posts() ) : $looplanding->the_post();?>
					<?php if( have_rows('slides_principal') ):?>
						<?php while ( have_rows('slides_principal') ) : the_row();?>
							<?php
								$fotografia_mobile_fb 		= get_sub_field('fotografia_mobile');
								$fotografia_mobile_array_fb = wp_get_attachment_image_src($fotografia_mobile_fb, 'full');
							?>
							<meta property="og:image" content="<?php echo $fotografia_mobile_array_fb[0];?>" />
						<?php endwhile;?>
					<?php wp_reset_postdata(); ?>
					<?php endif;?>
				<?php endwhile;?>
			<?php endif;?>
		<?php endif;?>

		<!-- Si son promociones -->
		<?php
		if (is_singular('promociones')):?>
		<?php
		$imagen = get_field('imagen_promocion_mobile');
		$imagen_promo_array = wp_get_attachment_image_src($imagen, 'full');
		?>
		<meta property="og:image" content="<?php echo $imagen_promo_array[0];?>" />
		<?php endif;?>


		<!-- Si es promocion referidos -->
		<?php
		if (is_page_template('pag-referidos.php')):?>
		<?php
		$imagen_referidos_fb 		= get_field('imagen');
		$imagen_referidos_fb_array 	= wp_get_attachment_image_src($imagen_referidos_fb, 'full');
		?>
		<meta property="og:image" content="<?php echo $imagen_referidos_fb_array[0];?>" />
		<?php endif;?>

		<!-- ----------------------------- -->
		<!-- ----------------------------- -->
		<!-- Fin Etiquetas Open Graph -->
		<!-- ----------------------------- -->
		<!-- ----------------------------- -->
	</head>
	<body>
		<?php get_template_part('secciones/politica');?>
		<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5L5DZZF"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		
		<?php endif; ?>
		<div class="page">