<?php
// Hola!
get_template_part('funciones/ctp');
get_template_part('funciones/tax');
get_template_part('funciones/librerias');
get_template_part('funciones/menu');
get_template_part('funciones/customizar');
get_template_part('funciones/formulario');
get_template_part('funciones/fotografias');
get_template_part('funciones/especiales');
get_template_part('funciones/insertardatos');
get_template_part('funciones/acf/acf');
get_template_part('funciones/rss');
require_once get_template_directory() . '/funciones/wp-bootstrap-navwalker.php';
// Aplaza la carga de jQuery usando la propiedad HTML5 defer
if (!(is_admin() )) {
    function defer_parsing_of_js ( $url ) {
        if ( FALSE === strpos( $url, '.js' ) ) return $url;
        if ( strpos( $url, 'jquery.js' ) ) return $url;
        // return "$url' defer ";
        return "$url' defer onload='";
    }
    add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
}



add_action('wp_footer', 'method');

function method(){    

    if (is_front_page() or is_page('new-home') or is_single('proyectos-condominio-plaza-buin-copia')){

        // include custom jQuery
        wp_deregister_script('jquery');
        wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.4.1.min.js', array(), '', true);

        wp_register_script('mapa', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAkVd6hg93FK8s6RAS0DjEgKdR_gaR2UXs', false);
        wp_enqueue_script('mapa');

        wp_register_script('mapa2', 'https://maps.stamen.com/js/tile.stamen.js?v1.3.0', false);
        wp_enqueue_script('mapa2');

        wp_register_script('mapa3', 'https://www.avellaneda.cl/wp-content/plugins/wp-google-map-gold/assets/js/maps.js?ver=2.3.4', false);
        wp_enqueue_script('mapa3');

		wp_register_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', false, '1.14.3', true);
		wp_enqueue_script('popper');

		wp_register_script('bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js', false, '4.1.1', true);
		wp_enqueue_script('bootstrap-js');


        wp_register_script('owl-carousel-js', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', false, null, true);
        wp_enqueue_script('owl-carousel-js');


         /*--------------------------------------------------------------------------------------
		-------------------------Consulta Proyectos por medio de JS-----------------------------
		----------------------------------------------------------------------------------------*/

        wp_register_script('main-js', get_template_directory_uri() . '/assets/js/main.js');

        //Se declaran los argumentos para el Loop del tipo de post a utilizar.
        $args = array(
            'post_type' => 'proyectos',
            'posts_per_page' => -1,
            'post_status' => 'publish',
        );
            
        //Se declara una variable como un Array vacío
        $rm_arr = array();
            
        //Segun los argumentos declarados anteriormente revisamos si el tipo de post, tiene posts en su interior
        $region_metropolitana = new WP_Query($args);
        if ($region_metropolitana->have_posts()) {
            //Comenzamos a hacer el loop de los posts
            while ($region_metropolitana->have_posts()) : $region_metropolitana->the_post();
            
            //Declaramos las variables del Post, ya sean propias de WP o bien de Custom Fields
            $postid = get_the_ID();
            $data = get_field("cn-datos", $postid);
            $post_type = get_post_type( $postid );
            $tag = get_field('selecciona_tag_del_proyecto');
            $bloque_rojo = get_field('bloque_rojo');
            $fotografia_menu = get_field('foto_portada_para_menu');
            $size = 'foto-menu-col2';
            $image_array = wp_get_attachment_image_src($fotografia_menu, $size);
            $image_url = $image_array[0];
            $logo = wp_get_attachment_image_src(get_field('logo_proyecto'), 'logo');            
            $pronto = get_field('url_landing_proyecto');
            
            //Si el post tiene tags los declaramos aqui
            if($tag){
                $tag_personalizado = get_field('personalizar_tag');
                if ($tag == "Personalizar") {
                    $estado = $tag_personalizado;
                }elseif ($tag == "Slide personalizado" || $tag == "Normal"){
                    $estado = "";
                }else {
                    $estado = $tag;
                }
            }
            
            //Si el post tiene taxonomia los declaramos aqui
            $terms = get_the_terms( $post->ID, 'ubicaciones' );
            if (!empty($terms)) {
                foreach($terms as $term) {
                    $parent = $term->parent;
                    $lugar = $term->name;
                }
            }

            if($estado == "VENTA EN VERDE"){
                $color_etiqueta = 'green';
            }else{
                $color_etiqueta = '#e5302d';
            }

            if($estado != "Agotado"){
                //Se cargan los elementos obtenidos en el loop a nuestro Array Vacío
                array_push( 
                    $rm_arr, array(
                        'title' => get_the_title(),
                        'url' => get_the_permalink(),
                        'region_parent' =>  $parent,
                        'region' =>  $lugar,
                        'tag' => $estado,
                        'image_url' => $image_url,
                        'bloque_rojo' => $bloque_rojo,
                        'logo'=> $logo[0],
                        'pronto' => $pronto,
                        'color_etiqueta' => $color_etiqueta
                    ) 
                );
            }

            endwhile; // Termina nuestro loop
            wp_reset_postdata(); //Reseteamos la consulta del post
        }
        
        //Cargamos nuestros datos obtenidos en un nuevo Array
        $script_link = array(
            'data' => $rm_arr
        );
        
        //Escribimos en nuestro archivo Main-js los datos de nuestro ultimo array guardado
        //Este se leera de manera interna y solo sera visto a travez de un console.log
        wp_localize_script(
            'main-js',//Nombre del js donde guardaremos los datos
            'project_data',//Nombre de la variable para consultar los datos más adelante
            $script_link //Nombre de la variable de donde obtenemos los datos
        );
        
        //Registro de nuestro archivo JS que solicita Enqueue
        wp_enqueue_script('main-js');
    }
    
}

function nelio_max_image_size( $file ) {

    $size = $file['size'];
    $size = $size / 1024;
    $type = $file['type'];
    $is_image = strpos( $type, 'image' ) !== false;
    $limit = 750;
    $limit_output = '750kb';
  
    if ( $is_image && $size > $limit ) {
      $file['error'] = 'La imagen debe tener un peso menor a ' . $limit_output;
    }//end if
  
    return $file;
  
  }//end nelio_max_image_size()
  add_filter( 'wp_handle_upload_prefilter', 'nelio_max_image_size' );

