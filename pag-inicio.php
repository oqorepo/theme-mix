<?php
/**
* Template Name: Inicio
*/
get_header();
get_template_part('secciones/header-normal');
?>

<!--=====================================
=            Título h1            =
======================================-->
<section class="seccion-oscura">
	<div class="container">
		<h1 class="display blanco text-center"><?php the_title();?></h1>
	</div>
</section>
<!--=====================================
=            Slides Principales Proyectos           =
======================================-->
<?php
$ids 			= get_field('slide_principal', false, false);
$query 			= new WP_Query(array(
				'post_type' 		=> array('proyectos', 'promociones', 'comunicados', 'corporativo'),
				'posts_per_page'	=> -1,
				'post__in'			=> $ids,
				'post_status'		=> 'publish',
				'orderby'        	=> 'post__in',
)); if ( $query->have_posts() ) : ?>
<section class=" inicio">

	<!-- Cargador de Slides -->
	<div class="loading_slides loading_slides_inicio">
		<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/loading.svg" alt="Cargando <?php the_title();?>" />
	</div>
	<!-- Cargador de Slides -->

	<div class="big-slide-contenedor">
		<div class="owl-carousel owl-theme owl-bigslide">
			<?php while ( $query->have_posts() ) : $query->the_post();

					$imagen 					= get_field('imagen');
					$imagen_mobile 				= get_field('imagen_mobile');
					$imagen_array 				= wp_get_attachment_image_src($imagen, $size);
					$imagen_mobile_array		= wp_get_attachment_image_src($imagen_mobile, $size);

					$url_pregunta 				= get_field('¿envia_a_una_url_en_especial');
					$url_personalizada 			= get_field('url_personalizada');
					$forma_abrir_url 			= get_field('forma_de_abrir_la_nueva_url');
					$valor_abrir_url 			= $forma_abrir_url['value'];
					$imagen_promo_desktop 		= get_field('imagen_promocion_desktop');
					$imagen_promo_mobile 		= get_field('imagen_promocion_mobile');
					$size 						= 'slides';
					$imagen_promo_desktop_array = wp_get_attachment_image_src($imagen_promo_desktop, $size);
					$imagen_promo_mobile_array 	= wp_get_attachment_image_src($imagen_promo_mobile, $size);
					$titulo_promo_imagen 		= get_the_title(get_field($imagen_promo_desktop));


			?>
					
					<!-- Comunicados -->
					<?php if ($imagen):?>
						<div class="item_slide av-comunicado">
							<?php if ($url_pregunta === "Si"):?>
								<a href="<?php echo $url_personalizada;?>" target="_<?php echo $forma_abrir_url;?>">
									<picture>
										<source
											media="(max-width: 920px)"
											srcset="<?php echo $imagen_mobile_array[0];?>">
										<img src="<?php echo $imagen_array[0];?>" class="img-responsive" alt="Comunicado <?php the_title();?>">
									</picture>
								</a>
							<?php else:?>
								<picture>
									<source
										media="(max-width: 920px)"
										srcset="<?php echo $imagen_mobile_array[0];?>">
									<img src="<?php echo $imagen_array[0];?>" class="img-responsive" alt="Comunicado <?php the_title();?>">
								</picture>
							<?php endif;?>
						</div>

					<?php endif;?>

					<!-- Slide Promociones -->
					<?php if ($imagen_promo_desktop):?>
							<div class="item_slide av-promocion">
								<?php if ($url_pregunta === "Si"):?>
								<a href="<?php echo $url_personalizada;?>" target="_<?php echo $forma_abrir_url;?>">
								<?php else:?>
								<a href="<?php the_permalink();?>">
								<?php endif;?>
									<picture>
										<source
											media="(max-width: 920px)"
											srcset="<?php echo $imagen_promo_mobile_array[0];?>">
										<img src="<?php echo $imagen_promo_desktop_array[0];?>" class="img-responsive" alt="Promoción <?php the_title();?>">
									</picture>
								</a>
							</div>
					<?php endif;?>
				<!-- Fin Slide Promociones -->

				<!-- Inicio Slides Proyectos -->
				<?php if( have_rows('slides_principal') ):
					while ( have_rows('slides_principal') ) : the_row();
						$fotografia 		= get_sub_field('fotografia_slide');
						$fotografia_mobile 	= get_sub_field('fotografia_mobile');
						$size 				= 'slides';
						$image_array 		= wp_get_attachment_image_src($fotografia, $size);
						$image_array_mobile = wp_get_attachment_image_src($fotografia_mobile, $size);
						$titulo_imagen 		= get_the_title(get_sub_field($fotografia));
						$video 				= get_sub_field('video_slide');
						$logo 				= get_field('logo_proyecto');
						$logo_array 		= wp_get_attachment_image_src($logo, 'logo');
						$logo_url 			= $logo_array[0];
						$entrega 			= get_field('entrega');
						$bloque_rojo 		= get_field('bloque_rojo');?>
				<div class="item_slide">
					<!-- Etiqueta del Proyecto -->
					<?php
					$tag = get_field('selecciona_tag_del_proyecto');
					if ($tag):?>
					<div class="estado-de-proyecto-wrap">
						<?php
						$tag_personalizado = get_field('personalizar_tag');
						if ($tag == "Personalizar"):?>
						<div class="container">
							<div class="estado-wrap">
								<div class="estado-de-proyecto"><?php echo $tag_personalizado;?></div>
							</div>
						</div>
						<?php elseif ($tag == "Slide personalizado"):?>
							<?php //if (get_sub_field('sv_bs_texto_del_tag')): ?>
							<!-- <div class="container">
								<div class="estado-wrap">
									<div class="estado-de-proyecto" style="background:<?php the_sub_field('sv_bs_color_del_tag'); ?>;"><?php the_sub_field('sv_bs_texto_del_tag');?></div>
								</div>
							</div> -->
							<?php //endif;?>
						<?php elseif ($tag == "Normal"):?>
						<?php else:?>
						<div class="container">
							<div class="estado-wrap">
								<div class="estado-de-proyecto"><?php echo $tag;?></div>
							</div>
						</div>
					<?php endif;?>
					</div>
					<?php endif;?>
					<!-- Fin Etiqueta del Proyecto -->
					<!-- Fotografía del Proyecto -->
					<?php
					if ($fotografia):?>
					<a href="<?php the_permalink();?>">
						<picture>
							<source
							media="(max-width: 920px)"
							srcset="<?php echo $image_array_mobile[0];?>">
							<img src="<?php echo $image_array[0];?>" class="img-responsive" alt="<?php echo $titulo_imagen;?>">
						</picture>
					</a>
					<?php endif;?>
					<!-- Fin Fotografía -->
					<?php // Si existe el bloque rojo con descripcion, muestra esta sección
					if ($bloque_rojo):?>
					<div class="seccion-oscura descripcion-proyecto">
						<div class="container">
							<div class="row">
								<?php // Si el proyecto cuenta con logo
								if ($logo):?>
								<!-- Logo -->
								<div class="col-md-2">
									<div class="avatar-proyecto borde-redondo ">
										<a href="<?php the_permalink(); ?>">
											<img src="<?php echo $logo_url;?>" class="img-responsive borde-redondo" alt="<?php the_title();?>" title="<?php echo $titulo_imagen;?>"/>
										</a>
									</div>
								</div>
								<?php endif;?>
								<?php // Si existe ubicación, la muestra
								$terms = get_the_terms( $post->ID , 'ubicaciones' );
								if ( $terms != null ):?>
								<div class="col-md-6">
									<div class="solo-celular text-center">
										<h2><?php the_title();?></h2>
									</div>
									<div class="bloque-gris">
										<div class="solo-desktop">
											<h2><div class="ubicacion-actual">Ubicación</div> <?php get_template_part('modulos/ubicacion-actual');?>
											</h2>
										</div>
										<div class="solo-celular text-center">
											<span class="ubicacion-actual">Ubicación <?php get_template_part('modulos/ubicacion-actual');?>
											</span>
										</div>
									</div>
								</div>
								<?php endif;?>
								<?php
								// Si existe el bloque rojo con información
								if ($bloque_rojo):?>
								<div class="col-md-4">
									<div class="bloque-rojo ">
										<?php echo $bloque_rojo;?>
									</div>
								</div>
								<?php endif;?>
							</div>
						</div>
					</div>
					<?php endif;?>
				</div>
				<!-- Fin Slide -->
				<?php break; endwhile;?>
				<?php endif;?>
				<?php endwhile;?>
				<?php wp_reset_postdata(); ?>
			</div>
			<!-- Controles Slide -->
			<div id="controles-slides">
				<div class="siguiente">
					<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Siguiente" class="tooltipster2 big-slide-siguiente" title="Ver Siguiente" />
				</div>
				<div class="anterior">
					<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Anterior" class="tooltipster2 big-slide-anterior" title="Ver Anterior" />
				</div>
			</div>
			<!-- Fin Controles Slide -->
		</div>
	</section>
	<?php endif;?>

<!--=====================================
=            Mensaje Personalizado           =
======================================-->
<?php $mensajeinicio = get_field('mensaje');?>
<?php if ($mensaje):?>
	<section class="gris-claro frase-inicio animated fadeInUp">
		<div class="container">
			<div class="frase">
				<?php echo $mensaje;?>
			</div>
		</div>
	</section>
<?php endif;?>



<!--=====================================
=            Proyectos Destacados           =
======================================-->
<?php
$ids_2 					= get_field('slide_detalles', false, false);
$query 					= new WP_Query(array(
'post_type'      		=> 'proyectos',
'posts_per_page'		=> -1,
'post__in'				=> $ids_2,
'post_status'			=> 'publish',
'orderby'        		=> 'post__in',
));
	if ( $query->have_posts() ) : ?>
	<section class="proyectos-slides-destacados">
		<!-- Título y Sub-título -->
		<?php
		$titulo = get_field('titulo_slide_detalles_inicio');
		$subtitulo = get_field('titulo_sub_slide_detalles_inicio');
		if($titulo):?>
		<div class="container">
			<h2 class="titulos"><?php echo $titulo;?></h2>
			<?php endif;?>
			<?php if($subtitulo):?>
			<h4 class="bajadas"><?php echo $subtitulo;?></h4>
		</div>
		<?php endif;?>
		<!-- Fin Título y Sub-título -->
		<!-- Cargador de Slides -->
		<div class="loading_slides loading_slides_proyectos">
			<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/loading.svg" alt="Cargando <?php the_title();?>" />
		</div>
		<!-- Fin Cargador de Slides -->
		<!-- Slide -->
		<div class="proyectos-slide-contenedor">
			<div class="owl-carousel owl-theme owl-proyectos">
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<!-- Loop de Proyectos -->
				<?php if( have_rows('slide_fotografias_galeria_finales') ):?>
				<?php
				while ( have_rows('slide_fotografias_galeria_finales') ) : the_row();
				// Variables
										$descripcion 		= get_sub_field('descripcion_galeria');
										$fotografia 		= get_sub_field('fotografia_galeria');
																$size 				= 'fotografias';
										$size_mobile 		= 'fotografias-tab-cuadrado';
										$image_array 		= wp_get_attachment_image_src($fotografia, $size);
				$image_array_mobile = wp_get_attachment_image_src($fotografia, $size_mobile);
										$titulo_imagen 		= get_the_title(get_sub_field($fotografia));
				?>
				<div class="slide-imagen">
					<div class="fotografia-fondo" style="background:url('<?php echo $image_array_mobile[0];?>'); background-size: cover; background-repeat: no-repeat;">
					</div>
					<div class="foto_galeria">
						<div class="container">
							<div class="solo-celular">
								<a href="<?php the_permalink();?>">
									<picture>
										<source
										media="(max-width: 920px)"
										srcset="<?php echo $image_array_mobile[0];?>">
										<img src="<?php echo $image_array[0];?>" class="img-responsive imagen-slider-slider img-full" alt="<?php echo $titulo_imagen;?>">
									</picture>
								</a>
							</div>
							<div class="solo-desktop">
								<picture>
									<source
									media="(max-width: 920px)"
									srcset="<?php echo $image_array_mobile[0];?>">
									<img src="<?php echo $image_array[0];?>" class="img-responsive imagen-slider-slider img-full" alt="<?php echo $titulo_imagen;?>">
								</picture>
							</div>
						</div>
					</div>
					<div class="galerias_wrap">
						<div class="container">
							<div class="descripcion_galeria_proyectos">
								<?php get_template_part('modulos/tag-small');?>
								<h3><?php the_title();?></h3>
								<?php echo $descripcion;?>
								<div class="solo-desktop"><a href="<?php the_permalink();?>"><button class="btn btn-rojo" id="menuProyectosInicio" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});">Ver Proyecto</button></a></div>
							</div>
						</div>
					</div>
				</div>
				<?php break;?>
				<?php endwhile;?>
				<?php endif;?>
				<?php endwhile;?>
			</div>
			<!-- Controles Slide -->
			<div id="controles-slides">
				<div class="siguiente">
					<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Siguiente" class="tooltipster2 proyectos-siguiente" title="Ver Siguiente" />
				</div>
				<div class="anterior">
					<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Anterior" class="tooltipster2 proyectos-anterior" title="Ver Anterior" />
				</div>
			</div>
			<!-- Fin Controles Slide -->
		</div>
	</section>
	<?php endif;?>
	<?php wp_reset_postdata(); ?>



<!--=====================================
=            Destacados Avellaneda           =
======================================-->


	<?php if( have_rows('hito', '315') ):?>
	<section class="destacados">

		<div class="container">
	<div class="row">
		<?php
		$titulo_avellaneda = get_field('titulo_avellaneda');
		$sub_titulo_avellaneda = get_field('sub-titulo_avellaneda');
		?>
		<div class="container">
			<?php if($titulo_avellaneda):?>
			<h2 class="titulos"><?php echo $titulo_avellaneda;?>	</h2>
			<?php endif;?>
			<?php if($sub_titulo_avellaneda):?>
			<h4 class="bajadas"><?php echo $sub_titulo_avellaneda;?></h4>
			<?php endif;?>
		</div>
	</div>
</div>


		<div class="container">
			<div class="row">
				<?php while ( have_rows('hito', '315') ) : the_row();
				$icono = get_sub_field('icono');
				$titulo = get_sub_field('titulo');
				$texto = get_sub_field('texto');
				?>
				<div class="col-md-4 destacados">
					<img src="<?php echo $icono;?>" class="img-responsive icono-destacado" alt="Hitos Destacados de Avellaneda" />
					<h2><?php echo $titulo;?></h2>
					<div class="bajo-destacado"><?php echo $texto;?></div>
				</div>
				<?php endwhile;?>
			</div>
		</div>
	</section>
	<?php endif;?>
	<?php wp_reset_postdata(); ?>


<!--=====================================
=            Corporativo            =
======================================-->
	<?php
$ids_3 = get_field('selecciona_post', false, false);
$query = new WP_Query(array(
'post_type' => array('corporativo'),
'posts_per_page'	=> -1,
'post__in'			=> $ids_3,
'post_status'		=> 'publish',
'orderby'        	=> 'post__in',
));
?>
	<?php if ( $query->have_posts() ) :
	$count = 0;
	while ( $query->have_posts() ) : $query->the_post();
	$imagenportada = get_field('imagen');
	$size = 'corporativo';
	$imagenportada_array = wp_get_attachment_image_src($imagenportada, $size);
	$contenido = get_field('contenido');
	$tipocontenido = get_field('tipo_de_contenido');
	$videoid = get_field('ingresa_id_de_youtube');
	?>
	<?php if ($tipocontenido == "Texto"):?>
	<?php if ($imagenportada):?>
	<section class="fotomastexto">
		<div class="bloque-fondo-completo">
			<div class="bloque-con-fondo" style="background: url('<?php echo $imagenportada_array[0];?>') top;"></div>
			<div class="bloque-con-fondo-wrap">
				<div class="container">
					<div class="row row-eq-height fondo-rojo centrarmedio">
						<div class="col-md-8 sin-padding">
							<img src="<?php echo $imagenportada_array[0];?>" class="img-responsive" alt="<?php the_title();?>" />
						</div>
						<div class="col-md-4 bloque-color  font-media">
							<h2 style="margin-bottom:20px;"><?php the_title();?></h2>
							<?php echo $contenido;?>
							<a href="<?php bloginfo('url');?>/empresa">
								<button class="btn btn-rojo btn-gris btn-lg "><i class="fas fa-home"></i> Saber más de Avellaneda</button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif;?>
	<?php endif;?>
	<?php if ($tipocontenido == "Video"):?>
	<section class="fotomastexto">
		<div class="bloque-fondo-completo">
			<div class="bloque-con-fondo"></div>
			<div class="bloque-con-fondo-wrap">
				<div class="container">
					<div class="row row-eq-height fondo-rojo centrarmedio">
						<?php if ($count % 2 == 0) :?>
						<div class="col-md-8 sin-padding col-lg-push-4">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="//www.youtube.com/embed/<?php echo $videoid;?>"></iframe>
							</div>
						</div>
						<div class="col-md-4 bloque-color  font-media col-lg-pull-8">
							<h2 class="titulos"><?php the_title();?></h2>
							<div style="font-size:12px !important;"><?php echo $contenido;?></div>
						</div>
						<?php else:?>
						<div class="col-md-8 sin-padding">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe allowfullscreen="allowfullscreen" allowfullscreen class="embed-responsive-item" src="//www.youtube.com/embed/<?php echo $videoid;?>"></iframe>
							</div>
						</div>
						<div class="col-md-4 bloque-color  font-media">
							<h2 class="titulos"><?php the_title();?></h2>
							<?php echo $contenido;?>
						</div>
						<?php endif;?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif;?>
	<?php endwhile;?>
	<?php endif;?>






<!--=====================================
=            Instagram            =
======================================-->
<section class="bloque-instagram">
	<div class="container">
			<div class="row">
				<?php echo do_shortcode('[elfsight_instagram_feed id="1"]');?>
			</div>
	</div>
</section>




<?php get_footer();?>