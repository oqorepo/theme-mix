<?php
/* Template Name: New Home */ 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name'); ?></title>
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/7ae8cf8f26.js" crossorigin="anonymous"></script>
    <!-- css -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main-new.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/megamenu.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/elastic_menu.css">
    <?php //wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    
    <div class="cd-loader">
        <div class="cd-loader__grid">
            <div class="cd-loader__item"></div>
        </div>
    </div>

    <!--
    ---------------------------------------------------------------------------------------------------
    ----------------------------------------HEADER-----------------------------------------------------
    ---------------------------------------------------------------------------------------------------
    -->
    <header id="header-desktop" class="d-none d-md-block">
        <!--------------------------------------Menu Buscar----------------------------------------------->
        <div class="accordion" id="collapseBuscar">
            <div class="container-fluid pt-2 pb-2 border-bottom">
            <div class="container">
                <div class="d-flex justify-content-between">
                <div class="contacto-top">
                    <a href="#"><i class="far fa-envelope"></i> CONTACTO</a>
                    <a class="pl-2" href="#">INMOBILIARIA AVELLANEDA</a>
                </div>
                <div class="buscar-top">
                    <a href="#" data-toggle="collapse" data-target="#contenido_collapseBuscar" aria-expanded="false" aria-controls="collapseOne">
                        <i class="fas fa-angle-down"></i> BUSCAR
                    </a>
                </div>
                </div>      
            </div>
            </div>

            <div id="contenido_collapseBuscar" class="collapse" aria-labelledby="headingOne" data-parent="#collapseBuscar">
            <div class="bg-dark text-white text-center pt-3 pb-3">
                <div id="preguntas-buscar1" class="show-preguntas">
                    <h4>¿DÓNDE QUIERES VIVIR?</h4>
                    <button id="btn-buscar-stgo" class="btn btn-lg btn-opciones btn-rojo">Santiago</button>
                    <button id="btn-buscar-reg" class="btn btn-lg btn-opciones btn-rojo">Regiones</button>
                </div>
                <div id="preguntas-buscar2-stgo" class="hide-preguntas">
                    <h4>SELECCIONA SECTOR</h4>
                    <div id="sector-stgo" class="container"></div>
                    <button id="btn-buscar-volver-stgo" class="btn btn-lg btn-opciones btn-rojo">Volver</button>
                </div>
                <div id="preguntas-buscar2-reg" class="hide-preguntas">
                    <h4>SELECCIONA SECTOR</h4>
                    <div id="sector-reg"></div>
                    <button id="btn-buscar-volver-reg" class="btn btn-lg btn-opciones btn-rojo">Volver</button>
                </div>
            </div>
            </div>
        </div>
        <!------------------------------------Fin Menu Buscar----------------------------------------------->

        <!----------------------------------------Mega Menu------------------------------------------------->
        <!--Desktop Menu-->
        <nav class="navbar-expand-lg main-header">
            <div class="container d-flex justify-content-between d-flex align-items-end">
                <div class="logo">
                    <a class="navbar-brand" href="#">
                        <img src="https://www.avellaneda.cl/wp-content/themes/avellaneda/recursos/img/iconos/logo.svg" alt="PROYECTOS INMOBILIARIOS EN SANTIAGO Y REGIONES" />
                    </a>
                </div>
            
                <ul class="navbar-nav main-menu-new">
                    <li class="nav-item nav-item-parent droppable">
                        <a class="nav-link nav-link-parent dropdown-toggle" href="#">PROYECTOS REGIÓN METROPOLITANA</a>
                        <div class="mega-menu">
                            <div class="container proyectos_rm"></div>
                        </div><!-- .mega-menu 1 -->
                    </li>

                    <li class="nav-item nav-item-parent droppable">
                        <a class="nav-link nav-link-parent dropdown-toggle" href="#">PROYECTOS REGIONES</a>
                        <div class="mega-menu">
                            <div class="container proyectos_re"></div>
                        </div><!-- .mega-menu 1 -->
                    </li>

                    <li class="nav-item nav-item-parent">
                        <a class="nav-link nav-link-parent" href="#">OPORTUNIDADES</a>
                    </li>

                    <li class="nav-item nav-item-parent dropdown">
                        <a class="nav-link nav-link-parent dropdown-toggle" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">SERVICIO AL CLIENTE</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#"><i class="fas fa-list-ol"></i> PROCESO DE COMPRA</a>
                            <a class="dropdown-item" href="#"><i class="fab fa-wpforms"></i> SERVICIO POST VENTA</a>
                            <a class="dropdown-item" href="#"><i class="far fa-calendar-check"></i> RESERVA DE HORA PRE-ENTREGA Y ENTREGA</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <!-------------------------------------Fin Mega Menu------------------------------------------------>

        <!-----------------------------------Scroll Desktop Menu-------------------------------------------->
        <nav id="scroll-desktop-menu" class="navbar-expand-lg main-header d-none esconder-menu w-100">
            <div class="container d-flex justify-content-between d-flex align-items-end">
                <div class="logo">
                    <a class="navbar-brand" href="#">
                    <img src="https://www.avellaneda.cl/wp-content/themes/avellaneda/recursos/img/iconos/logo.svg" alt="PROYECTOS INMOBILIARIOS EN SANTIAGO Y REGIONES" />
                    </a>
                </div>
                
                <ul class="navbar-nav main-menu-new">
                    <li class="nav-item nav-item-parent droppable">
                        <a class="nav-link nav-link-parent dropdown-toggle" href="#">PROYECTOS REGIÓN METROPOLITANA</a>
                        <div class="mega-menu">
                            <div class="container proyectos_rm"></div>
                        </div><!-- .mega-menu 1 -->
                    </li>

                    <li class="nav-item nav-item-parent droppable">
                        <a class="nav-link nav-link-parent dropdown-toggle" href="#">PROYECTOS REGIONES</a>
                        <div class="mega-menu">
                            <div class="container proyectos_re"></div>
                        </div><!-- .mega-menu 1 -->
                    </li>

                    <li class="nav-item nav-item-parent">
                        <a class="nav-link nav-link-parent" href="#">OPORTUNIDADES</a>
                    </li>

                    <li class="nav-item nav-item-parent dropdown">
                        <a class="nav-link nav-link-parent dropdown-toggle" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">SERVICIO AL CLIENTE</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#"><i class="fas fa-list-ol"></i> PROCESO DE COMPRA</a>
                            <a class="dropdown-item" href="#"><i class="fab fa-wpforms"></i> SERVICIO POST VENTA</a>
                            <a class="dropdown-item" href="#"><i class="far fa-calendar-check"></i> RESERVA DE HORA PRE-ENTREGA Y ENTREGA</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <header id="header-mobile" class="d-md-none">
        <!--------------------------------------Menu Buscar----------------------------------------------->
        <div class="accordion" id="collapseBuscar">
            <div class="container-fluid pt-1 pb-1 border-bottom">
                <div class="container">
                    <div class="d-flex justify-content-between">
                        <a href="#">
                            <img src="https://www.avellaneda.cl/wp-content/themes/avellaneda/recursos/img/iconos/logo.svg" width="140" alt="PROYECTOS INMOBILIARIOS EN SANTIAGO Y REGIONES" />
                        </a>
                        <div class="pt-2">
                            <a href="#" data-toggle="collapse" data-target="#contenido_collapseBuscar2" aria-expanded="false" aria-controls="collapseOne">
                                <i class="fas fa-search" style="font-size:30px; margin-right:10px;"></i>
                            </a>
                            <a id="btn_show" href="#"><i class="fas fa-bars" style="font-size:30px;"></i></a>
                        </div>
                    </div>      
                </div>
            </div>

            <div id="contenido_collapseBuscar2" class="collapse" aria-labelledby="headingOne" data-parent="#collapseBuscar2">
                <div class="bg-dark text-white text-center pt-3 pb-3">
                    <div id="preguntas-buscar1" class="show-preguntas">
                        <h4>¿DÓNDE QUIERES VIVIR?</h4>
                        <button id="btn-buscar-stgo" class="btn btn-lg btn-opciones btn-rojo">Santiago</button>
                        <button id="btn-buscar-reg" class="btn btn-lg btn-opciones btn-rojo">Regiones</button>
                    </div>
                    <div id="preguntas-buscar2-stgo" class="hide-preguntas">
                        <h4>SELECCIONA SECTOR</h4>
                        <div id="sector-stgo" class="container"></div>
                        <button id="btn-buscar-volver-stgo" class="btn btn-lg btn-opciones btn-rojo">Volver</button>
                    </div>
                    <div id="preguntas-buscar2-reg" class="hide-preguntas">
                        <h4>SELECCIONA SECTOR</h4>
                        <div id="sector-reg"></div>
                        <button id="btn-buscar-volver-reg" class="btn btn-lg btn-opciones btn-rojo">Volver</button>
                    </div>
                </div>
            </div>
        </div>
        <!------------------------------------Fin Menu Buscar----------------------------------------------->

        <!-----------------------------------Scroll Mobile Menu-------------------------------------------->
        <nav id="scroll-mobile-menu" class="navbar-expand-lg main-header d-none esconder-menu w-100">
            <div class="container-fluid pt-1 pb-1 border-bottom">
                <div class="container">
                    <div class="d-flex justify-content-between">
                        <a href="#">
                            <img src="https://www.avellaneda.cl/wp-content/themes/avellaneda/recursos/img/iconos/logo.svg" width="140" alt="PROYECTOS INMOBILIARIOS EN SANTIAGO Y REGIONES" />
                        </a>
                        <div class="pt-2">
                            <a class="text-secondary" href="#" data-toggle="collapse" data-target="#contenido_collapseBuscar2" aria-expanded="false" aria-controls="collapseOne">
                                <i class="fab fa-facebook" style="font-size:25px; margin-right:10px;"></i>
                            </a>
                            <a class="text-secondary" href="#" data-toggle="collapse" data-target="#contenido_collapseBuscar2" aria-expanded="false" aria-controls="collapseOne">
                                <i class="fab fa-instagram" style="font-size:25px; margin-right:10px;"></i>
                            </a>
                            <a class="text-secondary" href="#" data-toggle="collapse" data-target="#contenido_collapseBuscar2" aria-expanded="false" aria-controls="collapseOne">
                                <i class="fab fa-youtube" style="font-size:25px; margin-right:10px;"></i>
                            </a>
                            <a id="btn_show2"><i class="fas fa-bars" style="font-size:30px;"></i></a>
                        </div>
                    </div>      
                </div>
            </div>
        </nav>

        <!-----------------------------------Elastic Menu-------------------------------------------->
        <div id="elastic-menu" class="navigation">
            <div class="navigation__inner">
                <div class="cerrar-elastic">
                    <a class="text-white" id="btn_close"><i class="far fa-times-circle"></i></a>
                </div>

                <div class="content-elastic">
                    <div class="accordion" id="accordionExample">
                        <li><a href="#"><i class="fas fa-home"></i> Inicio</a></li>
                        <li><a data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><i class="fas fa-building"></i> Proyectos</a></li>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <ul>
                                <div class="accordion" id="accordionExample2">
                                    <li><a data-toggle="collapse" data-target="#collapseOneStgo" aria-expanded="false" aria-controls="collapseOneStgo">En Santiago</a></li>
                                    <div id="collapseOneStgo" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample2">
                                        <ul id="elastic-menu-stgo"></ul>
                                    </div>

                                    <li><a data-toggle="collapse" data-target="#collapseOneReg" aria-expanded="false" aria-controls="collapseOneReg">En Regiones</a></li>
                                    <div id="collapseOneReg" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample2">
                                        <ul id="elastic-menu-reg"></ul>
                                    </div>
                                </div>
                            </ul>
                        </div>

                        <li><a data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="fas fa-handshake"></i> Servicio al Cliente</a> </li> 
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <ul>
                                <li><a href="#"><i class="fas fa-comments"></i> Servicio Post-Venta</a></li>
                                <li><a href="#"><i class="fas fa-calendar-check"></i> Reserva de Hora Pre-entrega y entrega</a></li>
                            </ul>
                        </div>
                        <li><a href="#">Inmobiliaria Avellaneda</a></li>
                        <li><a href="#"><i class="fas fa-thumbs-up"></i> Oportunidades</a></li>
                        <li><a href="#"><i class="fas fa-envelope"></i> Contacto</a></li>
                    </div>
                </div>

            </div>
        </div>
    </header>

    <!--
    ---------------------------------------------------------------------------------------------------
    -------------------------------------CONTENIDO PRINCIPAL-------------------------------------------
    ---------------------------------------------------------------------------------------------------
    -->
    

    <div class="main">
        <section class="container-fluid av--bg__grey">
            <div class="container">
                <h1 class="text-white text-center m-0 p-3">PROYECTOS INMOBILIARIOS EN SANTIAGO Y REGIONES</h1>
            </div>
        </section>

        <?php 
            $ids = get_field('slide_principal', false, false);
            $slider = new WP_Query(array(
                'post_type' => array('proyectos', 'promociones', 'comunicados', 'corporativo'),
                'posts_per_page' => -1,
                'post__in' => $ids,
                'post_status' => 'publish',
                'orderby' => 'post__in',
            ));
            if($slider->have_posts()):
        ?>
        <section class="container-fluid p-0">
            <div id="banner-top" class="owl-carousel owl-theme">
            <?php 
                while($slider->have_posts()): $slider->the_post(); 
                if ( 'proyectos' == get_post_type() ) {
                    if( have_rows('slides_principal') ):
                        while ( have_rows('slides_principal') ) : the_row();
                        $fotografia = wp_get_attachment_image_src(get_sub_field('fotografia_slide'), 'slides');
                        $fotografia_mobile 	= wp_get_attachment_image_src(get_sub_field('fotografia_mobile'),'slides');
                        $tag = get_field('selecciona_tag_del_proyecto');
                        $bloque_rojo 		= get_field('bloque_rojo');
                        $logo 				= wp_get_attachment_image_src(get_field('logo_proyecto'), 'logo');
                        
                        ?>
                    <div class="item">
                        <a href="<?php echo the_permalink();?>" alt="">
                        <?php if ($tag):
                            $tag_personalizado = get_field('personalizar_tag');
                            $tag_color = get_field('av_color_de_tag');                      

                            if ($tag == "Personalizar") {
                                echo '<div class="av-slider-tag p-3" style="color:white;background:'.$tag_color.'">'.$tag_personalizado.'</div>';
                            }elseif ($tag == "Slide personalizado" || $tag == "Normal"){
                            }else {
                                echo '<div class="av-slider-tag av--bg__red p-3" style="color:white">'.$tag.'</div>';
                            }
                        endif;?>
                            <picture>
                                <source
                                    media="(max-width: 920px)"
                                    srcset="<?php echo $fotografia_mobile[0];?>">
                                <img src="<?php echo $fotografia[0];?>" class="w-100" alt="Comunicado <?php the_title();?>">
                            </picture>
                        </a>
                        <div class="av-sl-info av--bg__grey container-fluid p-3 text-white">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="<?php echo $logo[0];?>" class="d-none d-md-block av-sl-info--logo">
                                        <h2 class="d-md-none"><?php the_title(); ?></h2>
                                    </div>
                                
                                    <div class="col-md-4 col-lg-5 col-xl-4">                            
                                        <?php
                                        $terms = get_the_terms( $post->ID, 'ubicaciones' );
                                        if (!empty($terms)) {
                                            echo '<h2 class="av-sl-info--ubi"><span class="av-sl-info--ubi__title">Ubicación <span class="d-md-none"> - </span></span> 
                                            ';
                                            foreach($terms as $term) {
                                                echo $term->name;
                                            }
                                            echo '</h2>';                                
                                        }
                                        ?>                     
                                    </div>
                                    <div class="col-md-6 col-lg-5 col-xl-6">
                                        <div class="av-sl-info--block av--bg__red p-4">
                                            <div class="av-sl-info--text">
                                                <?php echo $bloque_rojo;?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php break; endwhile; endif;
                }else{
                    $imagen = wp_get_attachment_image_src(get_field('imagen_promocion_desktop'), 'slides');
                    $imagen_mobile = wp_get_attachment_image_src(get_field('imagen_promocion_mobile'), 'slides');
                    ?>
                    <div class="item">
                        <a href="<?php the_permalink();?>" alt="">
                            <picture>
                                <source
                                    media="(max-width: 920px)"
                                    srcset="<?php echo $imagen_mobile[0];?>">
                                <img src="<?php echo $imagen[0];?>" class="w-100" alt="Comunicado <?php the_title();?>">
                            </picture>
                        </a>
                    </div>
                <?php
                }
                endwhile; wp_reset_postdata();?>
            </div>
        </section>
        <?php endif;  wp_reset_query(); ?>

        <section class="py-4">
            <?php
                $titulo = get_field('titulo_slide_detalles_inicio');
                $subtitulo = get_field('titulo_sub_slide_detalles_inicio');
                ?>
                <div class="container text-center">
                <?php if($titulo):?>
                    <h2 class="titulos"><?php echo $titulo;?></h2>
                <?php endif;?>
                <?php if($subtitulo):?>
                    <h4 class="bajadas"><?php echo $subtitulo;?></h4>
                </div>
            <?php endif;?>
        </section>


        <?php
        $ids_2 					= get_field('slide_detalles', false, false);
        $query 					= new WP_Query(array(
        'post_type'      		=> 'proyectos',
        'posts_per_page'		=> -1,
        'post__in'				=> $ids_2,
        'post_status'			=> 'publish',
        'orderby'        		=> 'post__in',
        ));
        if ( $query->have_posts() ) : ?>
        <section class="container proyectos-slides-destacados">
            <div class="row equal">
            <?php while ( $query->have_posts() ) : $query->the_post(); 
            $descripcion 		= get_field('bloque_rojo');
            $imagen = wp_get_attachment_image_src(get_field('foto_portada_para_menu'), 'full');
            ?>
                <div class="col-sm-6 col-md-4 mb-4">
                    <a href="<?php the_permalink();?>" class="card av-pr--card ">
                    <picture>
                        <img src="<?php echo $imagen[0];?>" class="w-100 card-img-top" alt="<?php echo $titulo_imagen;?>">
                    </picture>
                        <?php if ($tag):
                            echo '<span class="card--tag">';
                            $tag_personalizado = get_field('personalizar_tag');
                            if ($tag == "Personalizar") {
                                echo '<div class="av-general-tag av--bg__red text-center p-1" style="color:white">'.$tag_personalizado.'</div>';
                            }elseif ($tag == "Slide personalizado" || $tag == "Normal"){
                            }else {
                                echo '<div class="av-general-tag av--bg__red" style="color:white">'.$tag.'</div>';
                            }
                            echo '</span>';
                        endif;?>
                        <div class="card-body">
                            <p lass="av-pr--card__title"><b><?php the_title();?></b></p>
                            <div class="av-pr--card__text"><?php echo $descripcion;?></div>
                            <button class="btn btn-primary av-pr--card__button">Cotizar</button>
                        </div>
                    </a>
                </div>
            <?php endwhile;?>
            </div>
        </section>
        <?php endif;?>

        <section class="container py-5">
            <div class="row text-center">
                <div class="col-12 pb-4">
                    <h3>30 AÑOS CONSTRUYENDO BARRIOS EN SANTIAGO Y REGIONES</h3>
                    <p>Nuestro compromiso: Construir Calidad de Vida</p>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo esc_url( home_url('/') );?>wp-content/uploads/2018/03/constructor.svg" alt="" style="width:80px;height:80px;">
                    <h3>+30</h3>
                    <p>Años en la Industria Inmobiliaria</p>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo esc_url( home_url('/') );?>wp-content/uploads/2018/03/hogar.svg" alt="" style="width:80px;height:80px;">
                    <h3>+ 19.000</h3>
                    <p>Viviendas entregadas</p>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo esc_url( home_url('/') );?>wp-content/uploads/2018/03/lugares.svg" alt="" style="width:80px;height:80px;">
                    <h3>+ 16</h3>
                    <p>Comunas de la Región Metropolitana</p>
                </div>
            </div>
        </section>

        <section class="container-fluid p-0 mb-5">
            <div class="w-100 av-exp__img" style="background: url('<?php echo esc_url( home_url('/') );?>wp-content/uploads/2018/03/Avellaneda-Oficinas-780x439.jpg')"></div>
            <div class="container pt-4" >
            <div class="row">
                <div class=" col-xl-8 px-0">
                    <img src="<?php echo esc_url( home_url('/') );?>wp-content/uploads/2018/03/Avellaneda-Oficinas-780x439.jpg" alt="" class="w-100">
                </div>
                <div class=" col-xl-4 pt-4 av--bg__red text-white p-4">
                    <h3>EXPERIENCIA AVELLANEDA</h3>
                    <p>La experiencia de haber� entregado más de 19.000 viviendas en estos ya 30 años desarrollando proyectos inmobiliarios, nos exige adaptarnos de manera permanente a los cambios propios del desarrollo, incorporando tecnología y avances, sin perder la capacidad de comprometernos y desarrollar la actividad constructiva de manera responsable y sustentable.</p>
                    <a href="<?php bloginfo('url');?>/empresa">
                        <button class="btn btn-primary "><i class="fas fa-home"></i> Saber más de Avellaneda</button>
                    </a>
                </div>
            </div>
            </div>
        </section>

        <?php
        $taxonomy = 'category';
        $terms_args = array(
            'orderby' => 'id',
            'hide_empty' => true,
            'exclude' => '1'
        );
        $cats = get_terms($taxonomy, $terms_args);
        ?>
        <section class="landing">
            <div class="container">
                <?php foreach ($cats as $cat):
                    $cat_id = $cat->term_id;
                    $term_slug = $cat->slug;
                ?>
                <li class="row bloques-paginas-landing">
                    <div class="col-md-2">
                        <div class="nombre-landing"><?php echo $cat->name; ?></div>
                    </div>
                    <div class="col-md-10">
                        <?php
                            $query = new WP_Query(array(
                                'post_type' 		=> array('landing'),
                                'posts_per_page'	=> -1,
                                'post_status'		=> 'publish',
                                'tax_query' => array(
                                    array(
                                    'taxonomy' => $taxonomy,
                                    'field'    => 'slug',
                                    'terms'    => $term_slug,
                                    ),
                                )
                            ));

                        if ( $query->have_posts() ) :?>
                    
                            <?php while ( $query->have_posts() ) : $query->the_post();
                                $apareceEnElMenu = get_field('incluir_landing_en_el_footer_del_sitio');
                                
                                if ($apareceEnElMenu == "Si"):?>
                                    <a href="<?php the_permalink();?>"><?php the_title();?>, </a>
                                <?php endif;
                            endwhile;
                            wp_reset_postdata();
                        endif;
                        ?>
                    </div>
                </li>
                <?php endforeach;?>
            </div>
        </section>
    </div>

    <!--
        ---------------------------------------------------------------------------------------------------
        -------------------------------------------FOOTER--------------------------------------------------
        ---------------------------------------------------------------------------------------------------
    -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="logo">
                        <a href="<?php bloginfo('url');?>" class="tooltipster" title="Ir al Inicio"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/logo.svg" alt="<?php bloginfo('name');?>" class="img-responsive"></a>
                    </div>
                </div>
                <div class="col-md-8">
                    <p>
                        <a href="https://goo.gl/maps/giB3uX9S9fk" target="_new"><img src="<?php bloginfo('template_url');?>/recursos/img/iconos/googlemaps.svg" class="img-responsive icon-svg" alt="Google Maps" style="width:30px;height:30px;"/></a>
                        <a href="https://goo.gl/maps/giB3uX9S9fk" target="_blank">Av. Santa María 2450, piso 4 Providencia, Santiago, Chile</a> <a href="tel:+56224903200">(56) 2 2490 3200</a>
                    </p>
                </div>
                <div class="col-md-2">
                    <div class="social-networks">
                        <a href="https://www.youtube.com/user/AvellanedaVideos" title="Síguenos en YouTube" target="_blank" class="social-networks__youtube tooltipster">
                            <img src="<?php bloginfo('template_url');?>/recursos/img/iconos/youtube.svg" class="img-responsive" alt="Youtube" style="width:30px;height:30px;background-color:grey; border-radius:15px; padding:2px;"/>
                        </a>
                        <a href="https://www.instagram.com/inmobiliariaavellaneda/" title="Síguenos en Instagram" target="_blank" class="social-networks__instagram tooltipster">
                            <img src="<?php bloginfo('template_url');?>/recursos/img/iconos/instagram.svg" class="img-responsive" alt="Instagram"  style="width:30px;height:30px;background-color:grey; border-radius:15px; padding:2px;"/>
                        </a>
                        <a href="https://www.facebook.com/InmobiliariaAvellaneda" title="Síguenos en Facebook" class="social-networks__facebook tooltipster" target="_blank">
                            <img src="<?php bloginfo('template_url');?>/recursos/img/iconos/facebook.svg" class="img-responsive" alt="Facebook"  style="width:30px;height:30px;background-color:grey; border-radius:15px; padding:2px;"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="pie">
            <div class="container text-center">
                <strong>Avellaneda</strong> <?php echo date('Y'); ?>. Todos los derechos reservados. <span class="politica" data-toggle="modal" data-target="#politica">Política de privacidad</span>
            </div>
        </div>
    </footer>


    <!-- scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <?php wp_footer(); ?>
</body>
</html>