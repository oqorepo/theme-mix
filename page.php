<?php
get_header(); 
get_template_part('secciones/header-normal');
?>



<section class="pagina-interior">

<div class="container">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php the_content();?>

<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

</div>

</section>


<?php get_footer();?>