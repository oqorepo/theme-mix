<?php
get_header();
get_template_part('secciones/header-normal');
?>

<script>
$(document).ready(function() {
$('.head').css('border', '0px');
$('.linea-menu-bot').addClass('activo');

// Google Tag Manager
dataLayer.push({
'tipoPagina': 'Archivo de Promociones',
});
});
</script>












<!-- Título -->
<section class="seccion-oscura">
	<div class="container">
					<div class="row">
		<h1 class="display blanco">Avellaneda Corporativo</h1>
	</div>
	</div>
</section>
<!-- Título -->



<?php

$query = new WP_Query(array(
      'post_type'     => array('corporativo'),
      'posts_per_page'  => -1,
      'post_status'   => 'publish',

));
if ( $query->have_posts() ) : ?>

<section class="mas-promos">
  <div class="container">
    <div class="row">
      <?php while ( $query->have_posts() ) : $query->the_post(); ?>
      <div class="col-md-3 misma-altura-corporativo" style="margin:15px 0px 15px 0px;">
        <!-- Fotografía -->
        <?php
$imagenportada = get_field('imagen');
$size = 'corporativo';
$imagenportada_array = wp_get_attachment_image_src($imagenportada, $size);
$contenido = get_field('contenido');
$tipocontenido = get_field('tipo_de_contenido');
$videoid = get_field('ingresa_id_de_youtube');

        ?>

<?php if ($tipocontenido == "Texto"):?>

        <a href="<?php the_permalink();?>">
          <img src="<?php echo $imagenportada_array[0];?>" class="img-responsive img-full tooltipster" title="<?php the_title();?>"  alt="<?php the_title();?>" />
        </a>

<?php elseif ($tipocontenido == "Video"):?>


  <a href="<?php the_permalink();?>" title="<?php the_title();?>" class="tooltipster">
          <img src="http://i.ytimg.com/vi/<?php echo $videoid;?>/maxresdefault.jpg" class="img-responsive img-full tooltipster videoimagen"  alt="<?php the_title();?>" />


<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/play.svg" class="img-responsive" alt="Reproducir Video <?php the_title();?>" style="position: absolute; max-width: 50px; top:20%; left:0; right:0; margin:0 auto;"/>

        </a>
<?php endif;?>


        <p><strong><?php the_title();?></strong></p>

        <p><?php echo corporativo_extracto();?></p>
      </a>
    </div>
    <?php endwhile;?>
  </div>
</div>
</section>
<?php endif;?>


























<!-- Fin Promociones No Vigentes -->


<?php get_footer();?>