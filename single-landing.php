<?php
get_header();
get_template_part('secciones/header-normal');
?>
<script>
$(document).ready(function() {
$('.head').css('border', '0px');
$('.linea-menu-bot').addClass('activo');
// Google Tag Manager
dataLayer.push({
'Landing': '<?php the_title();?>',
'tipoPagina': 'Landing',
});
});
</script>

<!-- Título -->
<section class="seccion-oscura">
	<div class="container">
		<div class="row">
			<h1 class="display blanco"><?php the_title();?></h1>
		</div>
	</div>
</section>
<!-- Título -->

<!-- 1 Proyectos -->
	<?php
	$ids = get_field('selecciona_proyectos', false, false);
	$query = new WP_Query(array(
		'post_type' 		=> array('proyectos'),
		'posts_per_page'	=> -1,
		'post__in'			=> $ids,
		'post_status'		=> 'publish',
		'orderby'        	=> 'post__in',
	));
	if ( $query->have_posts() ) :
	$count = $query->post_count;?>
	<?php if ($count == "1"):?>

	<section>
		<?php while ( $query->have_posts() ) : $query->the_post();?>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="proyecto-destacado">
						<div class="solo-desktop">
							<div class="estado-proyecto-small-destacado">
							<?php
								$tag					= get_field('selecciona_tag_del_proyecto');
								$tag_personalizado 		= get_field('personalizar_tag');
								$slide_personalizado 	= get_field('av_tag_menu_txt');
							if ($tag == "Personalizar"):?>
							<span class="label label-default"><?php echo $tag_personalizado;?></span>
							<?php elseif ($tag == "Normal"):?>
							<?php elseif ($tag == "Slide personalizado"):?>
							<!-- <span class="label label-default"><?php //echo $slide_personalizado ;?></span> -->
							<?php else:?>
							<span class="label label-default"><?php echo $tag;?></span>
							<?php endif;?>
							</div>
						</div>
						<div class="solo-celular">
							<!-- Etiqueta del Proyecto -->
							<?php
							$tag = get_field('selecciona_tag_del_proyecto');
							if ($tag):?>
							<div class="estado-de-proyecto-wrap">
							<?php
								$tag					= get_field('selecciona_tag_del_proyecto');
								$tag_personalizado 		= get_field('personalizar_tag');
								$slide_personalizado 	= get_field('av_tag_menu_txt');
							if ($tag == "Personalizar"):?>
							<span class="label label-default"><?php echo $tag_personalizado;?></span>
							<?php elseif ($tag == "Normal"):?>
							<?php elseif ($tag == "Slide personalizado"):?>
							<!-- <span class="label label-default"><?php echo $slide_personalizado ;?></span> -->
							<?php else:?>
							<span class="label label-default"><?php echo $tag;?></span>
							<?php endif;?>
							</div>
							<?php endif;?>
							<!-- Fin Etiqueta del Proyecto -->
						</div>
						<!-- Fotografías -->
						<?php
						if( have_rows('slides_principal') ):?>
						<?php
						while ( have_rows('slides_principal') ) : the_row();
						$fotografia 			= get_sub_field('fotografia_slide');
						$fotografia_mobile 		= get_sub_field('fotografia_mobile');
						$size 					= 'slides';
						$image_array 			= wp_get_attachment_image_src($fotografia, $size);
						$image_array_mobile 	=  wp_get_attachment_image_src($fotografia_mobile, $size);
						$titulo_imagen 			= get_the_title(get_sub_field($fotografia));
						?>
						<img src="<?php echo $image_array_mobile[0];?>" class="img-responsive" alt="<?php echo $titulo_imagen;?>">
						<?php break;?>
						<?php endwhile;?>
						<?php endif;?>
						<!-- Fotografías -->
						<!-- Descripción -->
						<div class="solo-desktop">
							<div class="descripcion-landing">
								<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
								<h4 class="altura-titulos-landing"><?php the_title(); ?></h4>
								<hr></hr>
								<p><?php the_field('bloque_rojo');?></p>
								<button href="#formulario-landing" id="verProyectoLanding" class="btn btn-rojo click" data-proyecto="<?php the_title(); ?>" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});"><i class="far fa-envelope"></i> Solicitar más info</button>
							</div>
						</div>
						<div class="solo-celular">
							<div class="descripcion-landing text-center">
								<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
								<h4 class="altura-titulos-landing"><?php the_title(); ?></h4>
								<hr></hr>
								<p><?php the_field('bloque_rojo');?></p>
								<button href="#formulario-landing" id="verProyectoLanding" class="btn btn-rojo click" data-proyecto="<?php the_title(); ?>" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});"><i class="far fa-envelope"></i> Solicitar más info</button>
							</div>
						</div>
						<!-- Descripción -->
					</div>
				</div>
			</div>
		</div>
		<?php endwhile;?>
		<?php endif;?>
	</section>
	<?php endif;?>
	<?php wp_reset_postdata(); ?>
<!-- 1 Proyectos -->

<!-- Big Slide -->
<section>
	<?php if( have_rows('slides') ):?>
	<!-- Cargador de Slides -->
	<div class="loading_slides loading_slides_landing">
		<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/loading.svg" alt="Cargando <?php the_title();?>" />
	</div>
	<!-- Cargador de Slides -->
	<div class="owl-carousel owl-theme owl_landing_proyecto">
		<?php while ( have_rows('slides') ) : the_row();
		$fotografia 		= get_sub_field('slide_desktop');
		$fotografia_mobile 	= get_sub_field('slide_mobile');
		$size 				= 'slides';
		$image_array 		= wp_get_attachment_image_src($fotografia, $size);
		$image_array_mobile = wp_get_attachment_image_src($fotografia_mobile, $size);
		$titulo_imagen 		= get_the_title(get_sub_field($fotografia));
		?>
		<picture>
			<source
			media="(max-width: 920px)"
			srcset="<?php echo $image_array_mobile[0];?>">
			<img src="<?php echo $image_array[0];?>" class="img-responsive" alt="<?php echo $titulo_imagen;?>">
		</picture>
		<?php endwhile;?>
	</div>
	<!-- Controles Slide -->
	<div id="controles-slides">
		<div class="siguiente">
			<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Siguiente" class="tooltipster2 slide-landing-siguiente" title="Ver Siguiente" />
		</div>
		<div class="anterior">
			<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/flecha.svg" alt="Flecha Anterior" class="tooltipster2 slide-landing-anterior" title="Ver Anterior" />
		</div>
	</div>
	<!-- Fin Controles Slide -->
	<?php endif;?>
</section>
<!-- Big Slide -->

<!-- Mensaje -->
<?php if ($count == "1"):?>
<section class="gris-claro">
	<div class="frase">
		<div class="container">
			<div class="row">
				<div class="solo-desktop">
					<div class="col-md-8 pull-right text-right">
						<?php the_field('frase_marketing_landing');?>
					</div>
				</div>
				<div class="solo-celular">
					<div class="col-md-8 text-center">
						<?php the_field('frase_marketing_landing');?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;?>
<!-- Mensaje -->

<!-- Si que existen más de 2 proyectos -->
<?php
$ids = get_field('selecciona_proyectos', false, false);
$query = new WP_Query(array(
							'post_type' 		=> array('proyectos'),
					'posts_per_page'	=> -1,
									'post__in'			=> $ids,
							'post_status'		=> 'publish',
					'orderby'        	=> 'post__in',
));
?>
<?php if ( $query->have_posts() ) :
$count = $query->post_count;?>
<?php if ($count == "2"):?>
<section class="proyectos-landing">
	<div class="container">
		<div class="row">
			<!-- Proyectos -->
			<?php while ( $query->have_posts() ) : $query->the_post();?>
			<div class="col-md-6 bloques-landing">
				<div class="solo-desktop">
					<div class="estado-proyecto-small" style="right:20px;">
						<?php
						// variables
						$tag = get_field('selecciona_tag_del_proyecto');
						$tag_personalizado = get_field('personalizar_tag');
						if ($tag == "Personalizar"):?>
						<span class="label label-default"><?php echo $tag_personalizado;?></span>
						<?php elseif ($tag == "Slide personalizado"):?>
						<?php elseif ($tag == "Normal"):?>
						<?php else:?>
						<span class="label label-default"><?php echo $tag;?></span>
						<?php endif;?>
					</div>
				</div>
				<div class="solo-celular">
					<!-- Etiqueta del Proyecto -->
					<?php
					$tag = get_field('selecciona_tag_del_proyecto');
					if ($tag):?>
					<div class="estado-de-proyecto-wrap">
						<?php
						$tag_personalizado = get_field('personalizar_tag');
						if ($tag == "Personalizar"):?>
						<div class="container"><div class="estado-wrap"><div class="estado-de-proyecto"><?php echo $tag_personalizado;?></div></div></div>
						<?php elseif ($tag == "Slide personalizado"):?>
						<?php elseif ($tag == "Normal"):?>
						<?php else:?>
						<div class="container"><div class="estado-wrap"><div class="estado-de-proyecto"><?php echo $tag;?></div></div></div>
						<?php endif;?>
					</div>
					<?php endif;?>
					<!-- Fin Etiqueta del Proyecto -->
				</div>
				<!-- Fotografías -->
				<?php
				if( have_rows('slides_principal') ):?>
				<?php
				while ( have_rows('slides_principal') ) : the_row();
													$fotografia 			= get_sub_field('fotografia_slide');
											$fotografia_mobile 		= get_sub_field('fotografia_mobile');
																	$size 					= 'slides';
													$image_array 			= wp_get_attachment_image_src($fotografia, $size);
									$image_array_mobile 	=  wp_get_attachment_image_src($fotografia_mobile, $size);
													$titulo_imagen 			= get_the_title(get_sub_field($fotografia));
				?>
				<picture>
					<source
					media="(max-width: 920px)"
					srcset="<?php echo $image_array_mobile[0];?>">
					<img src="<?php echo $image_array[0];?>" class="img-responsive" alt="Proyecto <?php the_title();?>">
				</picture>
				<?php break;?>
				<?php endwhile;?>
				<?php endif;?>
				<!-- Fotografías -->
				<!-- Descripción -->
				<div class="solo-desktop">
					<div class="descripcion-landing">
						<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
						<h4 class="altura-titulos-landing"><?php the_title(); ?></h4>
						<hr></hr>
						<p><?php the_field('bloque_rojo');?></p>
						<button href="#formulario-landing" id="verProyectoLanding" class="btn btn-rojo click" data-proyecto="<?php the_title(); ?>" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});"><i class="far fa-envelope"></i> Solicitar más info</button>
					</div>
				</div>
				<div class="solo-celular">
					<div class="descripcion-landing text-center">
						<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
						<h4 class="altura-titulos-landing"><?php the_title(); ?></h4>
						<hr></hr>
						<p><?php the_field('bloque_rojo');?></p>
						<button href="#formulario-landing" id="verProyectoLanding" class="btn btn-rojo click" data-proyecto="<?php the_title(); ?>" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});"><i class="far fa-envelope"></i> Solicitar más info</button>
					</div>
				</div>
				<!-- Descripción -->
			</div>
			<?php endwhile;?>
			<?php else:?>
			<?php endif;?>
		</div>
	</div>
</section>
<?php endif;?>
<?php wp_reset_postdata(); ?>
<!-- Si que existen más de 2 proyectos -->
<!-- Si que existen más de 3 proyectos -->
<?php
$ids = get_field('selecciona_proyectos', false, false);
$query = new WP_Query(array(
					// 'post_type'      	=> 'proyectos',
							'post_type' 		=> array('proyectos'),
					'posts_per_page'	=> -1,
									'post__in'			=> $ids,
							'post_status'		=> 'publish',
					'orderby'        	=> 'post__in',
));
?>
<?php if ( $query->have_posts() ) :
$count = $query->post_count;?>
<?php if ($count > "3"):?>
<section class="proyectos-landing">
	<div class="container">
		<div class="row">
			<!-- Proyectos -->
			<?php while ( $query->have_posts() ) : $query->the_post();?>
			<div class="col-md-3 bloques-landing">
				<div class="solo-desktop">
				<?php
						$tag					= get_field('selecciona_tag_del_proyecto');
						$tag_personalizado 		= get_field('personalizar_tag');
						$slide_personalizado 	= get_field('av_tag_menu_txt');
					if ($tag == "Personalizar"):?>
					<span class="label label-default"><?php echo $tag_personalizado;?></span>
					<?php elseif ($tag == "Normal"):?>
					<?php elseif ($tag == "Slide personalizado"):?>
					<!-- <span class="label label-default"><?php // echo $slide_personalizado ;?></span> -->
					<?php else:?>
					<span class="label label-default"><?php echo $tag;?></span>
					<?php endif;?>
					</div>
				</div>
				<div class="solo-celular">
					<!-- Etiqueta del Proyecto -->
					<?php
					$tag = get_field('selecciona_tag_del_proyecto');
					if ($tag):?>
					<div class="estado-de-proyecto-wrap">
						<?php
						$tag_personalizado = get_field('personalizar_tag');
						if ($tag == "Personalizar"):?>
						<div class="container"><div class="estado-wrap"><div class="estado-de-proyecto"><?php echo $tag_personalizado;?></div></div></div>
						<?php elseif ($tag == "Normal"):?>
						<?php elseif ($tag == "Slide personalizado"):?>
						<?php else:?>
						<div class="container"><div class="estado-wrap"><div class="estado-de-proyecto"><?php echo $tag;?></div></div></div>
						<?php endif;?>
					</div>
					<?php endif;?>
					<!-- Fin Etiqueta del Proyecto -->
				</div>
				<!-- Fotografías -->
				<?php
				if( have_rows('slides_principal') ):?>
				<?php
				while ( have_rows('slides_principal') ) : the_row();
															$fotografia 			= get_sub_field('fotografia_slide');
											$fotografia_mobile 		= get_sub_field('fotografia_mobile');
																							$size 					= 'slides';
															$image_array 			= wp_get_attachment_image_src($fotografia, $size);
							$image_array_mobile 	=  wp_get_attachment_image_src($fotografia_mobile, $size);
															$titulo_imagen 			= get_the_title(get_sub_field($fotografia));
				?>
				<a href="<?php the_permalink(); ?>"><img src="<?php echo $image_array_mobile[0];?>" class="img-responsive" alt="<?php echo $titulo_imagen;?>"></a>
				<?php break;?>
				<?php endwhile;?>
				<?php endif;?>
				<!-- Fotografías -->
				<!-- Descripción -->
				<div class="solo-desktop">
					<div class="descripcion-landing">
						<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
						<h4 class="altura-titulos-landing"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<hr></hr>
						<div class="altura-bloque-rojo"><?php the_field('bloque_rojo');?></div>
						<button href="#formulario-landing" id="verProyectoLanding" class="btn btn-rojo click" data-proyecto="<?php the_title(); ?>" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});"><i class="far fa-envelope"></i> Solicitar más info</button>
					</div>
				</div>
				<div class="solo-celular">
					<div class="descripcion-landing text-center">
						<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
						<h4 class="altura-titulos-landing"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<hr></hr>
						<p><?php the_field('bloque_rojo');?></p>
						<button href="#formulario-landing" id="verProyectoLanding" class="btn btn-rojo click" data-proyecto="<?php the_title(); ?>" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});"><i class="far fa-envelope"></i> Solicitar más info</button>
					</div>
				</div>
				<!-- Descripción -->
			</div>
			<?php endwhile;?>
			<?php else:?>
			<?php endif;?>
		</div>
	</div>
</section>
<?php endif;?>
<?php wp_reset_postdata(); ?>
<!-- Si que existen igual  3 proyectos -->
<!-- Si que existen más de 3 proyectos -->
<?php
$ids = get_field('selecciona_proyectos', false, false);
$query = new WP_Query(array(
					// 'post_type'      	=> 'proyectos',
							'post_type' 		=> array('proyectos'),
					'posts_per_page'	=> -1,
									'post__in'			=> $ids,
							'post_status'		=> 'publish',
					'orderby'        	=> 'post__in',
));
?>
<?php if ( $query->have_posts() ) :
$count = $query->post_count;?>
<?php if ($count == "3"):?>
<section class="proyectos-landing">
	<div class="container">
		<div class="row">
			<!-- Proyectos -->
			<?php while ( $query->have_posts() ) : $query->the_post();?>
			<div class="col-md-4 bloques-landing">
				<div class="solo-celular">
					<!-- Etiqueta del Proyecto -->
					<?php
					$tag = get_field('selecciona_tag_del_proyecto');
					if ($tag):?>
					<div class="estado-de-proyecto-wrap">
					<?php
						$tag					= get_field('selecciona_tag_del_proyecto');
						$tag_personalizado 		= get_field('personalizar_tag');
						$slide_personalizado 	= get_field('av_tag_menu_txt');
					if ($tag == "Personalizar"):?>
					<span class="label label-default"><?php echo $tag_personalizado;?></span>
					<?php elseif ($tag == "Normal"):?>
					<?php elseif ($tag == "Slide personalizado"):?>
					<!-- <span class="label label-default"><?php //echo $slide_personalizado ;?></span> -->
					<?php else:?>
					<span class="label label-default"><?php echo $tag;?></span>
					<?php endif;?>
					</div>
					<?php endif;?>
					<!-- Fin Etiqueta del Proyecto -->
				</div>
				<div class="solo-desktop">
					<div class="estado-proyecto-small" style="right:20px;">
					<?php
						$tag					= get_field('selecciona_tag_del_proyecto');
						$tag_personalizado 		= get_field('personalizar_tag');
						$slide_personalizado 	= get_field('av_tag_menu_txt');
					if ($tag == "Personalizar"):?>
					<span class="label label-default"><?php echo $tag_personalizado;?></span>
					<?php elseif ($tag == "Normal"):?>
					<?php elseif ($tag == "Slide personalizado"):?>
					<!-- <span class="label label-default"><?php //echo $slide_personalizado ;?></span> -->
					<?php else:?>
					<span class="label label-default"><?php echo $tag;?></span>
					<?php endif;?>
					</div>
				</div>
				<!-- Fotografías -->
				<?php
				if( have_rows('slides_principal') ):?>
				<?php
				while ( have_rows('slides_principal') ) : the_row();
															$fotografia 			= get_sub_field('fotografia_slide');
											$fotografia_mobile 		= get_sub_field('fotografia_mobile');
																							$size 					= 'slides';
															$image_array 			= wp_get_attachment_image_src($fotografia, $size);
							$image_array_mobile 	=  wp_get_attachment_image_src($fotografia_mobile, $size);
															$titulo_imagen 			= get_the_title(get_sub_field($fotografia));
				?>
				<a href="<?php the_permalink(); ?>"><img src="<?php echo $image_array_mobile[0];?>" class="img-responsive" alt="<?php echo $titulo_imagen;?>"></a>
				<?php break;?>
				<?php endwhile;?>
				<?php endif;?>
				<!-- Fotografías -->
				<!-- Descripción -->
				<div class="solo-desktop">
					<div class="descripcion-landing">
						<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
						<h4 class="altura-titulos-landing"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<hr></hr>
						<p><?php the_field('bloque_rojo');?></p>
						<button href="#formulario-landing" id="verProyectoLanding" class="btn btn-rojo click" data-proyecto="<?php the_title(); ?>" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});"><i class="far fa-envelope"></i> Solicitar más info</button>
					</div>
				</div>
				<div class="solo-celular">
					<div class="descripcion-landing text-center">
						<h5><?php get_template_part('modulos/ubicacion-actual');?></h5>
						<h4 class="altura-titulos-landing"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<hr></hr>
						<p><?php the_field('bloque_rojo');?></p>
						<button href="#formulario-landing" id="verProyectoLanding" class="btn btn-rojo click" data-proyecto="<?php the_title(); ?>" onclick="dataLayer.push({'nombreProyecto': '<?php the_title();?>'});"><i class="far fa-envelope"></i> Solicitar más info</button>
					</div>
				</div>
				<!-- Descripción -->
			</div>
			<?php endwhile;?>
			<?php else:?>
			<?php endif;?>
		</div>
	</div>
</section>
<?php endif;?>
<?php wp_reset_postdata(); ?>
<!-- Mensaje -->
<?php if ($count > "3"):?>
<section class="gris-claro">
	<div class="frase">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<?php the_field('frase_marketing_landing');?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;?>
<!-- Mensaje -->
<!-- Mensaje -->
<?php if ($count == "2"):?>
<section class="gris-claro">
	<div class="frase">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<?php the_field('frase_marketing_landing');?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;?>
<!-- Mensaje -->
<section class="formulario-y-texto">
	<?php $contenido = get_field('contenido_enriquecido');?>
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<?php $titulo_content = get_field('titulo_contenido');?>
				<?php if ($titulo_content):?>
				<h2 class="titulos"><?php echo $titulo_content;?></h2>
				<?php else:?>
				<h2 class="titulos">Información</h2>
				<?php endif;?>
				<?php if ($contenido):?>
				<?php echo $contenido;?>
				</br>
				<?php endif;?>
				<?php
				$ids = get_field('selecciona_proyectos', false, false);
				$query = new WP_Query(array(
									// 'post_type'      	=> 'proyectos',
									'post_type' 		=> array('proyectos'),
									'posts_per_page'	=> -1,
									'post__in'			=> $ids,
									'post_status'		=> 'publish',
									'orderby'        	=> 'post__in',
				));
				?>
				<?php if ( $query->have_posts() ) :?>
				<table class="table table-striped table-bordered tabla-seo">
					<thead>
						<tr>
							<th scope="col">Proyecto</th>
							<th scope="col">Comuna</th>
							<th scope="col">Desde</th>
						</tr>
					</thead>
					<tbody>
						<?php while ( $query->have_posts() ) : $query->the_post();?>
						<tr>
							<th scope="row"><?php the_title();?></th>
							<td><?php get_template_part('modulos/ubicacion-actual');?></td>
							<td><?php the_field('bloque_rojo');?></td>
						</tr>
						<?php endwhile;?>
					</tbody>
				</table>
				<?php endif;?>
				<?php wp_reset_postdata(); ?>
			</div>
			<style>
			.formulario-y-texto a {
				color:#e5302d;
				font-weight: bold;
			}
			</style>
			<!-- Formulario Gracias -->
			<div id="gracias-formulario-proyectos-landing" class="col-md-4 text-center pull-right">
				<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/enviado2.svg" alt="Mensaje Enviado" class="img-responsive icon-enviado">
				<h2>Gracias <span class="nombre-persona"></span></h2>
				<div><strong>Pronto te contactaremos con más información.</strong></div>
			</div>
			<!-- Formulario Gracias -->
			<div id="formulario-landing" class="col-md-4 pull-right">
				<h2 class="titulos">Solicita más información</h2>
				<?php
				$ids = get_field('selecciona_proyectos', false, false);
				$query = new WP_Query(array(
									// 'post_type'      	=> 'proyectos',
											'post_type' 		=> array('proyectos'),
									'posts_per_page'	=> -1,
													'post__in'			=> $ids,
											'post_status'		=> 'publish',
									'orderby'        	=> 'post__in',
				));
				?>
				<?php if ( $query->have_posts() ) :?>
				<?php $count = $query->post_count;?>
				<?php if ($count == "1"):?>
				<select aria-required="true" name="Proyectos" class="form-control form-control-blanco hide required">
					<?php while ( $query->have_posts() ) : $query->the_post();?>
					<option value="<?php the_title();?>"><?php the_title();?></option>
					<?php endwhile;?>
				</select>
				<?php else:?>
				<div class="form-group">
					<select aria-required="true" name="Proyectos" class="form-control form-control-blanco required hide">
						<option value="" disabled="disabled" selected class="required">Seleccione proyecto</option>
						<?php while ( $query->have_posts() ) : $query->the_post();?>
						<option value="<?php the_title();?>"><?php the_title();?></option>
						<?php endwhile;?>
					</select>
				</div>
				<?php endif;?>
				<?php endif;?>
				<?php wp_reset_postdata(); ?>
				<?php // localhost echo do_shortcode('[contact-form-7 id="975" title="Landing" html_id="formulario-proyectos-landing"]');?>
				<?php echo do_shortcode('[contact-form-7 id="1252" title="Landing" html_id="formulario-proyectos-landing"]');?>
			</div>
		</div>
	</div>
</section>
<!-- Fotografías de Entorno -->
<?php
$ids = get_field('selecciona_sectores', false, false);
$query = new WP_Query(array(
						'post_type' 		=> array('sectores'),
				'posts_per_page'	=> -1,
								'post__in'			=> $ids,
						'post_status'		=> 'publish',
				'orderby'        	=> 'post__in',
));
?>
<?php if ( $query->have_posts() ) :?>
<section class="seccion-con-espacio">
	<div class="container">
		<div class="row">
			<h2 class="titulos"><?php the_field('titulo_para_los_entornos');?></h2>
			<?php while ( $query->have_posts() ) : $query->the_post();?>
			<!-- Fotografías -->
			<?php
			if( have_rows('galeria_de_fotografias') ):?>
			<?php
			while ( have_rows('galeria_de_fotografias') ) : the_row();
										$fotografia 			= get_sub_field('fotografia');
														$size 					= 'fotografias-tab-cuadrado';
										$image_array 			= wp_get_attachment_image_src($fotografia, $size);
										$titulo_imagen 			= get_the_title(get_sub_field($fotografia));
								$imagen_array_full 		= wp_get_attachment_image_src($fotografia, 'full')
			?>
			<div class="col-md-3 descripcion-entorno">
				<a href="<?php echo $imagen_array_full[0];?>" data-fancybox="entorno" data-caption="<h4><?php the_title();?></h4><?php the_sub_field('descripcion_fotografia');?>">
					<img src="<?php echo $image_array[0];?>" class="img-responsive" alt="<?php echo $titulo_imagen;?>">
				</a>
				<?php the_sub_field('descripcion_fotografia');?>
			</div>
			<?php endwhile;?>
			<?php endif;?>
			<!-- Fotografías -->
			<?php endwhile;?>
		</div>
	</div>
</section>
<?php endif;?>
<!-- Fotografías de Entorno -->
<?php wp_reset_postdata(); ?>
<!-- Corporativo -->
<?php
$corporativo = get_field('hitos');?>
<?php if ($corporativo == "Si"):?>
<section class="formulario-y-texto">
	<div class="container">
		<div class="row">
			<?php
			$titulo_avellaneda = get_field('titulo_avellaneda', '9');
			$sub_titulo_avellaneda = get_field('sub-titulo_avellaneda', '9');
			?>
			<div class="container">
				<?php if($titulo_avellaneda):?>
				<h2 class="titulos"><?php echo $titulo_avellaneda;?>	</h2>
				<?php endif;?>
				<?php if($sub_titulo_avellaneda):?>
				<h4 class="bajadas"><?php echo $sub_titulo_avellaneda;?></h4>
				<?php endif;?>
			</div>
		</div>
	</div>
	<?php if( have_rows('hito', '315') ):?>
	<section class="destacados">
		<div class="container">
			<div class="row">
				<?php while ( have_rows('hito', '315') ) : the_row();
				$icono = get_sub_field('icono');
				$titulo = get_sub_field('titulo');
				$texto = get_sub_field('texto');
				?>
				<div class="col-md-4 destacados">
					<img src="<?php echo $icono;?>" class="img-responsive icono-destacado" alt="Hito Destacado Avellaneda" />
					<h2><?php echo $titulo;?></h2>
					<div class="bajo-destacado"><?php echo $texto;?></div>
				</div>
				<?php endwhile;?>
			</div>
		</div>
		<div class="center-block text-center">
			<a href="<?php bloginfo('url');?>/empresa" onclick="dataLayer.push({'visitaCorporativa': 'Información Corporativa'});">
				<button class="btn btn-rojo btn-lg "><i class="fas fa-home"></i> Saber más de Avellaneda</button>
			</a>
		</div>
	</section>
	<?php endif;?>
	<?php wp_reset_postdata(); ?>
	<?php endif;?>
</section>
<!--  -->
<?php get_footer();?>