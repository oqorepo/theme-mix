$(function () {






    $('.btn-ver-mas-plantas').on('click', function (e) {

        e.preventDefault();
        e.stopPropagation();
        $('section.ver-mas-plantas').slideToggle();

        $('section.single-plantas').toggleClass('open');
        // if (!$(event.target).closest('section.ver-mas-plantas').length) {
        //     if ($('section.ver-mas-plantas').is(":visible")) {
        //         $('section.ver-mas-plantas').hide();
        //     }
        // }


        $('.bloque-mas-plantas').matchHeight();


        //        $('.descripcion-ver-mas-plantas').matchHeight({
        //     property: 'min-height',
        //     byRow: true,
        // });



    });



    // Altura
    $('.altura-proyectos-buscador').matchHeight({
        property: 'min-height',
        byRow: false,
    });
    $('.altura-proyectos').matchHeight();

    $('.altura-titulos').matchHeight();
    $('.altura-plantas').matchHeight();
    $('.misma-altura-proyectos').matchHeight();
    $('.altura-bloque-rojo').matchHeight({
        property: 'min-height',
        byRow: false,
    });
    $('.altura-titulos-landing').matchHeight({
        property: 'min-height',
        byRow: false,
    });






    // Menu Hover
    $('ul.nav li.dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
    });


    // Compactar descripción para ver fotos a mayor tamaño
    $('.buscador').on('click', function () {




        $('.buscador-resultados').slideToggle();
        $('.pregunta_1').slideDown();
        $('.pregunta_2, .pregunta_3').hide();
        $(this).toggleClass('activo');
        $('.top').toggleClass('activo');
        $('.btn-gris').removeClass('btn-activo');
        $('.btn-rojo').removeClass('btn-activo');
    });
    $('.cerrar-buscador').on('click', function () {
        $('.santiago').removeClass('btn-activo');
        $('.regiones').removeClass('btn-activo');
        $('.buscador-resultados').slideUp();
        $('.pregunta_2').hide();
        $('.pregunta_3').slideUp();
        $('.resultados-listado-proyecto').hide();
        $('.buscador').removeClass('activo');
        $('.top').css('border-bottom', '1px solid #d5d5d5');
        $('.resultados-listado-proyecto').hide();
        $('.btn-gris').removeClass('btn-activo');
    });
    $('.btn-volver').on('click', function () {
        $('.pregunta_2').hide();
        $('.pregunta_3').hide();
        $('.pregunta_1').fadeIn();
        $('.regiones, .santiago').removeClass('btn-activo');
        $('.btn-gris').removeClass('btn-activo');
    });
    $('.santiago').on('click', function () {
        $(this).addClass('btn-activo');
        $('.pregunta_1').hide();
        $('.pregunta_3').hide();
        $('.regiones').removeClass('btn-activo');
        $('.pregunta_2').fadeIn();
    });
    $('.regiones').on('click', function () {
        $(this).addClass('btn-activo');
        $('.pregunta_1').hide();
        $('.pregunta_2').hide();
        $('.santiago').removeClass('btn-activo');
        $('.pregunta_3').fadeIn();
    });



    // --------------------------------------------------------------------------------------------
    // ----------------------- Carga imagenes de la vista previa en el contenedor principal
    // --------------------------------------------------------------------------------------------
    $('.vistas-previas li').each(function (index, value) {
        $(this).on('click', function () {
            // Carga foto
            foto_url = $(this).children().attr('src');
            $(this).parent().parent().find('.carga-fotos').children().attr('src', foto_url);
            // Animar Descripción
            tips = $(this).parent().parent().find('.slide-proyectos-descripcion');
            tips.children('.slide-compactar img').addClass('slide-compactar-open img');
            tips.children('.tips').hide();
            tips.find('h2').addClass('slide-proyectos-descripcion-small');
            // al hacer click en video
        });
    });



    // --------------------------------------------------------------------------------------------
    // ----------------------- Al hacer click sobre la descripción esta disminuye de tamaño
    // --------------------------------------------------------------------------------------------
    $('.slide-compactar').each(function (index, value) {
        $(this).on('click', function () {
            $(this).children().toggleClass('slide-compactar-open');
            $(this).parent().children('.tips').toggle();
            $(this).next('h2').toggleClass('slide-proyectos-descripcion-small');
        });
    });

    // --------------------------------------------------------------------------------------------
    // ----------------------- Header
    // --------------------------------------------------------------------------------------------
    $(document).on("scroll", function () {
        if ($(document).scrollTop() > 175) {
            $('.menu-fixed').slideDown(400);
        } else {
            $('.menu-fixed').slideUp(400);
        }



        if ($(this).scrollTop() > 300) {
            $('.subir').fadeIn();
        } else {
            $('.subir').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('.subir').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 1600);
        return false;
    });



    // --------------------------------------------------------------------------------------------
    // ----------------------- Tooltipster
    // --------------------------------------------------------------------------------------------

    $('.tooltipster').tooltipster({
        theme: 'tooltipster-shadow',
        plugins: ['follower'],
        offset: [20, 20],
    });
    $('.tooltipster2').tooltipster({
        theme: 'tooltipster-shadow',
        // offset: [20, 20],
    });
    // --------------------------------------------------------------------------------------------
    // ----------------------- Al hacer click en un enlace #
    // --------------------------------------------------------------------------------------------
    $('.click').click(function (e) {
        // prevent default action
        e.preventDefault();
        // get id of target div (placed in href attribute of anchor element)
        // and pass it to the scrollToElement function
        // also, use 2000 as an argument for the scroll speed (2 seconds, 2000 milliseconds)
        scrollToElement($(this).attr('href'), 900);
    });
    var scrollToElement = function (el, ms) {
        var speed = (ms) ? ms : 600;
        $('html,body').animate({
            scrollTop: $(el).offset().top - 119
        }, speed);
    }
    $('.click2').click(function (e) {
        // prevent default action
        e.preventDefault();
        // get id of target div (placed in href attribute of anchor element)
        // and pass it to the scrollToElement function
        // also, use 2000 as an argument for the scroll speed (2 seconds, 2000 milliseconds)
        scrollToElement($(this).attr('href'), 900);
    });
    var scrollToElement = function (el, ms) {
        var speed = (ms) ? ms : 600;
        $('html,body').animate({
            scrollTop: $(el).offset().top - 160
        }, speed);
    }


    $('.menu-proyectos a').each(function () {
        $(this).on('click', function () {
            $('.menu-proyectos a').children().removeClass('activo');
            $(this).children().addClass('activo');
        })
    });





    // Esconde filtros de plantas, cuando no existen resultados
    if ($('.plantas-grid .1dorm').length == 0) {
        $('.av-filter-i .1dormitorio').hide();
    }
    if ($('.plantas-grid .2dorm').length == 0) {
        $('.av-filter-i .2dormitorio').hide();
    }
    if ($('.plantas-grid .3dorm').length == 0) {
        $('.av-filter-i .3dormitorio').hide();
    }
    if ($('.plantas-grid .4dorm').length == 0) {
        $('.av-filter-i .4dormitorio').hide();
    }

    // Esconde filtros de plantas, cuando no existen resultados
    if ($('.plantas-grid-ii .1dorm').length == 0) {
        $('.av-filter-ii .1dormitorio').hide();
    }
    if ($('.plantas-grid-ii .2dorm').length == 0) {
        $('.av-filter-ii .2dormitorio').hide();
    }
    if ($('.plantas-grid-ii .3dorm').length == 0) {
        $('.av-filter-ii .3dormitorio').hide();
    }
    if ($('.plantas-grid-ii .4dorm').length == 0) {
        $('.av-filter-ii .4dormitorio').hide();
    }

    // Acciones del Menú
    $(".menu_proyectos_re, .proyectos_re").hover(function () {
        $(this).addClass('open');
        $(this).removeClass('cerrado');
        $('.menu_proyectos_re').addClass('activo');
        $('.proyectos_re').addClass('open');
        $('.menu_proyectos_rm').removeClass('activo');
        $('.linea-menu-bot').addClass('activo');
    }).mouseleave(function () {
        $('.proyectos_re').addClass('cerrado');
        $(this).removeClass('open');
        $(this).removeClass('activo');
        $('.proyectos_re').removeClass('open');
        $('.menu_proyectos_re').removeClass('activo');
        $('.linea-menu-bot').removeClass('activo');
    });
    $(".menu_proyectos_rm, .proyectos_rm").hover(function () {
        $(this).addClass('open');
        $(this).removeClass('cerrado');
        $('.menu_proyectos_rm').addClass('activo');
        $('.proyectos_rm').addClass('open');
        $('.menu_proyectos_re').removeClass('activo');
        $('.linea-menu-bot').addClass('activo');
    }).mouseleave(function () {
        $('.proyectos_rm').addClass('cerrado');
        $(this).removeClass('open');
        $(this).removeClass('activo');
        $('.proyectos_rm').removeClass('open');
        $('.menu_proyectos_rm').removeClass('activo');
        $('.linea-menu-bot').removeClass('activo');
    });




    // Cambiar fotografía según la vista previa y añade la descripción

    $('.tab-pane').each(function (index, value) {
        $(this).find('.miniaturas_de_tabs').on('click', function () {
            $(this).parent().find('.portada_tab').removeClass('miniatura_activa_portada');
            $(this).parent().children().children().removeClass('miniatura_activa');
            $(this).children().addClass('miniatura_activa');
            imagen = $(this).children().attr('src');
            descripcion = $(this).children('.descripcion_tab').html();
            $(this).parent().parent().find('.carga-fotos').attr('src', imagen);
            $(this).parent().parent().find('.descripcion_galeria_proyectos').html(descripcion);
        });
    });



    var $options = $("select[name='listado-de-proyectos']> option").clone();
    $('select[name="listaproyectos"]').append($options);
    $('select[name="listaproyectos"]').find("option").eq(0).remove();


    $("#menu").mmenu({
        "slidingSubmenus": false,
        "extensions": [
            "theme-dark"
        ],
        "offCanvas": {
            "position": "right"
        },
        "counters": false,
        "navbars": [{
            "position": "top"
        }, {
            "position": "bottom",
            "content": [
                "<a class='fas fa-envelope' href='mailto:contacto@avellaneda.cl/'></a>",
                "<a class='fab fa-facebook' href='https://www.facebook.com/InmobiliariaAvellaneda/' target='_blank'></a>",
                "<a class='fab fa-instagram' href='https://www.instagram.com/inmobiliariaavellaneda/' target='_blank'></a>",
                "<a class='fab fa-youtube' href='https://www.youtube.com/user/AvellanedaVideos/' target='_blank'></a>",
            ]
        }]
    });

    $('.btn-siguiente-tab').on('click', function () {
        $('.tabs-celular li.active').next('li').find('a').trigger('click');
    });
    $('.btn-anterior-tab').on('click', function () {
        $('.tabs-celular li.active').prev('li').find('a').trigger('click');
    });




    window.sr = ScrollReveal({
        reset: true
    });
    // Customizing a reveal set
    sr.reveal('.gris-claro', {
        duration: 500,
        delay: 200,
    });
    // sr.reveal('.slide-proyectos', { origin: 'bottom', duration: 500, delay:400 });
    sr.reveal('#exp-avellaneda', {
        reset: false,
        origin: 'bottom',
        duration: 500,
        delay: 600
    });
    sr.reveal('.slide-proyectos', {
        reset: true,
        origin: 'bottom',
        duration: 500,
        delay: 600
    });


    /*! konami-js v1.0.1 | http://mck.me/mit-license */
    var Konami = {};
    (function (d, e) {
        var f = d.sequence = function () {
            var b = Array.prototype.slice.call(arguments),
                c = 0;
            return function (a) {
                a = a || e.event;
                a = a.keyCode || a.which || a;
                if (a === b[c] || a === b[c = 0]) a = b[++c], "function" === typeof a && (a(), c = 0)
            }
        };
        d.code = function (b) {
            return f(38, 38, 40, 40, 37, 39, 37, 39, 66, 65, b)
        }
    })(Konami, window);


    $(document).on('keyup',
        Konami.code(function () {
            $('#oqo').fadeIn('slow');
        })
    );




    $('ul.menu-principal li').hover(function () {


        $('.linea-menu-bot').addClass('activo');

        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        $('.linea-menu-bot').removeClass('activo');

    });


    $('.dropdown-toggle').removeAttr('title');


    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var iframe = $(e.relatedTarget.hash).find('iframe');
        var src = iframe.attr('src');
        iframe.attr('src', '');
        iframe.attr('src', src);
    });


    $('.btn-anterior-tab').hide();
    $('.btn-siguiente-tab').on('click', function () {
        $('.btn-anterior-tab').fadeIn();
        if ($('ul.tabs-celular li').last().hasClass('active')) {
            $(this).fadeOut();
        }
    });
    $('.btn-anterior-tab').on('click', function () {
        $('.btn-siguiente-tab').fadeIn();

        if ($('ul.tabs-celular li').first().hasClass('active')) {
            $(this).fadeOut();
        }
    });


    $('button#verProyectoLanding').each(function (index, value) {
        $(this).on('click', function () {
            console.log('Click en el botón landing')
            nombreProyecto = $(this).data('proyecto');
            console.log(nombreProyecto);
            $('.proyecto-a-cotizar').text(nombreProyecto);
            $('input[name="Proyecto"]').val(nombreProyecto);
            $('select[name="Proyectos"]').val(nombreProyecto);
            $('select[name="listaProyectosLanding"]').val(nombreProyecto);
        });
    });

    var valorSeleccionado = $('select[name="Proyectos"] option:selected').val();
    $('input[name="Proyecto"]').val(valorSeleccionado);

    $('select[name="Proyectos"]').on('change', function () {
        var valorSeleccionado = $('select[name="Proyectos"] option:selected').val();
        $('input[name="Proyecto"]').val(valorSeleccionado);
    })



    var onClass = "on";
    var showClass = "show";

    $("input").bind("checkval", function () {
        var label = $(this).closest(".form-group").children('.ayuda-form');
        if (this.value !== "") {
            label.addClass(showClass);
        } else {
            label.removeClass(showClass);
        }
    }).on("keyup", function () {
        $(this).trigger("checkval");
    }).on("focus", function () {
        $(this).find(".ayuda-form").addClass(onClass);
    }).on("blur", function () {
        $(this).find(".ayuda-form").removeClass(onClass);
    }).trigger("checkval");


    $('.categoria').children().each(function () {
        var busca = $(this).children('div.col-md-10').children('.paginas-landing').find('li');

        if (!busca.hasClass('si-existe')) {

            $(this).children().hide();
            $(this).children().css('border', '0px');
        }

    });



    $('.footer-compartir').on('click', function () {

        $(this).find('.fa-inverse').toggleClass('fa-share-alt fa-times-circle');
        $('.compartir-iconos').slideToggle();
    });

    // --------------------------------------------------------------------------------------------
    // ----------------------- POSTVENTA
    // --------------------------------------------------------------------------------------------

    var iframeHeight = window.innerHeight;
    $('.codegena_iframe').height(iframeHeight);
    $('.codegena_iframe iframe').height(iframeHeight+70);
    function iframePvHeight() {
        $('.codegena_iframe').height(iframeHeight);
        $('.codegena_iframe iframe').height(iframeHeight+70);
    }


    function checkBtnClassPostVenta() {
        var avTabHasClass = $('.av-tab-postventa .tab-pane').hasClass('active');
        console.log(avTabHasClass);
        if (avTabHasClass) {
            $('.av-image-postventa').hide();
        }
    }

    $(window).resize(function () {
        if ($(window).width() >= 980) {
            iframePvHeight();
        }
    });

    $('.av-tab-link').on('click', function () {
       iframePvHeight();
        $('.av-image-postventa').hide();
        $('.av-postventa').slideToggle();
    });
    $('.av-select-project').on('click', function () {
        $('.av-postventa').slideToggle();
        $('.av-image-postventa').show();
        $('.av-tab-postventa li').removeClass('active');
        $('#inmobiliaria').removeClass('active');
        $('#constructora').removeClass('active');
    });

    // --------------------------------------------------------------------------------------------
    // ----------------------- Etiquetas de proyecto
    // --------------------------------------------------------------------------------------------


    $('.estado-proyecto-small-menu span').each(function () {
        loc = $(this).text();
        if (loc.toLowerCase().indexOf('verde') > -1) {
            $(this).css('background', 'green');
        } else if (loc.toLowerCase().indexOf('agotado') > -1) {
            $(this).css('background', 'grey');
        } else if (loc.toLowerCase().indexOf('homy') > -1) {
            $(this).css('background', '#008FA9');
        }
    });

    $('.estado-proyecto-small span').each(function () {
        loc = $(this).text();
        if (loc.toLowerCase().indexOf('verde') > -1) {
            $(this).css('background', 'green');
        } else if (loc.toLowerCase().indexOf('agotado') > -1) {
            $(this).css('background', 'grey');
        } else if (loc.toLowerCase().indexOf('homy') > -1) {
            $(this).css('background', '#008FA9');
        }
    });

    $('.ribbon span').each(function () {
        loc = $(this).text();
        if (loc.toLowerCase().indexOf('verde') > -1) {
            $(this).css('background', 'green');
        } else if (loc.toLowerCase().indexOf('agotado') > -1) {
            $(this).css('background', 'grey');
        } else if (loc.toLowerCase().indexOf('homy') > -1) {
            $(this).css('background', '#008FA9');
        }
    });

    $('.av-promo--block .padre-che p').each(function () {
        loc = $(this).text();
        console.log(loc);
        if (loc.toLowerCase().indexOf('verde') > -1) {
            console.log()
            $(this).siblings('#chevron').css('background', 'green');
        } else if (loc.toLowerCase().indexOf('agotado') > -1) {
            $(this).css('background', 'grey');
        } else if (loc.toLowerCase().indexOf('homy') > -1) {
            $(this).css('background', '#008FA9');
        }
    });

    var lec = $('.estado-de-proyecto').text();
    console.log(!(window.location.href.indexOf("parque-mirador") > -1));

    if (!(window.location.href.indexOf("parque-mirador") > -1) && lec.toLowerCase().indexOf('verde') > -1) {
        $('.estado-de-proyecto').css('background', 'green');
    } else if (lec.toLowerCase().indexOf('agotado') > -1) {
        $('.estado-de-proyecto').css('background', 'grey');
    } else if (loc.toLowerCase().indexOf('homy') > -1) {
        $(this).css('background', '#008FA9');
    }

    //-------------------------------------------------------
    //------------------------ Ocultar Agotados del menú
    //--------------------------------------------------------------------------

    $('.estado-proyecto-small-menu span').each(function () {
        loc = $(this).text();
        if  (loc.toLowerCase().indexOf('agotado') > -1) {
            $(this).parent().parent().hide();
        }
    });

    // --------------------------------------------------------------------------------------------
    // ----------------------- Formulario campaña homy
    // --------------------------------------------------------------------------------------------
    
    $('.av-tipo--ci').on('click', function () {
        // $('input[value="Tipo e"]').removeAttr('checked');
        // $('input[value="Tipo ci"]').prop('checked', true);
        // $(this).css('border','3px solid red');
        // $('.av-tipo--e').css('border','none');
        // console.log('tipo ci');
        alert('Tipo C1 Agotado, por favor selecciona el tipo E');
    });
    $('input[value="Tipo ci"]').removeAttr('checked');
    $('input[value="Tipo e"]').prop('checked', true);
    $('.av-tipo--e').css('border','3px solid red');

    $('.av-tipo--e').on('click', function () {
        $('input[value="Tipo ci"]').removeAttr('checked');
        $('input[value="Tipo e"]').prop('checked', true);
        $(this).css('border','3px solid red');
        $('.av-tipo--ci').css('border','none');
        console.log('tipo ci');
    });

    $("#wpcf7-f2692-p2708-o1").on('wpcf7:submit', function(event) {
        var nombre = $('input[name="contacto-name"]').val();
        $('.nombre-persona').text(nombre);
        $('#av-homy').hide();
        $('#gracias-formulario-referidos').fadeIn();
    });

    $('input[name="Mailing[]"]').prop('checked', true);


    
    if (window.location.href.indexOf("casas-del-rahue") > -1) {
        setTimeout(function(){ 
            $('#equipamiento iframe').attr('src', 'https://www.avellaneda.cl/360/rahue/tour.html');
            console.log("estamos");
         }, 1000);
    }
    if (window.location.href.indexOf("condominio-laguna-norte") > -1) {
        //$('#equipamiento').hide();
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    var avProjectName = getParameterByName('proyecto');
    console.log(avProjectName);

    $('#postventa_paso_3 select[name="listaproyectos"]').find('option[value="' + avProjectName + '"]').prop('selected', true);
    email = $('#postventa_paso_3 select[name="listaproyectos"]').find(':selected').attr('data-email');
    bcc = $('#postventa_paso_3 select[name="listaproyectos"]').find(':selected').attr('data-cc');
    cc = $('#postventa_paso_3 select[name="listaproyectos"]').find(':selected').attr('data-bcc');
    nombreProyecto = $('#postventa_paso_3 select[name="listaproyectos"]').find(':selected').text();

    $('input[name="Emaildestino"]').val(email);
    $('input[name="Bccdestino"]').val(bcc);
    $('input[name="Ccdestino"]').val(cc);
    $('input[name="NombreProyecto"]').val(nombreProyecto);
    $('#postventa_paso_3 select[name="listaproyectos"]').hide();
});

/* var d = new Date();
const coock = document.cookie.indexOf("visited=") >= 0;

if( d.getDay() === 3 || d.getDay() === 4 || d.getDay() === 5 || d.getDay() === 6 || d.getDate() == 7 ){
    if (coock) {
        $(window).on('load',function(){
            $('#myModal').modal('hide');
        });
      }
      else {
        expiry = new Date();
        expiry.setTime(expiry.getTime()+( 60 * 60 * 1000 )); 
        document.cookie = "visited=yes; expires=" + expiry.toGMTString();
        $(window).on('load',function(){
            $('#myModal').modal('show');
        });
    }
} */