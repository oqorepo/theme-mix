// Filtros de Plantas
$(window).on("load", function () {

    //   var $grid = $('.plantas-grid').imagesLoaded( function() {
    // // init Isotope after all images have loaded

    var $grid = $('.plantas-grid');

    $grid.isotope({
        itemSelector: '.bloque-plantas',
        layoutMode: 'fitRows',
        sortBy: 'category',
        stagger: 30
    });
    var $grid_ii = $('.plantas-grid-ii');

    $grid_ii.isotope({
        itemSelector: '.bloque-plantas',
        layoutMode: 'fitRows',
        sortBy: 'category',
        stagger: 30
    });
    // });

    // filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function () {
            var number = $(this).find('.number').text();
            return parseInt(number, 10) > 50;
        },
        // show if name ends with -ium
        ium: function () {
            var name = $(this).find('.name').text();
            return name.match(/ium$/);
        }
    };
    // bind filter button click
    $('.filters-button-group.av-filter-i').on('click', 'button', function () {
        var filterValue = $(this).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;
        $grid.isotope({
            filter: filterValue
        });
    });
    $('.filters-button-group.av-filter-ii').on('click', 'button', function () {
        var filterValue = $(this).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;
        $grid_ii.isotope({
            filter: filterValue
        });
    });
    // change is-checked class on buttons
    $('.button-group').each(function (i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'button', function () {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $(this).addClass('is-checked');
        });
    });
});