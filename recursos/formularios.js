$(window).on("load", function () {
    $('.btn-contacto').prop('disabled', true);
    $('.btn-solicitar-informacion').prop('disabled', true);
    $('.btn-solicitar-planta').prop('disabled', true);
    $('.btn-enviar-postventa').prop('disabled', true);
    $('.btn-enviar-cotizar').prop('disabled', true);
    $('.btn-descargar-pdf').prop('disabled', true);
    $('.btn-landing').prop('disabled', true);
    $('.btn-referidos').prop('disabled', true);

});



$(function () {

    // Activa check de mailing
    $('input[name="Mailing[]"]').iCheck('check');

    // Arregla el campo de RUT
    $('.Rut').Rut({
        on_error: function () {},
        format_on: 'keyup'
    });



    // --------------------------------------------------------------------------
    // -------------------- Formulario Referidos ----------------------------
    // --------------------------------------------------------------------------
    // Validación
    $('form#formulario-referidos').validate({
        rules: {

            Nombre: {
                required: true,
                lettersonly: true,
            },
            Apellido: {
                required: true,
                lettersonly: true,
            },

            Email: {
                required: true,
                email: true,
            },
            Telefono: {
                required: true,
                digits: true,
                minlength: 9,
                maxlength: 9,
            },
            Rut: {
                required: true,
            },
            NombreAmigo: {
                required: true,
                lettersonly: true,
            },
            ApellidoAmigo: {
                required: true,
                lettersonly: true,
            },

            EmailAmigo: {
                required: true,
                email: true,
            },
            TelefonoAmigo: {
                required: false,
                digits: true,
                minlength: 9,
                maxlength: 9,
            },
            RutAmigo: {
                required: false,
            },
            ProyectosReferidos: {
                required: true,
            }





        },
        submitHandler: function (form) {},
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error)
        },
    });
    // Validación cuando se escribe
    $('form#formulario-referidos input').on('keyup blur', function () {
        if ($('form#formulario-referidos').valid()) {
            $('.btn-referidos').prop('disabled', false);
        } else {
            $('.btn-referidos').prop('disabled', 'disabled');
        }
    });
    // Botón enviar
    $('.btn-referidos').one('click', function (event) {
        event.preventDefault();
        $(this).addClass('disabled');
        $(this).prop('disabled', 'disabled');
        $(this).val('ENVIANDO');
        $("form#formulario-referidos").submit();
        return false;
    });
    // Proceso enviador
    $(document).ready(function () {
        $("#wpcf7-f1397-o1").on('wpcf7:submit', function (event) {
            var nombre = $('input[name="Nombre"]').val();
            $('.nombre-persona').text(nombre);
            $('#formulario-referidos').hide();
            $('#gracias-formulario-referidos').fadeIn();


        });

        $("#wpcf7-f1397-o1").on('wpcf7:mailfailed', function (event) {
            $('.loading-formularios').fadeOut();
            $('.btn-referidos').val('ENVIAR');
            $('.btn-referidos').removeClass('disabled');
            $('.btn-referidos').prop('disabled', false);
        });


    });




    // --------------------------------------------------------------------------
    // -------------------- Formulario Landing ----------------------------
    // --------------------------------------------------------------------------
    // Validación
    $('form#formulario-proyectos-landing').validate({
        rules: {
            Nombre: {
                required: true,
                lettersonly: true,
            },
            Apellido: {
                required: true,
                lettersonly: true,
            },
            Telefono: {
                required: true,
                digits: true,
                minlength: 9,
                maxlength: 9,
            },
            Email: {
                required: true,
                email: true,
            },
            Rut: {
                required: false,
            },
            listaProyectosLanding: {
                required: true,
            }
        },
        submitHandler: function (form) {},
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error)
        },
    });
    // Validación cuando se escribe
    $('form#formulario-proyectos-landing input').on('keyup blur', function () {
        if ($('form#formulario-proyectos-landing').valid()) {
            $('.btn-landing').prop('disabled', false);
        } else {
            $('.btn-landing').prop('disabled', 'disabled');
        }
    });
    // Botón enviar
    $('.btn-landing').one('click', function (event) {
        event.preventDefault();
        $(this).addClass('disabled');
        $(this).prop('disabled', 'disabled');
        $(this).val('ENVIANDO');
        $("form#formulario-proyectos-landing").submit();
        return false;
    });
    // Proceso enviador
    $(document).ready(function () {
        $("#wpcf7-f1252-o1").on('wpcf7:submit', function (event) {
            var nombre = $('input[name="Nombre"]').val();
            $('.nombre-persona').text(nombre);
            $('#formulario-landing').hide();
            $('#gracias-formulario-proyectos-landing').fadeIn();


        });

        $("#wpcf7-f1252-o1").on('wpcf7:mailfailed', function (event) {
            $('.loading-formularios').fadeOut();
            $('.btn-landing').val('ENVIAR');
            $('.btn-landing').removeClass('disabled');
            $('.btn-landing').prop('disabled', false);
        });


    });



    // --------------------------------------------------------------------------
    // -------------------- Formulario Descargar PDF ----------------------------
    // --------------------------------------------------------------------------
    // Validación
    $('form#formulario-descargar-pdf').validate({
        rules: {
            Nombre: {
                required: true,
                lettersonly: true,
            },
            Email: {
                required: true,
                email: true,
            },
        },
        submitHandler: function (form) {},
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error)
        },
    });
    // Validación cuando se escribe
    $('form#formulario-descargar-pdf input').on('keyup blur', function () {
        if ($('form#formulario-descargar-pdf').valid()) {
            $('.btn-descargar-pdf').prop('disabled', false);
        } else {
            $('.btn-descargar-pdf').prop('disabled', 'disabled');
        }
    });
    // Botón enviar
    $('.btn-descargar-pdf').one('click', function (event) {
        event.preventDefault();
        $('.loading-formularios-modal').fadeIn();
        $(this).addClass('disabled');
        $(this).prop('disabled', 'disabled');
        $(this).val('ENVIANDO');
        $("form#formulario-descargar-pdf").submit();
        return false;
    });
    // Proceso enviador
    $(document).ready(function () {
        $("#wpcf7-f974-o1").on('wpcf7:submit', function (event) {
            $('#formulario-puede-descargar').fadeIn();
            $('#formulario-descargar').hide();
            var nombre_proyecto = Cookies.get('Nombre_Proyecto');
            var pregunta_form_pdf = "Enviado - " + nombre_proyecto;
            Cookies.set('form_pdf_para_' + nombre_proyecto + '', pregunta_form_pdf);
        });
    });


    // --------------------------------------------------------------------------
    // -------------------- Formulario Solicitar Info Planta --------------------
    // --------------------------------------------------------------------------



    // Validación

    $('form#formulario-planta').validate({
        rules: {
            Nombre: {
                required: true,
                lettersonly: true,
            },
            Apellido: {
                required: true,
                lettersonly: true,
            },
            Telefono: {
                required: true,
                digits: true,
                minlength: 9,
                maxlength: 9,
            },
            Email: {
                required: true,
                email: true,
            },
            Rut: {
                required: false,
            }
        },
        submitHandler: function (form) {},
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error)
        },
    });

    // Validación cuando se escribe
    $('form#formulario-planta input').on('keyup blur', function () {
        if ($('form#formulario-planta').valid()) {
            $('.btn-solicitar-planta').prop('disabled', false);
        } else {
            $('.btn-solicitar-planta').prop('disabled', 'disabled');
        }
    });

    // Botón enviar
    $('.btn-solicitar-planta').on('click', function (event) {
        event.preventDefault();
        $('.loading-formularios').fadeIn();
        $(this).addClass('disabled');
        $(this).prop('disabled', 'disabled');
        $(this).val('ENVIANDO');
        var nombre = $('input[name="Nombre"]').val();
        $('.nombre-persona').text(nombre);
        $("form#formulario-planta").submit();
        return false;
    });

    // Proceso enviador
    $(document).ready(function () {
        $("#wpcf7-f146-o1").on('wpcf7:mailsent', function (event) {
            $('#formulario-solicitar-planta').hide();
            $('#gracias-form-solicitar-planta').fadeIn();
        });

        $("#wpcf7-f146-o1").on('wpcf7:mailfailed', function (event) {
            $('.loading-formularios').fadeOut();
            $('.btn-solicitar-planta').val('ENVIAR');
            $('.btn-solicitar-planta').removeClass('disabled');
            $('.btn-solicitar-planta').prop('disabled', false);
        });



    });


    // --------------------------------------------------------------------------
    // -------------------- Formulario Solicitar Información --------------------
    // --------------------------------------------------------------------------

    $('form#formulario-solicitar').validate({
        rules: {
            Nombre: {
                required: true,
                lettersonly: true,
            },
            Apellido: {
                required: true,
                lettersonly: true,
            },
            Telefono: {
                required: true,
                digits: true,
                minlength: 9,
                maxlength: 9,
            },
            Email: {
                required: true,
                email: true,
            },
            Rut: {
                required: false,
            }
        },
        submitHandler: function (form) {},
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error)
        },
    });

    // Validación cuando se escribe
    $('form#formulario-solicitar input').on('keyup blur', function () {
        if ($('form#formulario-solicitar').valid()) {
            $('.btn-solicitar-informacion').prop('disabled', false);
        } else {
            $('.btn-solicitar-informacion').prop('disabled', 'disabled');
        }
    });

    // Botón enviar
    $('.btn-solicitar-informacion').on('click', function (event) {
        event.preventDefault();
        $('.loading-formularios').fadeIn();
        $(this).addClass('disabled');
        $(this).prop('disabled', 'disabled');
        $(this).val('ENVIANDO');
        var nombre_enviador = $('input[name="Nombre"]').val();
        $('.nombre-persona').html(nombre_enviador);
        $("form#formulario-solicitar").submit();
    });

    // Proceso enviador
    $(document).ready(function () {
        $("#wpcf7-f101-o2").on('wpcf7:submit', function (event) {
            $('#formulario-solicitar-informacion').hide();
            $('#gracias-form-solicitar-informacion').fadeIn();
        });

        $("#wpcf7-f101-o2").on('wpcf7:mailfailed', function (event) {
            $('.loading-formularios').fadeOut();
            $('.btn-solicitar-informacion').val('ENVIAR');
            $('.btn-solicitar-informacion').removeClass('disabled');
            $('.btn-solicitar-informacion').prop('disabled', false);
        });


    });

    // -------------------------------------------------------------
    // -------------------- Formulario Contacto --------------------
    // -------------------------------------------------------------
    $('form#formulario-contacto').validate({
        rules: {
            Nombre: {
                required: true,
                lettersonly: true,
            },
            Apellido: {
                required: true,
                lettersonly: true,
            },
            Telefono: {
                required: true,
                digits: true,
                minlength: 9,
                maxlength: 9,
            },
            Email: {
                required: true,
                email: true,
            },
            Rut: {
                // required: false,
            }
        },
        submitHandler: function (form) {},
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error)
        },
    });

    // $('form#formulario-contacto input').on('keyup', function() {
    //     $(this).each(function() {
    //         $(this).on('keyup blur', function() {
    //             if ($(this).attr('aria-invalid') == "true") {
    //                 console.log('es falsa');
    //             }
    //             else if($(this).attr('aria-invalid') == "false") {
    //                 console.log('es bueno');
    //             }

    //         });
    //     });
    // });


    //Validación cuando se escribe
    $('form#formulario-contacto input').on('keyup blur', function () {
        if ($('form#formulario-contacto').valid()) {
            $('.btn-contacto').prop('disabled', false);
        } else {
            $('.btn-contacto').prop('disabled', 'disabled');
        }
    });


    // Botón enviar
    $('.btn-contacto').on('click', function (event) {
        event.preventDefault();
        $('.loading-formularios').fadeIn();
        $(this).addClass('disabled');
        $(this).prop('disabled', 'disabled');
        $(this).val('ENVIANDO');
        var nombre = $('input[name="Nombre"]').val();
        $('.nombre-persona').text(nombre);
        $("form#formulario-contacto").submit();
        return false;
    });

    // Proceso enviador
    $(document).ready(function () {
        $("#wpcf7-f626-o1").on('wpcf7:mailsent', function (event) {
            $('#formulario-contacto').hide();
            $('#gracias-form-contacto').fadeIn();

        });

        $("#wpcf7-f626-o1").on('wpcf7:mailfailed', function (event) {
            $('.loading-formularios').fadeOut();
            $('.btn-contacto').val('ENVIAR');
            $('.btn-contacto').removeClass('disabled');
            $('.btn-contacto').prop('disabled', false);
        });

    });








    // ----------------------------------------------------------------------
    // -------------------- Formulario Cotizar Proyectos --------------------
    // ----------------------------------------------------------------------



    // Contacto
    $('form#formulario-cotizar').validate({
        rules: {
            Nombre: {
                required: true,
                lettersonly: true,
            },
            Apellido: {
                required: true,
                lettersonly: true,
            },
            Telefono: {
                required: true,
                digits: true,
                minlength: 9,
                maxlength: 9,
            },
            Email: {
                required: true,
                email: true,
            },
            Rut: {
                required: true,
            }
        },
        submitHandler: function (form) {},
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error)
        },
    });

    // Validación cuando se escribe
    $('form#formulario-cotizar input').on('keyup blur', function () {
        if ($('form#formulario-cotizar').valid()) {
            $('.btn-enviar-cotizar').prop('disabled', false);
        } else {
            $('.btn-enviar-cotizar').prop('disabled', 'disabled');
        }
    });

    // Botón enviar
    $('.btn-enviar-cotizar').on('click', function (event) {
        event.preventDefault();
        $('.loading-formularios').fadeIn();
        $(this).addClass('disabled');
        $(this).prop('disabled', 'disabled');
        $(this).val('ENVIANDO');
        var nombre = $('input[name="Nombre"]').val();
        $('.nombre-persona').text(nombre);
        $("form#formulario-cotizar").submit();
        return false;
    });

    // Proceso enviador
    $(document).ready(function () {
        $("#wpcf7-f505-o1").on('wpcf7:mailsent', function (event) {
            $('#formulario-cotizar').hide();
            $('#gracias-form-cotizar').fadeIn();
        });

        $("#wpcf7-f505-o1").on('wpcf7:mailfailed', function (event) {
            $('.loading-formularios').fadeOut();
            $('.btn-enviar-cotizar').val('ENVIAR');
            $('.btn-enviar-cotizar').removeClass('disabled');
            $('.btn-enviar-cotizar').prop('disabled', false);
        });





    });



    // ---------------------------------------------------------------
    // -------------------- Formulario Post Venta --------------------
    // ---------------------------------------------------------------


    $('form#formulario-postventa').validate({
        rules: {

            // Paso 1
            nombre: {
                required: true,
                lettersonly: true
            },
            apellidop: {
                required: true,
                lettersonly: true
            },
            apellidom: {
                required: true,
                lettersonly: true
            },
            rut: {
                required: true
            },
            // Paso 2
            nombre_sol: {
                required: true,
                lettersonly: true
            },
            apellidop_sol: {
                required: true,
                lettersonly: true
            },
            apellidom_sol: {
                required: true,
                lettersonly: true
            },
            rut_sol: {
                required: true
            },
            codigo_movil: {
                required: true,
                digits: true,
                minlength: 9,
                maxlength: 9,
            },
            codigo_fono: {
                required: false,
                digits: true,
                minlength: 9,
                maxlength: 9,
            },
            email: {
                required: true,
                email: true
            },
            // Paso 3
            direccion: {
                required: true
            },
            numero: {
                required: true,
                digits: true
            },
            depto: {
                required: false,
                digits: true
            },
            ciudad: {
                required: true
            },
            comuna: {
                required: true
            },
            listaproyectos: {
                required: true
            },

        },



        submitHandler: function (form) {},
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error)
        },



    });


    // Validación cuando se escribe
    $('form#formulario-postventa input, form#formulario-postventa select').on('keyup blur change', function () {
        if ($('form#formulario-postventa').valid()) {
            $('.btn-enviar-postventa').prop('disabled', false);
        } else {
            $('.btn-enviar-postventa').prop('disabled', 'disabled');
        }
    });






    // Botón enviar
    $('.btn-enviar-postventa').on('click', function (event) {
        event.preventDefault();
        $('.loading-formularios').fadeIn();
        $(this).addClass('disabled');
        $(this).prop('disabled', 'disabled');
        $(this).val('ENVIANDO');
        var nombre = $('input[name="nombre"]').val();
        $('.nombre-persona').text(nombre);



        // Proceso enviador
        $(document).ready(function () {
            $("#wpcf7-f506-p507-o1").on('wpcf7:mailsent', function (event) {
                $('#formulario-postventa').hide();
                $('.bs-wizard').hide();
                $('#gracias-form-postventa').fadeIn();
            });

            $("#wpcf7-f506-p507-o1").on('wpcf7:mailfailed', function (event) {
                $('.loading-formularios').fadeOut();
                $('.btn-enviar-postventa').val('ENVIAR');
                $('..btn-enviar-postventa').removeClass('disabled');
                $('.btn-enviar-postventa').prop('disabled', false);
            });


        });







        // Paso 1
        $nombre = $('input[name="nombre"]').val();
        $apellidop = $('input[name="apellidop"]').val();
        $apellidom = $('input[name="nombre"]').val();
        $rut = $('input[name="apellidop"]').val();
        // Paso 2
        $nombre_sol = $('input[name="nombre_sol"]').val();
        $apellidop_sol = $('input[name="apellidop_sol"]').val();
        $apellidom_sol = $('input[name="apellidom_sol"]').val();
        $rut_sol = $('input[name="rut_sol"]').val();
        $codigo_movil = $('input[name="codigo_movil"]').val();
        $codigo_fono = $('input[name="codigo_fono"]').val();
        $email = $('input[name="email"]').val();
        // Paso 3
        $direccion = $('input[name="direccion"]').val();
        $numero = $('input[name="numero"]').val();
        $depto = $('input[name="depto"]').val();
        $ciudad = $('select[name="ciudad"] option:selected').val();
        $comuna = $('select[name="comuna"] option:selected').val();
        $requerimiento = $('textarea[name="requerimiento"]').val();




        $listaproyectos = $('select[name="listaproyectos"] option:selected').val();
        $.ajax({
            url: ajax_object.ajax_url,
            method: 'POST',

            data: {
                nombre: $nombre,
                apellidop: $apellidop,
                proyecto: $listaproyectos,
                apellidom: $apellidom,
                rut: $rut,

                nombre_sol: $nombre_sol,
                apellidop_sol: $apellidop_sol,
                apellidom_sol: $apellidom_sol,
                rut_sol: $rut_sol,
                codigo_movil: $codigo_movil,
                codigo_fono: $codigo_fono,
                email: $email,

                direccion: $direccion,
                numero: $numero,
                depto: $depto,
                ciudad: $ciudad,
                comuna: $comuna,
                requerimiento: $requerimiento,
                action: 'insert_datos'
            },
            success: function (response) {
                console.log(response);
                $('input[name="IDSolicitud"]').val(response);
                $('.numero-solicitud').text(response);

                if ($('input[name="IDSolicitud"]').val().length) {

                    $("form#formulario-postventa").submit();
                }




            },
            error: function (e){
                console.log(e);
            }


        });

















        // 
        return false;
    });



    // Paso 1
    $('#postventa_paso_1 input').on('keyup blur', function () { // fires on every keyup & blur
        if ($('form#formulario-postventa').valid()) {
            $('.btn-siguiente-postventa-paso2').removeClass('disabled');
            $('.btn-siguiente-postventa-paso2').removeClass('btn-gris-claro');
            $('.btn-siguiente-postventa-paso2').addClass('btn-rojo');
            $('.btn-siguiente-postventa-paso2').on('click', function () {
                paso1.removeClass('active');
                paso2.addClass('active');
                $('.paso-1').removeClass('active');
                $('.paso-2').addClass('complete');
                $('.paso-2').removeClass('disabled');
            });

        } else {
            $('.btn-siguiente-postventa-paso2').addClass('disabled');
        }
    });

    // Paso 2

    $('#postventa_paso_2 input').on('keyup blur', function () { // fires on every keyup & blur
        if ($('form#formulario-postventa').valid()) {
            $('.btn-siguiente-postventa-paso3').removeClass('disabled');
            $('.btn-siguiente-postventa-paso3').removeClass('btn-gris-claro');
            $('.btn-siguiente-postventa-paso3').addClass('btn-rojo');

            $('.btn-siguiente-postventa-paso3').on('click', function () {
                $('.paso-3').addClass('active');
                $('.paso-3').removeClass('disabled');
                paso2.removeClass('active');
                paso3.addClass('active');
                $('.btn-solicitar-postventa').prop('disabled', true);
            });
        } else {
            $('.btn-siguiente-postventa-paso3').addClass('disabled');
        }
    });

    // Paso 3

    $('#postventa_paso_3 input').on('keyup blur', function () { // fires on every keyup & blur
        if ($('form#formulario-postventa').valid()) {
            $('.btn-solicitar-postventa').prop('disabled', false);



        } else {
            $('.btn-solicitar-postventa').prop('disabled', true);
        }
    });








});


// // Enviar
// $('.btn-solicitar-planta').one('click', function(event){
// event.preventDefault();
// $(this).prop('disabled', true);
// $(this).val('ENVIANDO');
// $('.loading-formularios').fadeIn();

// $('.btn-enviar-solicitar-planta').trigger('click');
// var nombre = $('input[name="Nombre"]').val();
// $('.nombre-persona').text(nombre);
// console.log('enviando formulario');

// });






// // Enviar Cotizar
// $('.btn-cotizar').one('click', function(event){
// event.preventDefault();
// $(this).prop('disabled', true);
// $(this).val('ENVIANDO');
// $('.loading-formularios').fadeIn();

// $('.btn-enviar-cotizar').trigger('click');
// var nombre = $('input[name="Nombre"]').val();
// $('.nombre-persona').text(nombre);
// console.log('enviando formulario');
// });



// // Enviar
// $('.btn-solicitar-postventa').one('click', function(event){
// event.preventDefault();
// $(this).prop('disabled', true);
// $(this).val('ENVIANDO');
// $('.loading-formularios').fadeIn();

// $('.btn-enviar-postventa').trigger('click');
// var nombre = $('input[name="NombreSolicitante"]').val();
// $('.nombre-persona').text(nombre);
// console.log('enviando formulario');
// });












$(document).ready(function () {
    // Formulario paso a paso
    paso1 = $('#postventa_paso_1');
    paso2 = $('#postventa_paso_2');
    paso3 = $('#postventa_paso_3');






    $('.btn-anterior-postventa-paso1').on('click', function () {
        paso2.removeClass('active');
        paso1.addClass('active');
        $('.paso-2').removeClass('complete');
        $('.paso-2').addClass('disabled');
    });

    $('.btn-anterior-postventa-paso2').on('click', function () {
        paso3.removeClass('active');
        paso2.addClass('active');
        $('.paso-3').removeClass('complete');
        $('.paso-3').addClass('disabled');
        $('.paso-3').removeClass('active');
    });



    $('.btn-siguiente-postventa-paso2').addClass('disabled');



});





$(document).ready(function () {






    // Validador de RUT
    $.validator.addMethod("Rut", function (value, element) {
        return this.optional(element) || $.Rut.validar(value);
    }, "Este campo debe ser un rut valido.");




    // Validación de sólo letras y espacio
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Por favor ingresa sólo letras.");



    // Mensajes Personalizados

    jQuery.extend(jQuery.validator.messages, {
        required: "Este campo es obligatorio",
        remote: "Please fix this field.",
        email: "Por favor ingresa un correo válido.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        lettersonly: "Por favor ingresa sólo letras.",
        digits: "Por favor ingresa sólo números.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Por favor ingresa un teléfono de 9 digitos."),
        minlength: jQuery.validator.format("Por favor ingresa un teléfono de 9 digitos."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Por favor ingresa un teléfono de 9 digitos."),
        min: jQuery.validator.format("Por favor ingresa un teléfono de 9 digitos.")
    });
});







// Desactiva múltiples envíos

jQuery(document).on('click', '.wpcf7-submit', function (e) {
    if (jQuery('.ajax-loader').hasClass('is-active')) {
        e.preventDefault();
        return false;
    }
});









// Select de Proyectos en Form Post Venta
$(document).ready(function () {
    var e = $("select[name='proyecto_db']> option").clone();
    $('select[name="listaproyectos"]').append(e);
    // $('select[name="listaproyectos"]').find("option").eq(0).remove();
});



// Select de Proyectos en Landing
$(document).ready(function () {
    var e = $("select[name='Proyectos']> option").clone();
    $('select[name="listaProyectosLanding"]').append(e);
    $('select[name="listaProyectosLanding"]').find("option").eq(0).remove();


});



// Select
$(document).ready(function () {
    $('select[name="listaproyectos"]').on('change', function () {



        email = $(this).find(':selected').attr('data-email');
        bcc = $(this).find(':selected').attr('data-cc');
        cc = $(this).find(':selected').attr('data-bcc');
        nombreProyecto = $(this).find(':selected').text();

        $('input[name="Emaildestino"]').val(email);
        $('input[name="Bccdestino"]').val(bcc);
        $('input[name="Ccdestino"]').val(cc);
        $('input[name="NombreProyecto"]').val(nombreProyecto);

        console.log(nombreProyecto);




    });

});



//  Regiones y Comunas
var RegionesYcomunas = {

    "regiones": [{
            "NombreRegion": "Arica y Parinacota",
            "comunas": ["Arica", "Camarones", "Putre", "General Lagos"]
        },
        {
            "NombreRegion": "Tarapacá",
            "comunas": ["Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica"]
        },
        {
            "NombreRegion": "Antofagasta",
            "comunas": ["Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena"]
        },
        {
            "NombreRegion": "Atacama",
            "comunas": ["Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco"]
        },
        {
            "NombreRegion": "Coquimbo",
            "comunas": ["La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paiguano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado"]
        },
        {
            "NombreRegion": "Valparaíso",
            "comunas": ["Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana"]
        },
        {
            "NombreRegion": "Región del Libertador Gral. Bernardo O’Higgins",
            "comunas": ["Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz"]
        },
        {
            "NombreRegion": "Región del Maule",
            "comunas": ["Talca", "ConsVtución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "ReVro", "San Javier", "Villa Alegre", "Yerbas Buenas"]
        },
        {
            "NombreRegion": "Región del Biobío",
            "comunas": ["Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío", "Chillán", "Bulnes", "Cobquecura", "Coelemu", "Coihueco", "Chillán Viejo", "El Carmen", "Ninhue", "Ñiquén", "Pemuco", "Pinto", "Portezuelo", "Quillón", "Quirihue", "Ránquil", "San Carlos", "San Fabián", "San Ignacio", "San Nicolás", "Treguaco", "Yungay"]
        },
        {
            "NombreRegion": "Región de la Araucanía",
            "comunas": ["Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria", ]
        },
        {
            "NombreRegion": "Región de Los Ríos",
            "comunas": ["Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno"]
        },
        {
            "NombreRegion": "Región de Los Lagos",
            "comunas": ["Puerto Montt", "Calbuco", "Cochamó", "Fresia", "FruVllar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena"]
        },
        {
            "NombreRegion": "Región Aisén del Gral. Carlos Ibáñez del Campo",
            "comunas": ["Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez"]
        },
        {
            "NombreRegion": "Región de Magallanes y de la AntárVca Chilena",
            "comunas": ["Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "AntárVca", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine"]
        },
        {
            "NombreRegion": "Región Metropolitana de Santiago",
            "comunas": ["Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "TilVl", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor"]
        }
    ]
}


jQuery(document).ready(function () {

    var iRegion = 0;
    var htmlRegion = '<option value="" disabled="disabled" selected>Seleccione región</option>';
    var htmlComunas = '<option value="" disabled="disabled" selected>Seleccione comuna</option>';

    jQuery.each(RegionesYcomunas.regiones, function () {
        htmlRegion = htmlRegion + '<option value="' + RegionesYcomunas.regiones[iRegion].NombreRegion + '">' + RegionesYcomunas.regiones[iRegion].NombreRegion + '</option>';
        iRegion++;
    });

    jQuery('#regiones').html(htmlRegion);
    jQuery('#comunas').html(htmlComunas);

    jQuery('#regiones').change(function () {
        var iRegiones = 0;
        var valorRegion = jQuery(this).val();
        var htmlComuna = '<option value="">Seleccione comuna</option>';
        jQuery.each(RegionesYcomunas.regiones, function () {
            if (RegionesYcomunas.regiones[iRegiones].NombreRegion == valorRegion) {
                var iComunas = 0;
                jQuery.each(RegionesYcomunas.regiones[iRegiones].comunas, function () {
                    htmlComuna = htmlComuna + '<option value="' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '">' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '</option>';
                    iComunas++;
                });
            }
            iRegiones++;
        });
        jQuery('#comunas').html(htmlComuna);
    });
    jQuery('#comunas').change(function () {
        if (jQuery(this).val() == 'sin-region') {
            alert('selecciones Región');
        } else if (jQuery(this).val() == 'sin-comuna') {
            alert('selecciones Comuna');
        }
    });
    jQuery('#regiones').change(function () {
        if (jQuery(this).val() == 'sin-region') {
            alert('selecciones Región');
        }
    });

});

$('.wpcf7-form').on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        return false;
    }
});


// iCheck

$(document).ready(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_flat-red',
        radioClass: 'iradio_flat-red',
        increaseArea: '30%' // optional,

    });
});