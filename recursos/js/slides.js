$(window).on('load', function() {
    // Slides - Es importante llamar los slides en la instancia Window on load
    var owl_bigslide = $('.owl-bigslide');
    owl_bigslide.owlCarousel({
        items: 1,
        lazyLoad: false,
        loop: true,
        autoHeight: true,
        dots: false,
        autoplay: true,
        autoplayHoverPause: true,
        video: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeInRight',
        nav: false,
        autoplayTimeout:8000,
        responsiveClass: true,
        responsive: {
            991: {
                dots: false,
                autoplay: true,
                nav: false,
            }
        },
        onTranslate: function() {
            $('.item-video').find('owl-video').each(function() {
                this.pause();
            });
        },
    });
    if (owl_bigslide.hasClass('owl-loaded')) {
        $('.big-slide-contenedor').fadeIn();
        $('.loading_slides_inicio').fadeOut();
    }
    $(".big-slide-siguiente").click(function(e) {
        e.preventDefault();
        owl_bigslide.trigger('next.owl.carousel');
    });
    $(".big-slide-anterior").click(function(e) {
        e.preventDefault();
        owl_bigslide.trigger('prev.owl.carousel');
    });
    // -------------------- BIG PROYECTOS DESTACADOS
    var owl_proyectos = $('.owl-proyectos');
    owl_proyectos.owlCarousel({
        items: 1,
        lazyLoad: false,
        loop: true,
        autoHeight: true,
        dots: true,
        autoplay: false,
        autoplayHoverPause: false,
        video: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeInRight',
        nav: false,
        responsiveClass: true,
        responsive: {
            991: {
                dots: true,
                autoplay: true,
                nav: false,
            }
        },
        onTranslate: function() {
            $('.item-video').find('owl-video').each(function() {
                this.pause();
            });
        },
    });
    if (owl_proyectos.hasClass('owl-loaded')) {
        $('.proyectos-slide-contenedor').fadeIn();
        $('.loading_slides_proyectos').fadeOut();
    }
    $(".proyectos-siguiente").click(function(e) {
        e.preventDefault();
        owl_proyectos.trigger('next.owl.carousel');
    });
    $(".proyectos-anterior").click(function(e) {
        e.preventDefault();
        owl_proyectos.trigger('prev.owl.carousel');
    });
    // -------------------- LANDING SLIDE
    var owl_landing_proyecto = $('.owl_landing_proyecto');
    owl_landing_proyecto.owlCarousel({
        items: 1,
        autoHeight: true,
        loop: true,
        autoplay: true,
        dots: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeInRight',
        nav: false,
        responsiveClass: true,
        responsive: {
            991: {
                dots: false,
                autoplay: true,
                nav: false,
            }
        },
        onTranslate: function() {
            $('.item-video').find('owl-video').each(function() {
                this.pause();
            });
        },
    });
    if (owl_landing_proyecto.hasClass('owl-loaded')) {
        $('.proyectos-slide-miniaturas_single_contenedor').fadeIn();
        $('.loading_slides_landing').fadeOut();
    }
    $(".slide-landing-siguiente").click(function(e) {
        e.preventDefault();
        owl_landing_proyecto.trigger('next.owl.carousel');
    });
    $(".slide-landing-anterior").click(function(e) {
        e.preventDefault();
        owl_landing_proyecto.trigger('prev.owl.carousel');
    });
    // -------------------- FOTOS CARACTERÍSTICAS
    var owl_proyectos_carac = $('.owl-proyectos-caracteristicas');
    owl_proyectos_carac.owlCarousel({
        items: 1,
        lazyLoad: false,
        loop: false,
        autoHeight: true,
        dots: true,
        autoplay: false,
        autoplayHoverPause: false,
        video: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeInRight',
        nav: false,
        center: true,
        responsiveClass: true,
        responsive: {
            991: {
                dots: true,
                autoplay: true,
                nav: false,
            }
        },
        onTranslate: function() {
            $('.item-video').find('owl-video').each(function() {
                this.pause();
            });
        },
    });
    if (owl_proyectos_carac.hasClass('owl-loaded')) {
        $('.proyectos-slide-contenedor').fadeIn();
        $('.loading_slides_caracteristicas').fadeOut();
    }
    $(".proyectos-caracteristicas-siguiente").click(function(e) {
        e.preventDefault();
        owl_proyectos_carac.trigger('next.owl.carousel');
    });
    $(".proyectos-caracteristicas-anterior").click(function(e) {
        e.preventDefault();
        owl_proyectos_carac.trigger('prev.owl.carousel');
    });
    // -------------------- FOTOS MINIATURAS TABS
    var owl_miniaturas = $('.owl-miniaturas');
    owl_miniaturas.owlCarousel({
        touchDrag: true,
        items: 1,
        loop: false,
        autoHeight: true,
        dots: true,
        autoplay: false,
        autoplayHoverPause: false,
        video: true,
        margin: 10,
        animateOut: 'fadeOut',
        animateIn: 'fadeInRight',
        nav: false,
        responsiveClass: true,
        stagePadding: 30,
        responsive: {
            991: {
                dots: true,
                autoplay: true,
                nav: false,
            }
        },
        onTranslate: function() {
            $('.item-video').find('owl-video').each(function() {
                this.pause();
            });
        },
    });
    if (owl_miniaturas.hasClass('owl-loaded')) {
        $('.proyectos-slide-miniaturas_contenedor').fadeIn();
        $('.loading_slides_miniaturas').fadeOut();
    }
    // -------------------- FOTOS MINIATURAS TABS SINGLE
    var owl_miniaturas_single = $('.owl-miniaturas-single');
    owl_miniaturas_single.owlCarousel({
        items: 1,
        lazyLoad: false,
        loop: false,
        autoHeight: true,
        dots: true,
        autoplay: false,
        autoplayHoverPause: false,
        video: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeInRight',
        nav: false,
        center: true,
        margin: 0,
        responsiveClass: true,
        responsive: {
            991: {
                dots: true,
                autoplay: true,
                nav: false,
            }
        },
        onTranslate: function() {
            $('.item-video').find('owl-video').each(function() {
                this.pause();
            });
        },
    });
    if (owl_miniaturas_single.hasClass('owl-loaded')) {
        $('.proyectos-slide-miniaturas_single_contenedor').fadeIn();
        $('.loading_slides_miniaturas_single').fadeOut();
    }
    var owl_logos = $('.owl-logos');
    owl_logos.owlCarousel({
        items: 5,
        loop: true,
        autoHeight: true,
        dots: true,
        autoplay: false,
        autoplayHoverPause: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeInRight',
        nav: false,
        responsive: {
            0: {
                dots: true,
                autoplay: false,
                nav: false,
                items: 2,
            },
            991: {
                items: 5,
            }
        },
    });


    var owlvermasplantas = $('.owl-ver-mas-plantas');
    owlvermasplantas.owlCarousel({
        items: 4,
        margin:15,
        loop: false,
        autoHeight: true,
        dots: true,
        autoplay: false,
        autoplayHoverPause: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeInRight',
        nav: false,
        responsive: {
            0: {
                dots: true,
                autoplay: false,
                nav: false,
                items: 1,
            },
            991: {
                items: 4,
            }
        },
    });


    var owl_ganadores = $('.owl-ganadores');
    owl_ganadores.owlCarousel({
        items:3,
        loop: false,
        autoHeight: true,
        dots: true,
        autoplay: false,
        autoplayHoverPause: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeInRight',
        nav: false,
        responsive: {
            0: {
                dots: true,
                autoplay: false,
                nav: false,
                items: 2,
            },
            991: {
                items: 3,
            }
        },
    });

    
 if (owlvermasplantas.hasClass('owl-loaded')) {
    $('.bloque-mas-plantas').matchHeight();
    }

});