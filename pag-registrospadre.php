<?php
/**
* Template Name: Registros Padre
*/
get_header();
get_template_part('secciones/header-registros');
?>

<?php
if (! is_user_logged_in() ) :?>

<?php auth_redirect();?>

<?php else:?>

<style>
.small {
	font-size:11px;
}
.btn-danger {
	background:#cccc;
}

.col-md-3 {
	margin-bottom:30px;
}
</style>
<section class="pagina-interior">
	<div class="container">



<!-- Formulario -->
<div class="col-md-3">
	<h4 style="margin-bottom:15px;"><strong>Form. Contacto</strong></h4>
	<div style="width: 100%; background:#e5302d; padding:50px; text-align: center; color:white;">
	<h1 style="font-size:70px;"><?php echo do_shortcode('[cfdb-count form="Contacto"]');?></h1>
	</div>
	<hr></hr>
	<div>Último registro:</div> 
	<div><?php echo do_shortcode('[cfdb-value form="Contacto" show="Email" limit="1"]');?></div>
	<div style="width:100%; display:block; float:left; height:15px;"></div>
	<a href="http://www.avellaneda.cl/web/wp-admin/admin-ajax.php?action=cfdb-login&cfdb-action=cfdb-export&form=Contacto&show=%23%2CNombre%2CEmail%2CTelefono%2CApellido%2CRut%2CMensaje%2CMailing%2CAno%2CMes%2CSubmitted%2CHora%2C&role=Administrator&permissionmsg=true&trans=Nombre%3Dstrtoupper(Nombre)%26%26Apellido%3Dstrtoupper(Apellido)%26%26Email%3Dstrtoupper(Email)%26%26Mailing%3Dstr_replace(%27Mailing%27%2C%20%27SI%27%2C%20%27Mailing%27)%26%26AddRowNumberField(%23%2C1)%26%26Fecha%3Dstrtotime(Submitted)%26%26Submitted%3Ddate(%27d-m-Y%27%2CFecha)%26%26Hora%3Ddate(%27H%3Ai%3As%27%2CFecha)%26%26Mes%3Dstrftime(%27%25B%27%2CFecha)%26%26Mes%3Dstrtoupper(Mes)%26%26Ano%3Ddate(%27Y%27%2CFecha)%20&torderby=Submitted%20DESC&enc=IQY&headers=Telefono%3DTel%C3%A9fono%2CSubmitted%3DFecha%2CAno%3DA%C3%B1o&l=45f455b75918572afbe7d3892ec6817f173407b865496962"><button class="btn btn small"><i class="fas fa-download"></i> Query</button></a>
	<div style="width:100%; float:left; height:15px;"> </div>
	<a href="<?php the_permalink();?>form-contacto"><button class="btn small btn-block">Ver Registros</button></a>
</div>
<!-- Cierre Formulario -->


<!-- Formulario -->
<div class="col-md-3">
	<h4 style="margin-bottom:15px;"><strong>Form. Landing SEO</strong></h4>
	
	<div style="width: 100%; background:#e5302d; padding:50px; text-align: center; color:white;">
	<h1 style="font-size:70px;"><?php echo do_shortcode('[cfdb-count form="Landing"]');?></h1>
	</div>
	<hr></hr>

	<div>Último registro:</div> 
	<div><?php echo do_shortcode('[cfdb-value form="Landing" show="Email" limit="1"]');?></div>
	<div style="width:100%; display:block; float:left; height:15px;"></div>
	<button class="btn btn small"><i class="fas fa-download"></i> CSV</button>

	<div style="width:100%; float:left; height:15px;"> </div>
	<a href="<?php the_permalink();?>form-landing"><button class="btn  btn small btn-block">Ver Registros</button></a>
</div>
<!-- Cierre Formulario -->



<!-- Formulario -->
<div class="col-md-3">
	<h4 style="margin-bottom:15px;"><strong>Form. Referidos</strong></h4>
	<div style="width: 100%; background:#e5302d; padding:50px; text-align: center; color:white;">
	<h1 style="font-size:70px;"><?php echo do_shortcode('[cfdb-count form="Referidos"]');?></h1>
	</div>
	<hr></hr>
		<div>Último registro: </div>
		<div><?php echo do_shortcode('[cfdb-value form="Referidos" show="Email" limit="1"]');?></div>
	<div style="width:100%; display:block; float:left; height:15px;"></div>
	<button class="btn btn small"><i class="fas fa-download"></i> CSV</button>

	<div style="width:100%; float:left; height:15px;"> </div>
	<a href="<?php the_permalink();?>form-referidos"><button class="btn  btn small btn-block">Ver Registros</button></a>
</div>
<!-- Cierre Formulario -->


<!-- Formulario -->
<div class="col-md-3">
	<h4 style="margin-bottom:15px;"><strong>Form. Descarga PDF</strong></h4>
	<div style="width: 100%; background:#e5302d; padding:50px; text-align: center; color:white;">
	<h1 style="font-size:70px;"><?php echo do_shortcode('[cfdb-count form="Descargar PDF"]');?></h1>
	</div>
	<hr></hr>

		<div>Último registro: </div>
		<div><?php echo do_shortcode('[cfdb-value form="Descargar PDF" show="Email" limit="1"]');?></div>

	<div style="width:100%; display:block; float:left; height:15px;"></div>
	<button class="btn btn small"><i class="fas fa-download"></i> CSV</button>

	<div style="width:100%; float:left; height:15px;"> </div>
	<a href="<?php the_permalink();?>form-pdf"><button class="btn  btn small btn-block">Ver Registros</button></a>
</div>
<!-- Cierre Formulario -->


<!-- Formulario -->
<div class="col-md-3">
	<h4 style="margin-bottom:15px;"><strong>Form. Servicio al Cliente</strong></h4>
	<div style="width: 100%; background:#e5302d; padding:50px; text-align: center; color:white;">
	<h1 style="font-size:70px;"><?php echo do_shortcode('[cfdb-count form="Servicio de Postventa"]');?></h1>
	</div>
	<hr></hr>
		<div>Último registro: </div>
		<div><?php echo do_shortcode('[cfdb-value form="Servicio de Postventa" show="email" limit="1"]');?></div>

	<div style="width:100%; display:block; float:left; height:15px;"></div>
	<button class="btn btn small"><i class="fas fa-download"></i> CSV</button>

	<div style="width:100%; float:left; height:15px;"> </div>
	<a href="<?php the_permalink();?>form-servicios"><button class="btn  btn small btn-block">Ver Registros</button></a>
</div>
<!-- Cierre Formulario -->



<!-- Formulario -->
<div class="col-md-3">
	<h4 style="margin-bottom:15px;"><strong>Form. Proyectos</strong></h4>
	<div style="width: 100%; background:#e5302d; padding:50px; text-align: center; color:white;">
	<h1 style="font-size:70px;"><?php echo do_shortcode('[cfdb-count form="Solicitar Información"]');?></h1>
	</div>
	<hr></hr>
		<div>Último registro: </div>
		<div><?php echo do_shortcode('[cfdb-value form="Solicitar Información" show="Email" limit="1"]');?></div>

	<div style="width:100%; display:block; float:left; height:15px;"></div>
	<button class="btn btn small"><i class="fas fa-download"></i> CSV</button>

	<div style="width:100%; float:left; height:15px;"> </div>
	<a href="<?php the_permalink();?>form-proyectos"><button class="btn  btn small btn-block">Ver Registros</button></a>
</div>
<!-- Cierre Formulario -->


<!-- Formulario -->
<div class="col-md-3">
	<h4 style="margin-bottom:15px;"><strong>Form. Plantas</strong></h4>
	<div style="width: 100%; background:#e5302d; padding:50px; text-align: center; color:white;">
	<h1 style="font-size:70px;"><?php echo do_shortcode('[cfdb-count form="Solicitar Información por planta"]');?></h1>
	</div>
	<hr></hr>
		<div>Último registro: </div>
		<div><?php echo do_shortcode('[cfdb-value form="Solicitar Información por planta" show="Email" limit="1"]');?></div>


	<div style="width:100%; display:block; float:left; height:15px;"></div>
	<button class="btn btn small"><i class="fas fa-download"></i> CSV</button>

	<div style="width:100%; float:left; height:15px;"> </div>
	<a href="<?php the_permalink();?>form-plantas"><button class="btn  btn small btn-block">Ver Registros</button></a>
</div>
<!-- Cierre Formulario -->




<!-- Formulario -->
<div class="col-md-3">
	<h4 style="margin-bottom:15px;"><strong>Base de datos Global</strong></h4>
	<div style="width: 100%; background:#222; padding:50px; text-align: center; color:white;">
	<h1 style="font-size:70px;"><?php echo do_shortcode('[cfdb-count form="Solicitar Información,Solicitar Información por planta"]');?></h1>
	</div>
	<hr></hr>
		<div>Último registro: </div>
		<div><?php echo do_shortcode('[cfdb-value form="Solicitar Información,Solicitar Información por planta" show="Email" limit="1"]');?></div>

	<div style="width:100%; display:block; float:left; height:15px;"></div>
	<button class="btn btn small"><i class="fas fa-download"></i> CSV</button>

	<div style="width:100%; float:left; height:15px;"> </div>
	<a href="<?php the_permalink();?>form-global"><button class="btn  btn small btn-block">Ver Registros</button></a>
</div>
<!-- Cierre Formulario -->


<!-- Formulario -->
<div class="col-md-3">
	<h4 style="margin-bottom:15px;"><strong>Bases para Mailing</strong></h4>
	<p>Personas que han aceptado recibir mailing en sus correos.</p>
	<div style="width: 100%; background:#e5302d; padding:50px; text-align: center; color:white;">
	<h1 style="font-size:70px;"><?php echo do_shortcode('[cfdb-count form="Contacto,Cotización de Proyecto,Descargar PDF,Landing,Referidos,Solicitar Información,Solicitar Información por planta" filter="Mailing===Deseo recibir información y novedades"]');?></h1>
	</div>
	<hr></hr>
		<div>Último registro: </div>
		<div><?php echo do_shortcode('[cfdb-value form="Contacto,Cotización de Proyecto,Descargar PDF,Landing,Referidos,Solicitar Información,Solicitar Información por planta" show="Email" limit="1"]');?></div>

	<div style="width:100%; display:block; float:left; height:15px;"></div>
	<button class="btn btn small"><i class="fas fa-download"></i> CSV</button>
	<div style="width:100%; float:left; height:15px;"> </div>
	<a href="<?php the_permalink();?>form-mailing"><button class="btn  btn small btn-block">Ver Registros</button></a>
</div>
<!-- Cierre Formulario -->


</div>
</section>
<?php get_footer();?>

<?php endif;?>