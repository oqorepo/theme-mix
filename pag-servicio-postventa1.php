<?php
/**
 * Template Name: Servicio Post-Venta1
*/
get_header(); 
get_template_part('secciones/header-normal');
	$current = $post->ID;
	
	$parent = $post->post_parent;
	
	$grandparent_get = get_post($parent);
	
	$grandparent = $grandparent_get->post_parent;


get_template_part("funciones/mysql.class");
$bd = new BD();
$bd->conectar();




	?>



<section class="pagina-interior">

<div class="container">

<ul class="breadcrumb">
    <li><a href="<?php bloginfo('url');?>">Inicio</a></li>


    <?php if ($root_parent = get_the_title($grandparent) !== $root_parent = get_the_title($current)):?>
	
<?php else:?>
	
<li><?php echo get_the_title($parent); ?></li>
<?php endif;?>


    <li class="active"><?php the_title();?></li>
</ul>





<h1 class="display"><?php the_title();?></h1>
<div class="row bs-wizard" style="border-bottom:0;">
	
	<div class="col-xs-4 bs-wizard-step paso-1  complete ">
		<div class="text-center bs-wizard-stepnum">Paso 1</div>
		<div class="progress"><div class="progress-bar"></div></div>
		<a href="#" class="bs-wizard-dot"></a>
		<div class="bs-wizard-info text-center">Datos del Propietario</div>
	</div>
	
	<div class="col-xs-4 bs-wizard-step paso-2 disabled">
		<div class="text-center bs-wizard-stepnum">Paso 2</div>
		<div class="progress"><div class="progress-bar"></div></div>
		<a href="#" class="bs-wizard-dot"></a>
		<div class="bs-wizard-info text-center">Datos del Solicitante</div>
	</div>
	<div class="col-xs-4 bs-wizard-step paso-3 disabled">
		<div class="text-center bs-wizard-stepnum">Paso 3</div>
		<div class="progress"><div class="progress-bar"></div></div>
		<a href="#" class="bs-wizard-dot"></a>
			<div class="bs-wizard-info text-center">Datos de la Vivienda</div>
	</div>

</div>












<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


<div id="gracias-form-postventa">
	<div class="col-md-12">
		<img src="<?php bloginfo('template_url');?>/recursos/img/iconos/enviado2.svg" class="img-responsive icon-enviado">
		<h3>Gracias <span class="nombre-persona"></span></h3>
		<p><strong>Hemos recibido su solicitud de postventa con N° <span class="numero-solicitud"></span></strong></p>
		<p>Nos pondremos en contacto con usted en un plazo máximo de 5 días hábiles, para coordinar una visita.</p>
		<hr></hr>
		<strong><p>Horario de Visita</p></strong>
		<p>Lunes a Viernes de 9:00 a 17:00</p>


<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> Ante emergencias de los servicios básicos (gas, agua, luz) debe contactarse directamente con la empresa proveedora.</div>



	

	</div>
</div>







<select name="proyecto_db" id="proyecto_db"  tabindex="14" class="required hide">
	<option value="" disabled="disabled" selected class="required">Seleccione proyecto</option>
	<?php
	
	$host = "localhost"; 
    $usuario = "desarrol_oqo";
    $password = "desarrollo";
    $db = "desarrol_postventa";
	
	$link = mysqli_connect("$host", "$usuario", "$password", "$db");
	
	//$bd2 = new BD();
	//$bd2->conectar_postventa();
	
	$rs2 = mysqli_query($link, "SELECT * FROM proyectos ORDER BY nombre ASC");
					//for($i=0; $i<$bd2->numregistros(); $i++){
						//$proyecto = $bd2->fetch_row($i);
	while($rowPr = mysqli_fetch_assoc($rs2)){
	?>
	<option value="<?php echo $rowPr["id"];?>" data-email="<?php echo $rowPr["email"];?>" data-cc="<?php echo $rowPr["cc"];?>" data-bcc="<?php echo $rowPr["bcc"];?>"><?php echo $rowPr["nombre"]; ?></option>


	<?php
	}
	//$bd2->desconectar();
	?>
</select>






<div id="formulario-postventa">
<?php the_content();?>
</div>


<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>







</div>

</section>











<?php get_footer();?>

