$(function () {

	//Cambiar los Clicks por hovers en el Megamenu con Bootstrap4
    $(".dropdown").hover(
        function() { $('.dropdown-menu', this).fadeIn("fast");
        },
        function() { $('.dropdown-menu', this).fadeOut("fast");
	});
	

	/* 
	------------------------------------------------------------------
	PRELOAD
	------------------------------------------------------------------
	*/
	$(window).on('load', function () {
		$('.cd-loader').fadeOut('slow', function () {
			$(this).remove();
		});
	});

	$('[data-toggle="tooltip"]').tooltip();


	$('#home-carousel').owlCarousel({
		loop: true,
		margin: 0,
		items: 1,
		autoplay:true
	});

	$('#proyecto-carousel-top').owlCarousel({
		loop: true,
		margin: 0,
		items: 1,
		autoplay:true,
		dots:false
	});

	$('#proyecto-carousel-caracteristicas').owlCarousel({
		loop: true,
		margin: 0,
		items: 1,
		autoplay:true
	});

	function toggleDropdown(e) {
		const _d = $(e.target).closest('.dropdown'),
			_m = $('.dropdown-menu', _d);
		setTimeout(function () {
			const shouldOpen = e.type !== 'click' && _d.is(':hover');
			_m.toggleClass('show', shouldOpen);
			_d.toggleClass('show', shouldOpen);
			$('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
		}, e.type === 'mouseleave' ? 300 : 0);
	}


	/* 
	------------------------------------------------------------------
	SCROLL MENU
	------------------------------------------------------------------
	*/

	$(window).scroll(function () {

		if ($(this).scrollTop() > 50) {
			$('#scroll-desktop-menu').removeClass('d-none');
			$('#scroll-mobile-menu').removeClass('d-none');

			if ($(this).scrollTop() > 150) {
				$('#scroll-desktop-menu').removeClass('esconder-menu');
				$('#scroll-desktop-menu').addClass('mostrar-menu');

				$('#scroll-mobile-menu').removeClass('esconder-menu');
				$('#scroll-mobile-menu').addClass('mostrar-menu');
			} else {
				$('#scroll-desktop-menu').addClass('esconder-menu');
				$('#scroll-desktop-menu').removeClass('mostrar-menu');

				$('#scroll-mobile-menu').addClass('esconder-menu');
				$('#scroll-mobile-menu').removeClass('mostrar-menu');
			}

		}else{
			$('#scroll-desktop-menu').addClass('d-none');
			$('#scroll-mobile-menu').addClass('d-none');
		}        
	});

	/* 
	------------------------------------------------------------------
	ELASTIC MENU
	------------------------------------------------------------------
	*/
	
	$("#btn_show").click(function(){
        $("#elastic-menu").addClass("active");
    });

    $("#btn_show2").click(function(){
        $("#elastic-menu").addClass("active");
	});
	
	$("#btn_close").click(function(){
        $("#elastic-menu").removeClass("active");
    });

	$('body')	
	
	/* 
	------------------------------------------------------------------
	IMPRESION DE QUERY PHP DE PROYECTOS EN EL MENU
	------------------------------------------------------------------
	*/
		
	var metropolitana = 23,
	regiones = 25;


	$.each(project_data.data, function (k, v) {
		if (metropolitana === v.region_parent) {
			//Para el megamenu
			$('.proyectos_rm').append(
				'<li class="nav-item">\n\
					<a href="'+v.url+'">\n\
					<div class="card">\n\
						<div class="card-img-top" style="background-image:url('+v.image_url+')"></div>\n\
						<div class="card-body">\n\
							<h5 class="card-title">'+v.region+'</h5>\n\
							<h4 class="card-text">'+v.title+'</h4>\n\
						</div>\n\
						<div class="estado-proyecto" style="background:'+v.color_etiqueta+';">'+v.tag+'</div>\n\
					</div>\n\
					</a>\n\
				</li>');
			
			//Para el menu buscr del top
			
			$('#sector-stgo').append('<a class="btn btn-lg btn-gris" href="'+v.url+'">'+v.region+'<a>');

			//Para el menu elastico en responsive
			$('#elastic-menu-stgo').append('<li><a href="'+v.url+'">'+v.title+'<a></li>');
		} else {
			$('.proyectos_re').append(
				'<li class="nav-item">\n\
					<a href="'+v.url+'">\n\
					<div class="card">\n\
						<div class="card-img-top" style="background-image:url('+v.image_url+')"></div>\n\
						<div class="card-body">\n\
							<h5 class="card-title">'+v.region+'</h5>\n\
							<h4 class="card-text">'+v.title+'</h4>\n\
						</div>\n\
						<div class="estado-proyecto" style="background:'+v.color_etiqueta+';">'+v.tag+'</div>\n\
					</div>\n\
					</a>\n\
				</li>');

			$('#sector-reg').append('<a class="btn btn-lg btn-gris" href="'+v.url+'">'+v.region+'<a>');

			$('#elastic-menu-reg').append('<li><a href="'+v.url+'">'+v.title+'<a></li>');
		}
	});

	//Esconder Mostrar Preguntas de Buscar

	$("#btn-buscar-stgo").click(function(){
		$("#preguntas-buscar1").addClass("hide-preguntas");
		$("#preguntas-buscar2-stgo").removeClass("hide-preguntas");
	});

	$("#btn-buscar-volver-stgo").click(function(){
		$("#btn-buscar-volver-stgo").parent().addClass("hide-preguntas");
		$("#preguntas-buscar1").removeClass("hide-preguntas");
	});

	$("#btn-buscar-reg").click(function(){
		$("#preguntas-buscar1").addClass("hide-preguntas");
		$("#preguntas-buscar2-reg").removeClass("hide-preguntas");
	});

	$("#btn-buscar-volver-reg").click(function(){
		$("#btn-buscar-volver-reg").parent().addClass("hide-preguntas");
		$("#preguntas-buscar1").removeClass("hide-preguntas");
	});
});