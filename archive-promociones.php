<?php
get_header();
get_template_part('secciones/header-normal');
?>

<script>
$(document).ready(function() {
$('.head').css('border', '0px');
$('.linea-menu-bot').addClass('activo');
// Google Tag Manager
dataLayer.push({
'tipoPagina': 'Archivo de Promociones',
});});
</script>

<!-- Título -->
<section class="seccion-oscura">
  <div class="container">
    <div class="row">
      <h1 class="display blanco">Promociones</h1>
    </div>
  </div>
</section>
<!-- Título -->


<!-- Loop Promociones -->
<?php
$post_id          = get_the_ID();
$hoy              = date( 'Ymd', current_time( 'timestamp', 0 ));
$query            = new WP_Query(array(
'post_type'       => array('promociones'),
'posts_per_page'  => -1,
'post_status'     => 'publish',
'order'           => 'DESC',
'orderby'         => 'meta_value',
'meta_key'        => 'fecha_de_expiracion' // Se ordena por fecha de expiración
));
if ( $query->have_posts() ) : ?>
<section class="mas-promos">
  <div class="container">
    <div class="row">
      <?php while ( $query->have_posts() ) : $query->the_post(); ?>
      <div class="col-md-3" style="margin-top:30px;">
        <?php // Variables
        $imagen = get_field('imagen_promocion_mobile');
        $imagen_promo_array = wp_get_attachment_image_src($imagen, 'full');
        $estado = get_field('estado');
        $personalizar = get_field('personalizar');
        $fecha = get_field('fecha_de_expiracion', false, false);
        $fecha = new DateTime($fecha);
        $fechaFormato = $fecha->format('Ymd');
        $hoy = date( 'Ymd', current_time( 'timestamp', 0 ));
        $dateformatstring = "j F Y";
        $unixtimestamp = strtotime(get_field('fecha_de_expiracion', false, false));
        $selecciona = get_field('selecciona_proyecto_o_planta');
        ?>
        <!-- Condicionales -->
        <?php  if ($fechaFormato >= $hoy):?>
        <a href="<?php the_permalink();?>" id="clickPromocion" onclick="dataLayer.push({'Promo': '<?php the_title();?>'});">
          <div class="ribbon ribbon-top-left" style="margin-top:0px; margin-left:0px; left:15px !important;">
            <span style="font-size:13px !important; top:42px;">
              <?php if ($estado == "Personalizar"):?>
              <?php echo $personalizar;?>
              <?php else:?>
              <?php echo $estado;?>
              <?php endif;?>
            </span>
          </div>
          <img src="<?php echo $imagen_promo_array[0];?>" class="img-responsive tooltipster" title="Ver Promoción"  alt="<?php the_title();?>" />
        </a>
        <strong><?php the_title();?></strong>
        <p><i class="far fa-calendar-alt"></i> Vigencia hasta <?php echo date_i18n($dateformatstring, $unixtimestamp);?> </p>
        <hr>
        <?php if( $selecciona ): ?>
        <div>Promoción válida para</div>
        <?php foreach( $selecciona as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <a href="<?php the_permalink();?>"  onclick="dataLayer.push({'promocionVinculada': '<?php the_title();?>'});"><?php the_title();?></a>
        <?php endforeach; ?>
        <?php endif;?>
        <?php else:?>
        <img src="<?php echo $imagen_promo_array[0];?>" class="img-responsive tooltipster blancoynegro" title="Promoción No Vigente"  alt="<?php the_title();?>" />
        <strong><?php the_title();?></strong>
        <div><i class="far fa-calendar-alt"></i> Promoción No Vigente</div>
        Finalizó el <?php echo date_i18n($dateformatstring, $unixtimestamp);?>
        <?php if( $selecciona ): ?>
        <hr>
        <div>Promoción válida para</div>
        <?php foreach( $selecciona as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <a href="<?php the_permalink();?>"  onclick="dataLayer.push({'promocionVinculada': '<?php the_title();?>'});"><?php the_title();?></a>
        <?php endforeach; ?>
        <?php endif;?>
        <?php endif;?>
        <!-- Condicionales -->
      </div>
      <?php endwhile;?>
    </div>
  </div>
</section>
<?php endif;?>


<?php get_footer();?>